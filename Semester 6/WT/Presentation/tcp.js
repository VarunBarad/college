var net = require('net');

net.createServer(function (socket) {
  socket.write('Echo server\n');
  socket.pipe(socket);
  console.log('Request received');
}).listen(8080);

console.log('Server started');
