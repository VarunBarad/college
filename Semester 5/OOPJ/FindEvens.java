import java.util.HashSet;

class EvenRunnable implements Runnable {

  private HashSet<Integer> evens;
  private int lowerLimit;
  private int upperLimit;

  public EvenRunnable(int index, int n, HashSet<Integer> evens) {
    this.evens = evens;
    this.lowerLimit = index * 100;
    this.upperLimit = ((lowerLimit + 100) < n) ? (lowerLimit + 100) : (n);
  }

  @Override
  public void run() {
    for (int i = this.lowerLimit; i < this.upperLimit; i++) {
      if ((i % 2) == 0) {
        this.evens.add(i);
      }
    }
  }
}

public class FindEvens {
  public static void main(String[] args) {
    int n = Integer.parseInt(args[0]);

    HashSet<Integer> evens = new HashSet<>();

    int numberOfThreads = (n / 100) + 1;

    Thread[] threads = new Thread[numberOfThreads];
    for (int i = 0; i < numberOfThreads; i++) {
      threads[i] = new Thread(new EvenRunnable(i, n, evens));
      threads[i].start();
    }

    try {
      for (int i = 0; i < numberOfThreads; i++) {
        threads[i].join();
      }

      System.out.println("Even numbers are:");
      for (Integer i : evens) {
        System.out.print(i + " ");
      }
      System.out.println();
    } catch (InterruptedException e) {
      e.printStackTrace();
      System.out.println("Some stuff went wrong");
    }
  }
}
