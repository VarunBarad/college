class IndexOutOfBoundsException extends Exception {
  public IndexOutOfBoundsException() {
    super("Index out of bounds was encountered");
  }

  public IndexOutOfBoundsException(String message) {
    super(message);
  }
}

class OverflowException extends Exception {
  public OverflowException() {
    super("The specified space is already filled");
  }

  public OverflowException(String message) {
    super(message);
  }
}

class NegativeNumberException extends Exception {
  public NegativeNumberException() {
    super("Negative number encountered");
  }

  public NegativeNumberException(String message) {
    super(message);
  }
}

public class ExceptionAssignment {
  public static void main(String[] args) {
    try {
      int size = Integer.parseInt(args[0]);
      int[] store = new int[size];
      for (int i = 0; i < store.length; i++) {
        store[i] = -1;
      }
      int index, number;
      for (int i = 1; i < args.length; i += 2) {
        index = Integer.parseInt(args[i]);
        number = Integer.parseInt(args[i + 1]);

        try {
          if ((index < 0) || (index > (size - 1))) {
            throw new IndexOutOfBoundsException();
          } else if (store[index] != -1) {
            throw new OverflowException();
          } else if (number < 0) {
            throw new NegativeNumberException();
          } else {
            store[index] = number;
            System.out.println(index + " " + number);
          }
        } catch (IndexOutOfBoundsException | NegativeNumberException | OverflowException e) {
          System.err.println(e.getMessage());
        }
      }
    } catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
      System.err.println("Improper arguments");
    }
  }
}
