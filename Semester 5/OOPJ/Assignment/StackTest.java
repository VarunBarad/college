import java.util.EmptyStackException;
import java.util.Stack;

public class StackTest {
  public static void main(String[] args) {
    Stack<Integer> stack = new Stack<>();
    int n;
    for (int i = 0; i < args.length; i++) {
      switch (args[i].toLowerCase()) {
        case "push":
          try {
            n = Integer.parseInt(args[++i]);
            stack.push(n);
            System.out.println(n + " pushed on stack");
          } catch (NumberFormatException e) {
            System.out.println("Number not formatted correctly");
          }
          break;
        case "pop":
          try {
            System.out.println("Popped value: " + stack.pop());
          } catch (EmptyStackException e) {
            System.out.println("Empty Stack");
          }
          break;
        default:
          System.out.println("Invalid Argument");
      }
    }
  }
}
