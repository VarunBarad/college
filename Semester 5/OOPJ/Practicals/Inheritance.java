class Parent {
  private String id;

  public Parent(String id) {
    this.id = id;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String toString() {
    return ("ID: " + id);
  }

  public void display() {
    System.out.println(this);
  }
}

class Child extends Parent {
  private int rank;

  public Child(String id, int rank) {
    super(id);
    this.rank = rank;
  }

  public int getRank() {
    return rank;
  }

  public void setRank(int rank) {
    this.rank = rank;
  }

  @Override
  public String toString() {
    return (super.toString() + " Rank: " + this.getRank());
  }

  @Override
  public void display() {
    System.out.println(this);
  }
}

public class Inheritance {
  public static void main(String[] args) {
    Child[] children = new Child[args.length / 2];
    for (int i = 0; i < children.length; i++) {
      children[i] = new Child(args[2 * i], Integer.parseInt(args[(2 * i) + 1]));
      children[i].display();
    }
  }
}
