class Coordinate {
  private int x;
  private int y;
  private int z;

  public Coordinate() {
    this.setX(0);
    this.setY(0);
    this.setZ(0);
  }

  public Coordinate(int x, int y, int z) {
    this.setX(x);
    this.setY(y);
    this.setZ(z);
  }


  public int getX() {
    return x;
  }

  public void setX(int x) {
    this.x = x;
  }

  public int getY() {
    return y;
  }

  public void setY(int y) {
    this.y = y;
  }

  public static Coordinate add(Coordinate a, Coordinate b) {
    return (new Coordinate((a.getX() + b.getX()), (a.getY() + b.getY()), (a.getZ() + b.getZ())));
  }

  public String toString() {
    return ("(" + this.getX() + ", " + this.getY() + ", " + this.getZ() + ")");
  }

  public void display() {
    System.out.println(this.toString());
  }

  public int getZ() {
    return z;
  }

  public void setZ(int z) {
    this.z = z;
  }
}

public class CoordinateTest {
  public static void main(String[] args) {
    Coordinate c1 = new Coordinate(1, 2, 3);
    Coordinate c2 = new Coordinate(4, 5, 6);
    Coordinate c3 = Coordinate.add(c1, c2);
    System.out.println(c1.toString());
    System.out.println(c2);
    c3.display();
  }
}
