import java.util.EmptyStackException;
import java.util.Scanner;
import java.util.Stack;

public class StackTest {
  private Stack<Integer> stack;
  private Scanner input;
  
  public StackTest() {
    this.stack = new Stack<>();
    this.input = new Scanner(System.in);
  }
  
  public int displayMenu() {
    System.out.println("Select option:");
    System.out.println("1. Push");
    System.out.println("2. Pop");
    System.out.println("3. Quit");
    System.out.print("Enter your choice: ");
    int choice = input.nextInt();
    return choice;
  }
  
  public void push() {
    System.out.print("Enter value to push: ");
    this.stack.push(input.nextInt());
  }
  
  public void pop() {
    try {
      System.out.println("Popped value: " + this.stack.pop());
    } catch (EmptyStackException e) {
      System.out.println("Empty Stack");
    }
  }
  
  public void close() {
    this.input.close();
  }
  
  public static void main(String[] args) {
    StackTest s = new StackTest();
    int choice;
    finish:
    while (true) {
      choice = s.displayMenu();
      switch (choice) {
        case 1:
          s.push();
          break;
        case 2:
          s.pop();
          break;
        case 3:
          break finish;
        default:
          break;
      }
    }
    s.close();
  }
}
