interface Area {
  abstract int measureArea();
}

class Square implements Area {
  private int side;

  public Square(int side) {
    this.side = side;
  }

  @Override
  public int measureArea() {
    return this.getSide() * this.getSide();
  }

  public int getSide() {
    return side;
  }

  public void setSide(int side) {
    this.side = side;
  }
}

class Rectangle implements Area {
  private int height;
  private int width;

  public Rectangle(int height, int width) {
    this.height = height;
    this.width = width;
  }

  @Override
  public int measureArea() {
    return this.getHeight() * this.getWidth();
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }
}

public class InterfaceTest {
  public static void main(String[] args) {
    Area area;
    int side = 5;
    int height = 21;
    int width = 8;
    Square square = new Square(side);
    Rectangle rectangle = new Rectangle(height, width);
    area = square;
    System.out.println("Square: " + area.measureArea());
    area = rectangle;
    System.out.println("Rectangle: " + area.measureArea());
  }
}
