import java.util.Stack;

interface StackInterface {
  abstract void push(Integer n);

  abstract Integer pop();
}

class StackTest implements StackInterface {
  private Stack<Integer> stack;

  public StackTest() {
    this.stack = new Stack<>();
  }

  @Override
  public void push(Integer n) {
    this.stack.push(n);
  }

  @Override
  public Integer pop() {
    Integer n;
    try {
      n = this.stack.pop();
    } catch (Exception e) {
      n = 0;
    }
    return n;
  }
}

public class StackInterfaceTest {
  public static void main(String[] args) {
    StackTest stack = new StackTest();
    int n;
    for (int i = 0; i < args.length; i++) {
      switch (args[i].toLowerCase()) {
        case "push":
          try {
            n = Integer.parseInt(args[++i]);
            stack.push(n);
            System.out.println(n + " pushed on stack");
          } catch (NumberFormatException e) {
            System.out.println("Number not formatted correctly");
          }
          break;
        case "pop":
          System.out.println("Popped value: " + stack.pop());
          break;
        default:
          System.out.println("Invalid Argument");
      }
    }
  }
}
