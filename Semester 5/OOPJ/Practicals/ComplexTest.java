class Complex {
  private int real;
  private int imaginary;

  public Complex() {
    this.setReal(0);
    this.setImaginary(0);
  }

  public Complex(int real, int imaginary) {
    this.setReal(real);
    this.setImaginary(imaginary);
  }


  public int getReal() {
    return real;
  }

  public void setReal(int real) {
    this.real = real;
  }

  public int getImaginary() {
    return imaginary;
  }

  public void setImaginary(int imaginary) {
    this.imaginary = imaginary;
  }

  public static Complex add(Complex a, Complex b) {
    return (new Complex((a.getReal() + b.getReal()), (a.getImaginary() + b.getImaginary())));
  }

  public String toString() {
    return this.getReal() + " " + ((this.getImaginary() < 0) ? ("- " + (-this.getImaginary())) : ("+ " + this.getImaginary())) + "i";
  }

  public void display() {
    System.out.println(this.toString());
  }
}

public class ComplexTest {
  public static void main(String[] args) {
    Complex c1 = new Complex(1, 2);
    Complex c2 = new Complex(3, 4);
    Complex c3 = Complex.add(c1, c2);
    System.out.println(c1);
    System.out.println(c2.toString());
    c3.display();
  }
}
