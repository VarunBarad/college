public class ParseInteger {
  public static void main(String[] args) {
    int countPos = 0;
    int countNeg = 0;
    int countInvalid = 0;
    int n;

    for (String arg : args) {
      try {
        n = Integer.parseInt(arg);
        if (n < 0) {
          countNeg++;
        } else {
          countPos++;
        }
      } catch (NumberFormatException e) {
        countInvalid++;
      }
    }

    System.out.println("Positive: " + countPos);
    System.out.println("Negative: " + countNeg);
    System.out.println("Exceptions: " + countInvalid);
  }
}
