// Dynamic Programming code for Optimal Binary Search Tree Problem
#include <iostream>
#include <fstream>
#include <climits>

using namespace std;

class Node {
private:
  int key;
  double frequency;
public:
  Node() {
    this->key = 0;
    this->frequency = 0.0;
  }

  Node(int key, double frequency) {
    this->key = key;
    this->frequency = frequency;
  }

  int getKey() {
    return this->key;
  }

  double getFrequency() {
    return this->frequency;
  }

  void setKey(int key) {
    this->key = key;
  }

  void setFrequency(double frequency) {
    this->frequency = frequency;
  }

};

// A utility function to calculate lines in a given file
unsigned long countNumberOfLines(string filePath) {
  unsigned long count = 0;
  string temp;
  ifstream file;
  file.open(filePath);
  if (file.is_open()) {
    while (!file.eof()) {
      getline(file, temp);
      if (temp.length() > 0) {
        count++;
      }
    }
  }
  return count;
}

// A utility function to read the data from file into array of Node
void readData(string dataFilePath, Node *nodes, unsigned long numberOfNodes) {
  int tempKey;
  double tempFrequency;
  ifstream dataFile;
  dataFile.open(dataFilePath);
  if (dataFile.is_open()) {
    for (unsigned long i = 0; i < numberOfNodes; ++i) {
      dataFile >> tempKey >> tempFrequency;
      nodes[i].setKey(tempKey);
      nodes[i].setFrequency(tempFrequency);
    }
    dataFile.close();
  }
}

// A utility function to calculate linear index for a 2D array
unsigned long getIndex(unsigned long i, unsigned long j, unsigned long numberOfColumns) {
  return ((i * numberOfColumns) + j);
}

// A utility function to get sum of array elements freq[i] to freq[j]
double sumFrequency(Node *data, unsigned long size, unsigned long startIndex, unsigned long endIndex) {
  double sum = 0;
  if ((startIndex >= 0) && (startIndex <= endIndex) && (endIndex < size)) {
    for (unsigned long i = startIndex; i <= endIndex; i++) {
      sum += data[i].getFrequency();
    }
  }
  return sum;
}

// A Dynamic Programming based function to calculate minimum cost of
// a Binary Search Tree
double optimalSearchTree(Node *nodes, unsigned long numberOfNodes) {
  // Auxiliary 2D matrix to store results of sub-problems
  double *cost = new double[numberOfNodes * numberOfNodes];

  for (unsigned long i = 0; i < numberOfNodes; ++i) {
    cost[getIndex(i, i, numberOfNodes)] = nodes[i].getFrequency();
  }

  // Chains of lengths 2,3,..,n-1
  // chainLength = (length of chain) - 1
  for (unsigned long chainLength = 1; chainLength < numberOfNodes; ++chainLength) {
    // i is row number in cost matrix
    for (unsigned long i = 0; i < (numberOfNodes - chainLength); ++i) {
      // Calculate column number j from i and chainLength
      unsigned long j = i + chainLength;
      cost[getIndex(i, j, numberOfNodes)] = LONG_MAX;

      // Make all keys from [i..j] as root
      for (unsigned long row = i; row < (j + 1); ++row) {
        // c = cost when [r] becomes root of this subtree
        double c = ((row > i) ? (cost[getIndex(i, (row - 1), numberOfNodes)]) : (0)) + ((row < j) ? (cost[getIndex((row + 1), j, numberOfNodes)]) : (0)) + sumFrequency(nodes, numberOfNodes, i, j);
        if (c < cost[getIndex(i, j, numberOfNodes)]) {
          cost[getIndex(i, j, numberOfNodes)] = c;
        }
      }
    }
  }

  double optimalCost = cost[getIndex(0, (numberOfNodes - 1), numberOfNodes)];
  delete[] cost;
  return optimalCost;
}

int main(int argc, char *argv[]) {
  // Determine path of file where data is stored
  string dataFilePath = "./data";
  if (argc > 1) {
    dataFilePath = argv[1];
  }

  // Find the number of nodes the tree is supposed to have
  unsigned long numberOfNodes = countNumberOfLines(dataFilePath);

  // Create array to store the nodes
  Node *nodes = new Node[numberOfNodes];

  // Read the data from file into array of Node
  readData(dataFilePath, nodes, numberOfNodes);

  // Find the cost of Optimal Binary Search Tree
  double costOfOptimalTree = optimalSearchTree(nodes, numberOfNodes);

  cout << "The cost of Optimal Binary Search Tree: " << costOfOptimalTree << endl;

  return 0;
}