#include <iostream>

using namespace std;

class Node {
private:
  int n;
  Node *next;

public:
  Node(int n) {
    this->n = n;
    this->next = NULL;
  }

  int getN() {
    return this->n;
  }

  void setNext(Node *next) {
    this->next = next;
  }

  Node *getNext() {
    return this->next;
  }
};

class Queue {
private:
  Node *front;
  Node *rear;

public:
  Queue() {
    this->front = NULL;
    this->rear = NULL;
  }

  void append(int n) {
    Node *temp = new Node(n);
    if (front == NULL) {
      front = temp;
      rear = temp;
    } else {
      rear->setNext(temp);
      rear = temp;
    }
  }

  int remove() {
    Node *temp = front;
    if (front != NULL) {
      if (front == rear) {
        front = NULL;
        rear = NULL;
      } else {
        front = front->getNext();
      }
    }
    int n = 0;
    if (temp != NULL) {
      n = temp->getN();
      delete temp;
    }
    return n;
  }

  bool isEmpty() {
    return (this->front == NULL);
  }
};

class Matrix {
public:
  bool matrix[5][5];
};

void breadthFirstSearch(Matrix matrix) {
  int color[5] = {-1, -1, -1, -1, -1};
  Queue queue;
  queue.append(0);
  while(!queue.isEmpty()) {
    int nodeIndex = queue.remove();
    color[nodeIndex] = 1;
    cout << nodeIndex << endl;
    for (int i = 0; i < 5; ++i) {
      if ((matrix.matrix[nodeIndex][i]) && (color[i] < 0)) {
        queue.append(i);
        color[i] = 0;
      }
    }
  }
}

int main() {
  Matrix matrix = Matrix();
  int n;
  for (int i = 0; i < 5; ++i) {
    for (int j = 0; j < 5; ++j) {
      cin >> n;
      matrix.matrix[i][j] = (n != 0);
    }
  }
  breadthFirstSearch(matrix);
  return 0;
}