#include <iostream>

using namespace std;

class Matrix {
public:
  bool matrix[5][5];
};

void depthFirstSearch(Matrix matrix, int index, bool color[5]) {
  color[index] = true;
  cout << index << endl;
  for (int i = 0; i < 5; ++i) {
    if (matrix.matrix[index][i] && !color[i]) {
      depthFirstSearch(matrix, i, color);
    }
  }
}

int main() {
  Matrix matrix = Matrix();
  bool color[5] = {false, false, false, false, false};
  int n;
  for (int i = 0; i < 5; ++i) {
    for (int j = 0; j < 5; ++j) {
      cin >> n;
      matrix.matrix[i][j] = (n != 0);
    }
  }
  depthFirstSearch(matrix, 0, color);
  return 0;
}