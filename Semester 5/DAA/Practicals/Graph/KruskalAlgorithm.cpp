#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct Edge {
  int s, d, w;
};

struct Graph {
  int V, E;

  struct Edge *edge;
};

struct Graph *createGraph(int V, int E) {
  struct Graph *g = (struct Graph *) malloc(sizeof(struct Graph));
  g->V = V;
  g->E = E;

  g->edge = (struct Edge *) malloc(g->E * sizeof(struct Edge));

  return g;
}

struct subset {
  int p;
  int r;
};

int find(struct subset ss[], int i) {
  if (ss[i].p != i)
    ss[i].p = find(ss, ss[i].p);

  return ss[i].p;
}

void Union(struct subset ss[], int x, int y) {
  int xr = find(ss, x);
  int yr = find(ss, y);

  if (ss[xr].r < ss[yr].r)
    ss[xr].p = yr;
  else if (ss[xr].r > ss[yr].r)
    ss[yr].p = xr;

  else {
    ss[yr].p = xr;
    ss[xr].r++;
  }
}

int myComp(const void *a, const void *b) {
  struct Edge *a1 = (struct Edge *) a;
  struct Edge *b1 = (struct Edge *) b;
  return a1->w > b1->w;
}

void KruskalMST(struct Graph *g) {
  int V = g->V;
  struct Edge res[V];
  int e = 0;
  int i = 0;

  qsort(g->edge, g->E, sizeof(g->edge[0]), myComp);

  struct subset *ss =
      (struct subset *) malloc(V * sizeof(struct subset));

  for (int v = 0; v < V; ++v) {
    ss[v].p = v;
    ss[v].r = 0;
  }

  while (e < V - 1) {
    struct Edge ne = g->edge[i++];

    int x = find(ss, ne.s);
    int y = find(ss, ne.d);

    if (x != y) {
      res[e++] = ne;
      Union(ss, x, y);
    }
  }

  printf("Following are the edges in the constructed MST\n");
  for (i = 0; i < e; ++i)
    printf("%d -- %d == %d\n", res[i].s, res[i].d,
           res[i].w);
  return;
}

int main() {
  /*
   Let us create following weighted g
           10
      0--------1
      |  \     |
     6|   5\   |15
      |      \ |
      2--------3
          4
  */
  int V = 4;
  int E = 5;
  struct Graph *g = createGraph(V, E);


  g->edge[0].s = 0;
  g->edge[0].d = 1;
  g->edge[0].w = 10;

  g->edge[1].s = 0;
  g->edge[1].d = 2;
  g->edge[1].w = 6;

  g->edge[2].s = 0;
  g->edge[2].d = 3;
  g->edge[2].w = 5;

  g->edge[3].s = 1;
  g->edge[3].d = 3;
  g->edge[3].w = 15;

  g->edge[4].s = 2;
  g->edge[4].d = 3;
  g->edge[4].w = 4;

  KruskalMST(g);

  return 0;
}