#include <iostream>

using namespace std;

int getIndex(int i, int j, int numberOfColumns) {
  return ((i * numberOfColumns) + j);
}

int count(int *S, int m, int n) {
  int *table = new int[(n + 1) * m];

  for (int i = 0; i < m; i++) {
    table[getIndex(0, i, m)] = 1;
  }

  for (int i = 1; i < n + 1; i++) {
    for (int j = 0; j < m; j++) {
      int x = (i - S[j] >= 0) ? table[getIndex((i - S[j]), j, m)] : 0;

      int y = (j >= 1) ? table[getIndex(i, (j - 1), m)] : 0;

      table[getIndex(i, j, m)] = x + y;
    }
  }
  return table[getIndex(n, (m - 1), m)];
}

int main() {
  int *arr, m, n;

  cout << "Enter the number of coins: ";
  cin >> m;

  arr = new int[m];
  n = 4;

  cout << "Enter values of coins: ";
  for (int i = 0; i < m; ++i) {
    cin >> arr[i];
  }

  cout << "Number of coins: " << count(arr, m, n) << endl;
  return 0;
}
