#include <iostream>
#include <climits>

using namespace std;

int getIndex(int i, int j, int numberOfColumns) {
  return ((i * numberOfColumns) + j);
}

int matrixChainOrder(int *matrices, int size) {

  int *multiplications = new int[size * size];

  for (int i = 1; i < size; i++) {
    multiplications[getIndex(i, i, size)] = 0;
  }

  for (int length = 2; length < size; length++) {
    int j;
    for (int i = 1; i <= size - length + 1; i++) {
      j = i + length - 1;
      multiplications[getIndex(i, j, size)] = INT_MAX;
      for (int k = i; k <= j - 1; k++) {
        int temp = multiplications[getIndex(i, k, size)] + multiplications[getIndex((k + 1), j, size)] + (matrices[i - 1] * matrices[k] * matrices[j]);
        if (temp < multiplications[getIndex(i, j, size)]) {
          multiplications[getIndex(i, j, size)] = temp;
        }
      }
    }
  }

  return multiplications[getIndex(1, (size - 1), size)];
}

int main() {
  int *matrices, size;

  cout << "Enter the number of matrices: ";
  cin >> size;

  size++;

  matrices = new int[size];

  cout << "Enter dimensions of matrices: ";
  for (int i = 0; i < size; ++i) {
    cin >> matrices[i];
  }

  int numberOfMultiplications = matrixChainOrder(matrices, size);

  cout << "Minimum number of Multiplications: " << numberOfMultiplications << endl;

  return 0;
}
