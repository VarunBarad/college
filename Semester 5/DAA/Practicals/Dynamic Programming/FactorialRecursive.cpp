#include <iostream>
#include <sys/time.h>
#include <cstdlib>

using namespace std;

double getTime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return ((t.tv_sec * 1000000) + t.tv_usec);
}

long factorial(long n) {
  if (n < 2) {
    return 1;
  } else {
    return (n * factorial(n - 1));
  }
}

int main() {
  double startTime, endTime, busyTime;
  long n;
  cout << "Enter number: ";
  cin >> n;

  startTime = getTime();
  factorial(n);
  endTime = getTime();

  busyTime = endTime - startTime;

  cout << "Time: " << busyTime << endl;

  return 0;
}