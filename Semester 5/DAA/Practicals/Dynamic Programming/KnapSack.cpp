#include <iostream>
#include <sys/time.h>
#include <cstdlib>

using namespace std;

class Item {
private:
  int value;
  int weight;
public:
  Item() {
    this->value = 0;
    this->weight = 0;
  }

  Item(int value, int weight) {
    this->value = value;
    this->weight = weight;
  }

  int getValue() {
    return this->value;
  }

  int getWeight() {
    return this->weight;
  }

  void setValue(int value) {
    this->value = value;
  }

  void setWeight(int weight) {
    this->weight = weight;
  }
};

double getTime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return ((t.tv_sec * 1000000) + t.tv_usec);
}

void populateItems(Item *items, int itemCount, int maximumWeight) {
  int weightStep = maximumWeight / itemCount;
  int valueStep = maximumWeight;
  int randomWeight, randomValue;

  srand((unsigned int) time(NULL));
  for (int i = 0; i < itemCount; ++i) {
    randomWeight = (rand() % ((weightStep * i) + 1)) + (weightStep * (i + 1));
    randomValue = (rand() % ((valueStep * i) + 1)) + (valueStep * (i + 1));
    items[i].setValue(randomValue);
    items[i].setWeight(randomWeight);
  }
}

double knapSack(Item *items, int size, int maximumWeight) {
  double startTime, endTime;

  startTime = getTime();

  int solutionSize = maximumWeight + 1;
  int *solution = new int[2 * solutionSize];
  for (int i = 0; i < solutionSize; ++i) {
    solution[i] = 0;
    solution[solutionSize + i] = 0;
  }

  for (int i = 0; i < size; ++i) {
    for (int j = 0; j < solutionSize; ++j) {
      solution[j] = solution[solutionSize + j];
    }
    for (int j = 1; j < solutionSize; ++j) {
      if (j >= items[i].getWeight()) {
        if (solution[j] > (items[i].getValue() + solution[j - items[i].getWeight()])) {
          solution[solutionSize + j] = solution[j];
        } else {
          solution[solutionSize + j] = items[i].getValue() + solution[j - items[i].getWeight()];
        }
      } else {
        solution[solutionSize + j] = solution[j];
      }
    }
  }

  delete[] solution;

  endTime = getTime();

  return (endTime - startTime);
}

int main() {
  int itemCount, maximumWeight;
  Item *items;
  cout << "Enter the number of items: ";
  cin >> itemCount;
  cout << "Enter the maximum weight: ";
  cin >> maximumWeight;

  items = new Item[itemCount];
  populateItems(items, itemCount, maximumWeight);

  double solutionTime = knapSack(items, itemCount, maximumWeight);

  cout << "Solution Time: " << solutionTime << endl;

  delete[] items;
  return 0;
}
