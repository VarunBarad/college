#include <iostream>
#include <sys/time.h>
#include <cstdlib>

using namespace std;

double getTime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return ((t.tv_sec * 1000000) + t.tv_usec);
}

long factorial(long n) {
  long fact = 1;
  while (n > 1) {
    fact *= n;
    n--;
  }
  return fact;
}

int main() {
  double startTime, endTime, busyTime;
  long n;
  cout << "Enter number: ";
  cin >> n;

  startTime = getTime();
  factorial(n);
  endTime = getTime();

  busyTime = endTime - startTime;

  cout << "Time: " << busyTime << endl;

  return 0;
}