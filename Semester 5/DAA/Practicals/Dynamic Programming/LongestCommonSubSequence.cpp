#include <iostream>

using namespace std;

unsigned long getIndex(unsigned long i, unsigned long j, unsigned long numberOfColumns) {
  return ((i * numberOfColumns) + j);
}

string longestCommonSubSequence(string string1, string string2) {
  unsigned long numberOfRows = string1.length() + 1;
  unsigned long numberOfColumns = string2.length() + 1;

  int *length = new int[numberOfRows * numberOfColumns];
  int *route = new int[numberOfRows * numberOfColumns];

  for (unsigned long i = 0; i < numberOfRows; ++i) {
    length[getIndex(i, 0, numberOfColumns)] = 0;
    route[getIndex(i, 0, numberOfColumns)] = 1;
  }
  for (unsigned long j = 0; j < numberOfColumns; ++j) {
    length[getIndex(0, j, numberOfColumns)] = 0;
    route[getIndex(0, j, numberOfColumns)] = -1;
  }

  for (unsigned long i = 1; i < numberOfRows; ++i) {
    for (unsigned long j = 1; j < numberOfColumns; ++j) {
      if (string1.at(i - 1) == string2.at(j - 1)) {
        length[getIndex(i, j, numberOfColumns)] = length[getIndex((i - 1), (j - 1), numberOfColumns)] + 1;
        route[getIndex(i, j, numberOfColumns)] = 0;
      } else {
        if (length[getIndex(i, (j - 1), numberOfColumns)] > length[getIndex((i - 1), j, numberOfColumns)]) {
          length[getIndex(i, j, numberOfColumns)] = length[getIndex(i, (j - 1), numberOfColumns)];
          route[getIndex(i, j, numberOfColumns)] = -1;
        } else {
          length[getIndex(i, j, numberOfColumns)] = length[getIndex((i - 1), j, numberOfColumns)];
          route[getIndex(i, j, numberOfColumns)] = 1;
        }
      }
    }
  }

  string subSequence = "";
  unsigned long i = (numberOfRows - 1), j = (numberOfColumns - 1);
  while ((i > 0) && (j > 0)) {
    if (route[getIndex(i, j, numberOfColumns)] == -1) {
      j--;
    } else if (route[getIndex(i, j, numberOfColumns)] == 0) {
      subSequence = string1.at(i - 1) + subSequence;
      i--;
      j--;
    } else {
      i--;
    }
  }

  delete[] length;
  delete[] route;

  return subSequence;
}

int main() {
  string string1, string2;
  cout << "Enter string 1: ";
  cin >> string1;
  cout << "Enter string 2: ";
  cin >> string2;

  string subSequence = longestCommonSubSequence(string1, string2);

  cout << "Longest Common SubSequence: " << subSequence << endl;

  return 0;
}