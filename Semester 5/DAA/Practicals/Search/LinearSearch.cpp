#include <iostream>
#include <sys/time.h>
#include <cstdlib>

using namespace std;

double getTime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return ((t.tv_sec * 1000000) + t.tv_usec);
}

void populateElements(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = i;
  }
}

int worstCaseSearch(int *data, int size) {
  return data[size - 1];
}

int averageCaseSearch(int *data, int size) {
  srand((unsigned int) time(NULL));
  return data[rand() % size];
}

int bestCaseSearch(int *data, int size) {
  return data[0];
}

double linearSearch(int *data, int size, int n) {
  double startTime, endTime;
  int index = -1;

  startTime = getTime();

  for (int i = 0; i < size; ++i) {
    if (data[i] == n) {
      index = i;
      break;
    }
  }

  endTime = getTime();

  return (endTime - startTime);
}

int main() {
  int *data, size, n;
  cout << "Enter problem size: ";
  cin >> size;
  data = new int[size];

  populateElements(data, size);

  n = worstCaseSearch(data, size);
  double worstCaseTime = linearSearch(data, size, n);
  cout << "Worst case: " << worstCaseTime << endl;

  n = averageCaseSearch(data, size);
  double averageCaseTime = linearSearch(data, size, n);
  cout << "Average case: " << averageCaseTime << endl;

  n = bestCaseSearch(data, size);
  double bestCaseTime = linearSearch(data, size, n);
  cout << "Best case: " << bestCaseTime << endl;

  return 0;
}