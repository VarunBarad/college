#include <iostream>
#include <sys/time.h>
#include <cstdlib>

using namespace std;

double getTime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return ((t.tv_sec * 1000000) + t.tv_usec);
}

void populateWorstCase(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = size - i;
  }
}

void populateAverageCase(int *data, int size) {
  srand((unsigned int) time(NULL));
  for (int i = 0; i < size; ++i) {
    data[i] = rand() % size;
  }
}

void populateBestCase(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = i;
  }
}

double selectionSort(int *data, int size) {
  double startTime, endTime;
  int min, temp;
  bool isSorted = true;

  startTime = getTime();

  for (int i = 0; ((isSorted) && (i < size - 1)); ++i) {
    if (data[i] > data[i + 1]) {
      isSorted = false;
      break;
    }
  }

  for (int i = 0; ((!isSorted) && (i < size - 1)); ++i) {
    min = i;
    for (int j = i + 1; j < size; ++j) {
      if (data[min] > data[j]) {
        min = j;
      }
    }
    temp = data[min];
    data[min] = data[i];
    data[i] = temp;
  }

  endTime = getTime();

  return endTime - startTime;
}

int main() {
  int *data, size;
  cout << "Enter problem size: ";
  cin >> size;
  data = new int[size];

  populateWorstCase(data, size);
  double worstCaseTime = selectionSort(data, size);
  cout << "Worst case: " << worstCaseTime << endl;

  populateAverageCase(data, size);
  double averageCaseTime = selectionSort(data, size);
  cout << "Average case: " << averageCaseTime << endl;

  populateBestCase(data, size);
  double bestCaseTime = selectionSort(data, size);
  cout << "Best case: " << bestCaseTime << endl;

  return 0;
}
