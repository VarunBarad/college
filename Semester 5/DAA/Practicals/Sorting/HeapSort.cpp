#include <iostream>
#include <sys/time.h>
#include <cstdlib>

using namespace std;

double getTime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return ((t.tv_sec * 1000000) + t.tv_usec);
}

void populateWorstCase(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = i;
  }
}

void populateAverageCase(int *data, int size) {
  srand((unsigned int) time(NULL));
  for (int i = 0; i < size; ++i) {
    data[i] = rand() % size;
  }
}

void populateBestCase(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = size - i;
  }
}

int getParent(int index) {
  return ((index / 2) - (1 - (index % 2)));
}

int getLeftChild(int index) {
  return ((2 * index) + 1);
}

int getRightChild(int index) {
  return ((2 * index) + 2);
}

void swap(int *data, int size, int i, int j) {
  if ((i < size) && (j < size)) {
    int temp = data[i];
    data[i] = data[j];
    data[j] = temp;
  }
}

void heapify(int *data, int size, int index) {
  int left = getLeftChild(index);
  int right = getRightChild(index);

  int max;
  if (left < size) {
    if (right < size) {
      max = (data[index] < data[left]) ? ((data[left] < data[right]) ? (right) : (left)) : ((data[index] < data[right]) ? (right) : (index));
    } else {
      max = (data[index] < data[left]) ? (left) : (index);
    }
    if (max != index) {
      swap(data, size, index, max);
      heapify(data, size, max);
    }
  }
}

void buildMaxHeap(int *data, int size) {
  int index = ((size / 2) - 1);
  for (int i = index; i > -1; i--) {
    heapify(data, size, i);
  }
}

double heapSort(int *data, int size) {
  double startTime, endTime;

  startTime = getTime();

  buildMaxHeap(data, size);
  for (int i = 1; i < size; ++i) {
    swap(data, size, 0, (size - i));
    heapify(data, (size - i), 0);
  }

  endTime = getTime();

  return endTime - startTime;
}

void display(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    cout << data[i] << " ";
  }
  cout << endl;
}

int main() {
  int *data, size;
  cout << "Enter problem size: ";
  cin >> size;
  data = new int[size];

  populateWorstCase(data, size);
  double worstCaseTime = heapSort(data, size);
  cout << "Worst case: " << worstCaseTime << endl;

  populateAverageCase(data, size);
  double averageCaseTime = heapSort(data, size);
  cout << "Average case: " << averageCaseTime << endl;

  populateBestCase(data, size);
  double bestCaseTime = heapSort(data, size);
  cout << "Best case: " << bestCaseTime << endl;

  delete[] data;

  return 0;
}
