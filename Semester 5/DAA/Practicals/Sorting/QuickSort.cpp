#include <iostream>
#include <sys/time.h>
#include <cstdlib>

using namespace std;

double getTime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return ((t.tv_sec * 1000000) + t.tv_usec);
}

void populateWorstCase(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = i;
  }
}

void populateAverageCase(int *data, int size) {
  srand((unsigned int) time(NULL));
  for (int i = 0; i < size; ++i) {
    data[i] = rand() % size;
  }
}

void populateBestCase(int *data, int size) {
  srand((unsigned int) time(NULL));
  for (int i = 0; i < size; ++i) {
    data[i] = rand() % size;
  }
}

void swap(int *data, int size, int i, int j) {
  if ((i < size) && (j < size)) {
    int temp = data[i];
    data[i] = data[j];
    data[j] = temp;
  }
}

int partition(int *data, int size, int low, int high) {
  int pivot = data[high];
  int i = low - 1;
  for (int j = low; j < high; ++j) {
    if (data[j] < pivot) {
      i++;
      swap(data, size, i, j);
    }
  }
  swap(data, size, (i + 1), high);
  return (i + 1);
}

void quickSort_sort(int *data, int size, int low, int high) {
  if (low < high) {
    int part = partition(data, size, low, high);
    quickSort_sort(data, size, low, (part - 1));
    quickSort_sort(data, size, (part + 1), high);
  }
}

double quickSort(int *data, int size) {
  double startTime, endTime;

  startTime = getTime();

  quickSort_sort(data, size, 0, (size - 1));

  endTime = getTime();

  return (endTime - startTime);
}

int main() {
  int *data, size;
  cout << "Enter problem size: ";
  cin >> size;
  data = new int[size];

  populateWorstCase(data, size);
  double worstCaseTime = quickSort(data, size);
  cout << "Worst case: " << worstCaseTime << endl;

  populateAverageCase(data, size);
  double averageCaseTime = quickSort(data, size);
  cout << "Average case: " << averageCaseTime << endl;

  populateBestCase(data, size);
  double bestCaseTime = quickSort(data, size);
  cout << "Best case: " << bestCaseTime << endl;

  delete[] data;

  return 0;
}
