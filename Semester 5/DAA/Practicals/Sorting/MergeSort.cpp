#include <iostream>
#include <sys/time.h>
#include <cstdlib>

using namespace std;

double getTime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return ((t.tv_sec * 1000000) + t.tv_usec);
}

void populateWorstCase(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = size - i;
  }
}

void populateAverageCase(int *data, int size) {
  srand((unsigned int) time(NULL));
  for (int i = 0; i < size; ++i) {
    data[i] = rand() % size;
  }
}

void populateBestCase(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = i;
  }
}

void sort(int *data, int start, int stop) {
  if (start < stop) {
    int middle = (start + stop) / 2;
    sort(data, start, middle);
    sort(data, (middle + 1), stop);

    int m = middle - start + 1;
    int n = stop - middle;
    int *left = new int[m];
    int *right = new int[n];
    for (int i = 0; i < m; ++i) {
      left[i] = data[start + i];
    }
    for (int i = 0; i < n; ++i) {
      right[i] = data[middle + 1 + i];
    }
    int i, j, k;
    i = 0;
    j = 0;
    k = 0;
    while ((i < m) && (j < n)) {
      if (left[i] < right[j]) {
        data[start + k] = left[i];
        ++i;
      } else {
        data[start + k] = right[j];
        ++j;
      }
      ++k;
    }
    if (i == m) {
      while (j < n) {
        data[start + k] = right[j];
        ++j;
        ++k;
      }
    } else {
      while (i < m) {
        data[start + k] = left[i];
        ++i;
        ++k;
      }
    }
    delete[](left);
    delete[](right);
  }
}

double mergeSort(int *data, int size) {
  double startTime, endTime;

  startTime = getTime();

  sort(data, 0, (size - 1));

  endTime = getTime();

  return (endTime - startTime);
}

int main() {
  int *data, size;
  cout << "Enter problem size: ";
  cin >> size;
  data = new int[size];

  populateWorstCase(data, size);
  double worstCaseTime = mergeSort(data, size);
  cout << "Worst case: " << worstCaseTime << endl;

  populateAverageCase(data, size);
  double averageCaseTime = mergeSort(data, size);
  cout << "Average case: " << averageCaseTime << endl;

  populateBestCase(data, size);
  double bestCaseTime = mergeSort(data, size);
  cout << "Best case: " << bestCaseTime << endl;

  return 0;
}
