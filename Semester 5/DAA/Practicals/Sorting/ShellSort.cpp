#include <iostream>
#include <cmath>
#include <sys/time.h>
#include <cstdlib>

using namespace std;

double getTime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return ((t.tv_sec * 1000000) + t.tv_usec);
}

void populateWorstCase(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = size - i;
  }
}

void populateAverageCase(int *data, int size) {
  srand((unsigned int) time(NULL));
  for (int i = 0; i < size; ++i) {
    data[i] = rand() % size;
  }
}

void populateBestCase(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = i;
  }
}

int getGapListSize(int size) {
  int gapListSize;
  for (gapListSize = 1; (pow(2, gapListSize) - 1) < size; gapListSize++);
  return (gapListSize - 1);
}

void populateGapList(int *gapList, int size) {
  for (int i = 0; i < size; ++i) {
    gapList[i] = pow(2, (i + 1)) - 1;
  }
}

void insertionSort(int *data, int size, int gap, int start) {
  int temp;
  for (int i = (start + gap); i < size; i += gap) {
    temp = data[i];
    int j;
    for (j = i; ((data[j - gap] > temp) && (j > (start + gap - 1))); j -= gap) {
      data[j] = data[j - gap];
    }
    data[j] = temp;
  }
}

double shellSort(int *data, int size) {
  double startTime, endTime;

  startTime = getTime();

  int gapListSize = getGapListSize(size);
  int *gaps = new int[gapListSize];
  populateGapList(gaps, gapListSize);

  for (int i = 0; i < gapListSize; ++i) {
    for (int j = 0; j < gaps[i]; ++j) {
      insertionSort(data, size, gaps[i], j);
    }
  }

  endTime = getTime();

  return endTime - startTime;
}

int main() {
  int *data, size;
  cout << "Enter problem size: ";
  cin >> size;
  data = new int[size];

  populateWorstCase(data, size);
  double worstCaseTime = shellSort(data, size);
  cout << "Worst case: " << worstCaseTime << endl;

  populateAverageCase(data, size);
  double averageCaseTime = shellSort(data, size);
  cout << "Average case: " << averageCaseTime << endl;

  populateBestCase(data, size);
  double bestCaseTime = shellSort(data, size);
  cout << "Best case: " << bestCaseTime << endl;

  delete[] data;

  return 0;
}
