#include <iostream>
#include <sys/time.h>
#include <cstdlib>

using namespace std;

double getTime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return ((t.tv_sec * 1000000) + t.tv_usec);
}

void populateWorstCase(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = size - i;
  }
}

void populateAverageCase(int *data, int size) {
  srand((unsigned int) time(NULL));
  for (int i = 0; i < size; ++i) {
    data[i] = rand() % size;
  }
}

void populateBestCase(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = i;
  }
}

double bubbleSort(int *data, int size) {
  double startTime, endTime;
  int temp;
  bool isSorted = false;

  startTime = getTime();

  for (int i = 0; ((!isSorted) && (i < size - 1)); ++i) {
    isSorted = true;
    for (int j = 0; j < size - i - 1; ++j) {
      if (data[j] > data[j + 1]) {
        isSorted = false;
        temp = data[j];
        data[j] = data[j + 1];
        data[j + 1] = temp;
      }
    }
  }

  endTime = getTime();

  return endTime - startTime;
}

int main() {
  int *data, size;
  cout << "Enter problem size: ";
  cin >> size;
  data = new int[size];

  populateWorstCase(data, size);
  double worstCaseTime = bubbleSort(data, size);
  cout << "Worst case: " << worstCaseTime << endl;

  populateAverageCase(data, size);
  double averageCaseTime = bubbleSort(data, size);
  cout << "Average case: " << averageCaseTime << endl;

  populateBestCase(data, size);
  double bestCaseTime = bubbleSort(data, size);
  cout << "Best case: " << bestCaseTime << endl;

  return 0;
}
