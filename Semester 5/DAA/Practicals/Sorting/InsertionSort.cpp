#include <iostream>
#include <sys/time.h>
#include <cstdlib>

using namespace std;

double getTime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return ((t.tv_sec * 1000000) + t.tv_usec);
}


void populateWorstCase(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = size - i;
  }
}

void populateAverageCase(int *data, int size) {
  srand((unsigned int) time(NULL));
  for (int i = 0; i < size; ++i) {
    data[i] = rand() % size;
  }
}

void populateBestCase(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = i;
  }
}

double insertionSort(int *data, int size) {
  double startTime, endTime;
  int temp;
  bool isSorted = true;

  startTime = getTime();

  for (int i = 0; ((isSorted) && (i < size - 1)); ++i) {
    if (data[i] > data[i + 1]) {
      isSorted = false;
      break;
    }
  }

  for (int i = 1; i < size; ++i) {
    temp = data[i];
    int j;
    for (j = i; ((data[j - 1] > temp) && (j > 0)); --j) {
      data[j] = data[j - 1];
    }
    data[j] = temp;
  }

  endTime = getTime();

  return endTime - startTime;
}

int main() {
  int *data, size;
  cout << "Enter problem size: ";
  cin >> size;
  data = new int[size];

  populateWorstCase(data, size);
  double worstCaseTime = insertionSort(data, size);
  cout << "Worst case: " << worstCaseTime << endl;

  populateAverageCase(data, size);
  double averageCaseTime = insertionSort(data, size);
  cout << "Average case: " << averageCaseTime << endl;

  populateBestCase(data, size);
  double bestCaseTime = insertionSort(data, size);
  cout << "Best case: " << bestCaseTime << endl;

  return 0;
}
