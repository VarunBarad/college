#include <iostream>
#include <sys/time.h>
#include <cstdlib>

using namespace std;

double getTime() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return ((t.tv_sec * 1000000) + t.tv_usec);
}

void populateWorstCase(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = size - i;
  }
}

void populateAverageCase(int *data, int size) {
  srand((unsigned int) time(NULL));
  for (int i = 0; i < size; ++i) {
    data[i] = rand() % size;
  }
}

void populateBestCase(int *data, int size) {
  for (int i = 0; i < size; ++i) {
    data[i] = i;
  }
}

int getMinimum(int *data, int size) {
  int min = -1;
  if (size > 0) {
    min = 0;
    for (int i = 1; i < size; ++i) {
      if (data[i] < data[min]) {
        min = i;
      }
    }
  }
  return min;
}

int getMaximum(int *data, int size) {
  int max = -1;
  if (size > 0) {
    max = 0;
    for (int i = 1; i < size; ++i) {
      if (data[i] > data[max]) {
        max = i;
      }
    }
  }
  return max;
}

void countElements(int *data, int size, int *count, int range, int min) {
  for (int i = 0; i < size; ++i) {
    count[data[i] - min]++;
  }
  for (int i = 1; i < range; ++i) {
    count[i] += count[i-1];
  }
}

double countingSort(int *data, int size) {
  double startTime, endTime;

  startTime = getTime();

  int minimumElement = data[getMinimum(data, size)];
  int maximumElement = data[getMaximum(data, size)];
  int range = maximumElement - minimumElement + 1;

  int *count = new int[range];
  for (int i = 0; i < range; ++i) {
    count[i] = 0;
  }

  countElements(data, size, count, range, minimumElement);

  int *temp = new int[size];

  for (int i = (size - 1); i > -1; --i) {
    temp[count[data[i] - minimumElement] - 1] = data[i];
    count[data[i] - minimumElement]--;
  }
  for (int i = 0; i < size; ++i) {
    data[i] = temp[i];
  }

  delete[] temp;

  endTime = getTime();

  return (endTime - startTime);
}

int main() {
  int *data, size;
  cout << "Enter problem size: ";
  cin >> size;
  data = new int[size];

  populateWorstCase(data, size);
  double worstCaseTime = countingSort(data, size);
  cout << "Worst case: " << worstCaseTime << endl;

  populateAverageCase(data, size);
  double averageCaseTime = countingSort(data, size);
  cout << "Average case: " << averageCaseTime << endl;

  populateBestCase(data, size);
  double bestCaseTime = countingSort(data, size);
  cout << "Best case: " << bestCaseTime << endl;

  delete[] data;

  return 0;
}
