#include <iostream>
#include <cstdlib>

using namespace std;

class Item {
private:
  int value;
  int weight;
public:
  Item() {
    this->value = 0;
    this->weight = 0;
  }

  Item(int value, int weight) {
    this->value = value;
    this->weight = weight;
  }

  int getValue() {
    return this->value;
  }

  void setValue(int value) {
    this->value = value;
  }

  int getWeight() {
    return this->weight;
  }

  void setWeight(int weight) {
    this->weight = weight;
  }

  double getRatio() {
    return (((double) this->value) / ((double) this->weight));
  }
};

void populateItems(Item *items, int itemCount, int maximumWeight) {
  int weightStep = maximumWeight / itemCount;
  int valueStep = maximumWeight;
  int randomWeight, randomValue;

  srand((unsigned int) time(NULL));
  for (int i = 0; i < itemCount; ++i) {
    randomWeight = (rand() % ((weightStep * i) + 1)) + (weightStep * (i + 1));
    randomValue = (rand() % ((valueStep * i) + 1)) + (valueStep * (i + 1));
    items[i].setValue(randomValue);
    items[i].setWeight(randomWeight);
  }
}

void sortItems(Item *items, int itemCount) {
  Item temp;
  int max;
  for (int i = 0; i < itemCount - 1; ++i) {
    max = i;
    for (int j = i + 1; j < itemCount; ++j) {
      if (items[max].getRatio() < items[j].getRatio()) {
        max = j;
      }
    }
    temp = items[max];
    items[max] = items[i];
    items[i] = temp;
  }
}

int knapSack(Item *items, int itemCount, int maximumWeight) {
  int currentWeight = 0, currentValue = 0, i = 0;

  while ((i < itemCount) && ((currentWeight + items[i].getWeight()) <= maximumWeight)) {
    currentValue += items[i].getValue();
    currentWeight += items[i].getWeight();
    i++;
  }

  if ((i < itemCount) && (currentWeight < maximumWeight)) {
    double ratio = ((double) (maximumWeight - currentWeight)) / ((double) items[i].getWeight());
    currentValue += (items[i].getValue() * ratio);
    currentWeight += (items[i].getWeight() * ratio);
  }

  return currentValue;
}

int main() {
  int itemCount, maximumWeight;
  Item *items;
  cout << "Enter the number of items: ";
  cin >> itemCount;
  cout << "Enter the maximum weight: ";
  cin >> maximumWeight;

  items = new Item[itemCount];
  populateItems(items, itemCount, maximumWeight);

  sortItems(items, itemCount);

  double knapSackValue = knapSack(items, itemCount, maximumWeight);

  cout << "Knap Sack Value: " << knapSackValue << endl;

  delete[] items;
  return 0;
}