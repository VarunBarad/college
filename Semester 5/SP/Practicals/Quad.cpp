#include <iostream>
#include <cstdlib>

using namespace std;

class Stack {
private:
  string stack[50];
  int top;
public:
  Stack() {
    this->top = -1;
  }

  void push(string s) {
    if (top < 49) {
      stack[++top] = s;
    }
  }

  string pop() {
    if (top > -1) {
      return stack[top--];
    } else {
      return "";
    }
  }

  bool isEmpty() {
    return top <= -1;
  }
};

string itos(int n) {
  if (n == 0) {
    return "0";
  } else {
    string s = "";
    while (n > 0) {
      s += ((char) (n % 10) + 48);
      n /= 10;
    }
    string s2 = "";
    for (unsigned long i = 0; i < s.length(); i++) {
      s2 += s.at(s.length() - i - 1);
    }
    return s2;
  }
}

int main() {
  string postfix = "zx2y2/x4x+^*+*=";
  Stack s1, s2;
  for (unsigned long i = 0; i < postfix.length(); ++i) {
    string temp = "";
    temp += postfix.at(postfix.length() - i - 1);
    s1.push(temp);
  }
  int count = 0;
  while (!s1.isEmpty()) {
    string temp = s1.pop();
    while (!(temp == "+" || temp == "-" || temp == "*" || temp == "/" || temp == "^" || temp == "=")) {
      s2.push(temp);
      temp = s1.pop();
    }
    string op1 = s2.pop(), op2 = s2.pop(), op = temp;
    temp = "t" + itos(count++);
    cout << op << "\t" << op1 << "\t" << op2 << "\t" << temp << endl;
    if (!s1.isEmpty() && !s2.isEmpty()) {
      s1.push(temp);
    }
    while (!s2.isEmpty()) {
      s1.push(s2.pop());
    }
  }

  return 0;
}