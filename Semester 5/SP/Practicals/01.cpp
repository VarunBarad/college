#include <iostream>

using namespace std;

int get_count(string s, char c) {
  int count = 0;
  unsigned long length = s.length();
  for (unsigned long i = 0; i < length; ++i) {
    if (s.at(i) == c) {
      count++;
    }
  }
  return count;
}

void split(string source, char separator, string *parts) {
  unsigned long length = source.length();
  for (int i = 0, j = 0; i < length; ++i, ++j) {
    parts[j] = "";
    while ((i < length) && (source.at(i) != separator)) {
      parts[j] += source.at(i);
      ++i;
    }
  }
}

bool contains(string s, string *list, int size) {
  for (int i = 0; i < size; ++i) {
    if (!s.compare(list[i])) {
      return true;
    }
  }
  return false;
}

int main() {
  string keywords[3] = {"int", "float", "double"};
  string meta_characters[3] = {",", ";", ":"};
  string operators[6] = {"+", "-", "*", "/", "=", "%"};

  string s = "c = a + b ;";
  int size = get_count(s, ' ') + 1;
  string *parts = new string[size];
  split(s, ' ', parts);
  for (int i = 0; i < size; ++i) {
    if (contains(parts[i], keywords, 3)) {
      cout << parts[i] << " is a keyword" << endl;
    } else if (contains(parts[i], meta_characters, 3)) {
      cout << parts[i] << " is a meta-character" << endl;
    } else if (contains(parts[i], operators, 6)) {
      cout << parts[i] << " is an operator" << endl;
    } else {
      cout << parts[i] << " is a variable" << endl;
    }
  }
  delete []parts;
  return 0;
}
