#include <iostream>
#include <fstream>

using namespace std;

unsigned long ULONG_MAX = 18446744073709551615;

class Symbol {
public:
  string symbol;
  unsigned long address;

  Symbol() {
    this->symbol = "";
    this->address = 0;
  }
};

class Literal {
public:
  string literal;
  unsigned long address;

  Literal() {
    this->literal = "";
    this->address = 0;
  }
};

unsigned long countLines(string fileName) {
  ifstream inputFile;
  inputFile.open(fileName, ios::in);
  unsigned long count = 0;
  if (inputFile.is_open()) {
    string s;
    while (!inputFile.eof()) {
      getline(inputFile, s);
      count++;
    }
    inputFile.close();
  }
  return count;
}

string *getContents(string fileName, unsigned long numberOfLines) {
  string *lines = new string[numberOfLines];
  ifstream inputFile;
  inputFile.open(fileName, ios::in);
  if (inputFile.is_open()) {
    for (unsigned long i = 0; i < numberOfLines; ++i) {
      getline(inputFile, lines[i]);
    }
    inputFile.close();
  } else {
    for (unsigned long i = 0; i < numberOfLines; ++i) {
      lines[i] = "";
    }
  }
  return lines;
}

unsigned long countLiterals(string *lines, unsigned long numberOfLines) {
  unsigned long count = 0;
  for (unsigned long i = 0; i < numberOfLines; ++i) {
    if (lines[i].find('=') < ULONG_MAX) {
      count++;
    }
  }
  return count;
}

unsigned long countSymbols(string *lines, unsigned long numberOfLines) {
  unsigned long count = 0;
  for (unsigned long i = 0; i < numberOfLines; ++i) {
    if (lines[i].find(':') < ULONG_MAX) {
      count++;
    } else if (lines[i].find("DS") < ULONG_MAX) {
      count++;
    }
  }
  return count;
}

Literal *findLiterals(string *lines, unsigned long numberOfLines, unsigned long numberOfLiterals) {
  Literal *literals = new Literal[numberOfLiterals];
  unsigned long literalIndex = 0;
  unsigned long temp;
  for (unsigned long i = 0; i < numberOfLines; ++i) {
    temp = lines[i].find('=');
    if (temp < ULONG_MAX) {
      string tempString = "";
      temp += 2;
      while (lines[i].at(temp) != '\'') {
        tempString += lines[i].at(temp);
        temp++;
      }
      literals[literalIndex].literal = tempString;
      literals[literalIndex].address = i;
      literalIndex++;
    }
  }
  return literals;
}

Symbol *findSymbols(string *lines, unsigned long numberOfLines, unsigned long numberOfSymbols) {
  Symbol *symbols = new Symbol[numberOfSymbols];
  unsigned long symbolIndex = 0;
  for (unsigned long i = 0; i < numberOfLines; ++i) {
    if (lines[i].find(':') < ULONG_MAX) {
      symbols[symbolIndex].symbol = lines[i].substr(0, lines[i].find(':'));
      symbols[symbolIndex].address = i;
      symbolIndex++;
    } else if (lines[i].find("DS") < ULONG_MAX) {
      symbols[symbolIndex].symbol = lines[i].substr(0, lines[i].find(' '));
      symbols[symbolIndex].address = i;
      symbolIndex++;
    }
  }
  return symbols;
}

int main() {
  const string fileName = "/home/vbarad/Documents/college/Semester 5/SP/Playground/assembly.asm";
  unsigned long numberOfLines, numberOfSymbols, numberOfLiterals;

  numberOfLines = countLines(fileName);
  string *lines = getContents(fileName, numberOfLines);

  numberOfLiterals = countLiterals(lines, numberOfLines);
  numberOfSymbols = countSymbols(lines, numberOfLines);

  Literal *literals = findLiterals(lines, numberOfLines, numberOfLiterals);
  Symbol *symbols = findSymbols(lines, numberOfLines, numberOfSymbols);

  cout << "Symbols" << endl;
  for (unsigned long i = 0; i < numberOfSymbols; i++) {
    cout << symbols[i].symbol << "\t" << symbols[i].address << endl;
  }
  cout << "Literals" << endl;
  for (unsigned long i = 0; i < numberOfSymbols; i++) {
    cout << literals[i].literal << "\t" << literals[i].address << endl;
  }
  return 0;
}