#include <iostream>
#include <regex>

using namespace std;

class Set {
private:
  char *set;
  unsigned long size;
  unsigned long currentSize;
public:
  Set(unsigned long size) {
    this->size = size;
    this->currentSize = 0;
    this->set = new char[size];
    for (unsigned long i = 0; i < size; ++i) {
      this->set[i] = '\0';
    }
  }

  bool add(char element) {
    for (unsigned long i = 0; i < currentSize; ++i) {
      if (this->set[i] == element) {
        return false;
      }
    }
    this->set[this->currentSize] = element;
    this->currentSize++;
    return true;
  }

  unsigned long getSetSize() {
    return this->currentSize;
  }

  char getElement(unsigned long index) {
    if (index < this->currentSize) {
      return this->set[index];
    } else {
      return '\0';
    }
  }

  void clear() {
    for (unsigned long i = 0; i < this->currentSize; ++i) {
      this->set[currentSize] = '\0';
    }
    this->currentSize = 0;
  }
};

void findFirst(string grammars[7], char nonTerminal, Set *first) {
  for (int i = 0; i < 7; ++i) {
    if (grammars[i].at(0) == nonTerminal) {
      if ((grammars[i].at(2) >= 'A') && (grammars[i].at(2) <= 'Z')) {
        findFirst(grammars, grammars[i].at(2), first);
      } else {
        first->add(grammars[i].at(2));
      }
    }
  }
}

int main() {
  string grammars[7] = {
      "S ABE",
      "S a",
      "S f",
      "A p",
      "A t",
      "A w",
      "B Aq"
  };

  Set nonTerminals(10), terminals(10);

  // Find terminals and non-terminals
  for (int i = 0; i < 7; ++i) {
    nonTerminals.add(grammars[i].at(0));
    for (unsigned long j = 2; j < grammars[i].length(); ++j) {
      char temp = grammars[i].at(j);
      if ((temp >= 'A') && (temp <= 'Z')) {
        nonTerminals.add(temp);
      } else {
        terminals.add(temp);
      }
    }
  }

  cout << "Terminals:" << endl;
  for (unsigned long i = 0; i < terminals.getSetSize(); ++i) {
    cout << terminals.getElement(i) << endl;
  }

  cout << endl << endl;

  cout << "Non-terminals:" << endl;
  for (unsigned long i = 0; i < nonTerminals.getSetSize(); ++i) {
    cout << nonTerminals.getElement(i) << endl;
  }

  cout << endl << endl;

  // Find first of each non-terminal
  unsigned long nonTerminalCount = nonTerminals.getSetSize();
  for (unsigned long i = 0; i < nonTerminalCount; ++i) {
    Set temp(10);
    findFirst(grammars, nonTerminals.getElement(i), &temp);
    unsigned long firstSize = temp.getSetSize();
    if (firstSize > 0) {
      cout << nonTerminals.getElement(i) << "\t->\t{";
      for (unsigned long j = 0; j < firstSize; ++j) {
        cout << temp.getElement(j);
        if (j < (firstSize - 1)) {
          cout << ", ";
        }
      }
      cout << "}" << endl;
    } else {
      cout << nonTerminals.getElement(i) << "\t->\t{}" << endl;
    }
  }

  return 0;
}