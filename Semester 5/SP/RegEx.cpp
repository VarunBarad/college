#include <iostream>
#include <regex>

using namespace std;

int main(int argc, char *argv[]) {
  string input = "V";
  if (argc > 1) {
    input = argv[1];
  }

  regex vowels("[aeiouAEIOU]");

  cout << "Input string: " << input << endl;
  if (regex_match(input, vowels)) {
    cout << "Input string matches the regex pattern" << endl;
  } else {
    cout << "Pattern match not found in input string" << endl;
  }
  return 0;
}