//
// Created by vbarad on 21/4/15.
//

#include "iostream"
#include "cmath"

using namespace std;

namespace que_01 {
    double d1, d2;

    void readNumbers() {
        cin >> d1 >> d2;
    }

    double getResult() {
        double r;
        if (d2 == 0.0) {
            throw 0;
        } else {
            r = d1 / d2;
            if (r < 0.0) {
                throw 1;
            } else {
                r = sqrt(r);
                return r;
            }
        }
    }

    void main() {
        double result;
        readNumbers();
        try {
            result = getResult();
            cout << "Result: " << result << endl;
        } catch (int err) {
            if (err == 0) {
                cout << "Divide-By-Zero" << endl;
            } else if (err == 1) {
                cout << "Complex root" << endl;
            }
        }
    }
}
