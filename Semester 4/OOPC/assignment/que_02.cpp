//
// Created by vbarad on 20/4/15.
//

#include "iostream"
#include "fstream"
#include "iomanip"

using namespace std;

struct contact {
    string name;
    string number;
    contact *next;
};

class ArrayList {
private:
    contact *root;
    int maxName;
    int maxNumber;
public:
    ArrayList() {
        root = NULL;
        maxName = 0;
        maxNumber = 0;
    }

    int getMaxName() {
        return maxName;
    }

    void setMaxName(int m) {
        maxName = m;
    }

    int getMaxNumber() {
        return maxNumber;
    }

    void setMaxNumber(int m) {
        maxNumber = m;
    }

    int size() {
        contact *c = root;
        int i = 0;
        while (c != NULL) {
            i++;
            c = c->next;
        }
        return i;
    }

    contact get(int pos) {
        contact *c = root;
        int i = 0;
        while (i < pos) {
            i++;
            c = c->next;
        }
        return *c;
    }

    void append(contact a) {
        contact *c = new contact;
        c->name = a.name;
        c->number = a.number;
        c->next = NULL;
        if (root == NULL) {
            root = c;
        } else {
            contact *temp = root;
            while (temp->next != NULL) {
                temp = temp->next;
            }
            temp->next = c;
        }
    }

    void clear() {
        contact *temp;
        while (root != NULL) {
            temp = root;
            root = root->next;
            delete temp;
        }
    }
};

ArrayList readData() {
    ArrayList list;
    contact c;
    string s;
    c.next = NULL;
    ifstream input;
    input.open("/home/vbarad/Documents/college/Semester 4/OOPC/assignment/contacts.txt", ios::in);
    if (input.is_open()) {
        while (!input.eof()) {
            input >> s;
            if (s.length() > list.getMaxName()) {
                list.setMaxName(s.length());
            }
            c.name = s;
            input >> s;
            if (s.length() > list.getMaxNumber()) {
                list.setMaxNumber(s.length());
            }
            c.number = s;
            list.append(c);
        }
        input.close();
    } else {
        cout << "File not opened" << endl;
    }
    return list;
}

void displayData() {
    ArrayList list = readData();
    int size = list.size();
    int sizeName = list.getMaxName();
    int sizeNumber = list.getMaxNumber();
    for (int i = 0; i < size; i++) {
        cout << setw(sizeName) << left << list.get(i).name << " | " << setw(sizeNumber) << right <<
        list.get(i).number << endl;
    }
}
