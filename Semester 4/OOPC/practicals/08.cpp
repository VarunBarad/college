#include <iostream>

using namespace std;

class A {
    int n;
public:
    A(int x) {
        n = x;
    }
    int getN() {
        return n;
    }
    A operator++() {
        n++;
        return *this;
    }
    A operator++(int i) {
        A obj = *this;
        n++;
        return obj;
    }
};

int main() {
    A a(5);
    cout << "Original: " << a.getN() << endl;
    cout << "Post-Increment: " << (a++).getN() << endl;
    cout << "Pre-Increment: " << (++a).getN() << endl;
    return 0;
}
