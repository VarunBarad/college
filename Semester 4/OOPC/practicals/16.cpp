#include <iostream>

using namespace std;

int main() {
    int *a;
    a = new int;
    cout << "Memory assigned" << endl;
    cout << "Enter value to store: ";
    cin >> *a;
    cout << "a = " << *a << endl;
    delete a;
    cout << "Memory freed" << endl;
    return 0;
}
