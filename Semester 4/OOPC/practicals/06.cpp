#include <iostream>

using namespace std;

class A {
    int n;
public:
    A() {
        n = 0;
        cout << "Default Constructor" << endl;
    }
    A(int m) {
        n = m;
        cout << "Parametrised Constructor" << endl;
    }
    int getNumber() {
        return n;
    }
};

int main() {
    A a, b(5);
    cout << "A: " << a.getNumber() << endl;
    cout << "B: " << b.getNumber() << endl;
    return 0;
}
