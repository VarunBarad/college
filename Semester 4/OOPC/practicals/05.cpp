#include <iostream>

using namespace std;

class A {
public:
    A() {
        cout << "This is constructor" << endl;
    }
    ~A() {
        cout << "This is destructor" << endl;
    }
};

int main() {
    A a;
    return 0;
}
