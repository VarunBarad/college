#include <iostream>

using namespace std;

class A {
    int a;
public:
    int getA() {
        return a;
    }

    void setA(int n) {
        a = n;
    }
};

class B : public A {
    int b;
public:
    int getB() {
        return b;
    }

    void setB(int n) {
        b = n;
    }
};

class C : public B {
    int c;
public:
    int getC() {
        return c;
    }

    void setC(int n) {
        c = n;
    }
};

int main() {
    C c;
    c.setA(5);
    c.setB(10);
    c.setC(15);
    cout << "A: " << c.getA() << endl;
    cout << "B: " << c.getB() << endl;
    cout << "C: " << c.getC() << endl;
    return 0;
}
