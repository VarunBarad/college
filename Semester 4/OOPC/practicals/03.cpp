#include <iostream>

using namespace std;

class Complex {
    int real;
    int imaginary;
public:
    void getData() {
        cout << "Real: ";
        cin >> real;
        cout << "Imaginary: ";
        cin >> imaginary;
    }
    Complex add(Complex c) {
        Complex r;
        r.real = real + c.real;
        r.imaginary = imaginary + c.imaginary;
        return r;
    }
    string display() {
        string s="";
        s += to_string(real);
        s += ((imaginary < 0) ? (" - ") : (" + "));
        s += ((imaginary < 0) ? (to_string(-imaginary)) : (to_string(imaginary)));
        s += "i";
        return s;
    }
};

int main() {
    Complex a, b, c;
    cout << "Enter details of A:" << endl;
    a.getData();
    cout << "Enter details of B:" << endl;
    b.getData();
    c = a.add(b);
    cout << "C = " << c.display() << endl;
    return 0;
}
