#include <iostream>
#include <cstring>

using namespace std;

class Stack {
    int a[10];
    int top;
public:
    Stack() {
        top = -1;
    }
    void push(int n) {
        if (top < 9) {
            top++;
            a[top] = n;
            cout << n << " pushed on stack" << endl;
        } else {
            throw *(new string("OVERFLOW"));
        }
    }
    int pop() {
        int n = 0;
        if (top < 0) {
            throw *(new string("UNDERFLOW"));
        } else {
            n = a[top];
            top--;
        }
        return n;
    }
};

int main() {
    Stack stack;
    stack.push(1);
    stack.push(50);
    stack.push(2);
    cout << stack.pop() << " popped from stack" << endl;
    cout << stack.pop() << " popped from stack" << endl;
    return 0;
}
