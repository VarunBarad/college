#include <iostream>

using namespace std;

class A {
public:
    virtual void display() = 0;
};
class B : public A {
public:
    void display() {
        cout << "Contents of B" << endl;
    }
};
class C : public A {
public:
    void display() {
        cout << "Contents of C" << endl;
    }
};

int main() {
    A *a;
    B b;
    C c;
    a = &b;
    a->display();
    a = &c;
    a->display();
    return 0;
}
