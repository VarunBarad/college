#include <iostream>

using namespace std;

class Item {
    string name;
    float cost;
public:
    void getData() {
        cout << "Name: ";
        cin >> name;
        cout << "Cost: ";
        cin >> cost;
    }
    void display() {
        cout << "Name: " << name << endl;
        cout << "Cost: " << cost << endl;
    }
};

int main() {
    Item item;
    cout << "Enter details:" << endl;
    item.getData();
    cout << "Display" << endl;
    item.display();
    return 0;
}
