#include <iostream>

using namespace std;

class A {
    int a;
public:
    void A_in() {
        cout << "Enter A: ";
        cin >> a;
    }
    void A_out() {
        cout << "A: " << a << endl;
    }
};
class B : virtual public A {
    int b;
public:
    void B_in() {
        cout << "Enter B: ";
        cin >> b;
    }
    void B_out() {
        cout << "B: " << b << endl;
    }
};
class C : virtual public A {
    int c;
public:
    void C_in() {
        cout << "Enter C: ";
        cin >> c;
    }
    void C_out() {
        cout << "C: " << c << endl;
    }
};
class D : public B, public C {
    int d;
public:
    void D_in() {
        A_in();
        B_in();
        C_in();
        cout << "Enter D: ";
        cin >> d;
    }
    void D_out() {
        A_out();
        B_out();
        C_out();
        cout << "D: " << d << endl;
    }
};

int main() {
    D obj;
    obj.D_in();
    obj.D_out();
    return 0;
}
