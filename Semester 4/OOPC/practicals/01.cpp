#include <iostream>

using namespace std;

struct student {
    string name;
    float marks[3];
};

int main() {
    student s[3];
    float temp;
    cout << "Enter details:" << endl;
    for (int i = 0; i < 3; ++i) {
        cout << "Student " << (i+1) << endl;
        cout << "Name: ";
        cin >> s[i].name;
        for (int j = 0; j < 3; ++j) {
            cout << "Subject " << (j+1) << ": ";
            cin >> s[i].marks[j];
        }
    }
    cout << endl;
    cout << "Name    Sub. 1  Sub. 2  Sub. 2  Average" << endl;
    for (int i = 0; i < 3; ++i) {
        temp = 0.0;
        cout << s[i].name << "\t";
        for (int j = 0; j < 3; ++j) {
            cout << s[i].marks[j] << "\t\t";
            temp += s[i].marks[j];
        }
        temp /= 3.0;
        cout << temp << endl;
    }
    return 0;
}
