#include <iostream>

using namespace std;

int main() {
    int *a;
    a = (int *) malloc(sizeof(int));
    cout << "Memory assigned" << endl;
    cout << "Enter value to store: ";
    cin >> *a;
    cout << "a = " << *a << endl;
    free(a);
    cout << "Memory freed" << endl;
    return 0;
}
