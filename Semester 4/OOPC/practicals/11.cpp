#include <iostream>

using namespace std;

class Father {
    string name;
public:
    string getName() {
        return name;
    }

    void setName(string s) {
        name = s;
    }
};

class Mother {
    string name;
public:
    string getName() {
        return name;
    }

    void setName(string s) {
        name = s;
    }
};

class Child : private Father, private Mother {
    string name;
public:
    string getName() {
        return name;
    }

    void setName(string s) {
        name = s;
    }

    string getFatherName() {
        return Father::getName();
    }

    void setFatherName(string s) {
        Father::setName(s);
    }

    string getMotherName() {
        return Mother::getName();
    }

    void setMotherName(string s) {
        Mother::setName(s);
    }
};

int main() {
    Child c;
    c.setName("Harry");
    c.setFatherName("James");
    c.setMotherName("Lily");
    cout << "Child: " << c.getName() << endl;
    cout << "Father: " << c.getFatherName() << endl;
    cout << "Mother: " << c.getMotherName() << endl;
    return 0;
}
