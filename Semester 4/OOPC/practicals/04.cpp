#include <iostream>

using namespace std;

inline int max(int a, int b, int c) {
    return ((a > b) ? ((a > c) ? (a) : (c)) : ((b > c) ? (b) : (c)));
}

int main() {
    int a, b, c;
    cout << "Enter 3 numbers:" << endl;
    cin >> a >> b >> c;
    cout << "Maximum: " << max(a, b, c) << endl;
    return 0;
}
