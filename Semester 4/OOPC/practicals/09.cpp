#include <iostream>
#include <cstring>

using namespace std;

bool operator==(string a, string b) {
    char *x, *y;
    x = new char[a.length()+1];
    y = new char[b.length()+1];
    strcpy(x, a.c_str());
    strcpy(y, b.c_str());
    return !strcmp(x, y);
}

int main() {
    string a = "Varun";
    string b = "Barad";
    if (a == b) {
        cout << "Match" << endl;
    } else {
        cout << "No Match" << endl;
    }
    return 0;
}
