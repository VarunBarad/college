#include <iostream>
#include <fstream>

using namespace std;

int main() {
    ifstream ifs;
    ofstream ofs;
    int sum = 0, temp;
    ifs.open("/home/vbarad/inp", ios::in);
    if (ifs.is_open()) {
        ifs >> temp;
        sum += temp;
        ifs >> temp;
        sum += temp;
        ifs.close();
        ofs.open("/home/vbarad/out", ios::out);
        if (ofs.is_open()) {
            ofs << sum << endl;
            ofs.close();
            cout << "Process finished successfully" << endl;
        } else {
            cout << "Output file error" << endl;
        }
    } else {
        cout << "Input file error" << endl;
    }
    return 0;
}
