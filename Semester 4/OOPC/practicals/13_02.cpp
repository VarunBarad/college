#include <iostream>

using namespace std;

class Class {
    int a;
    int b;
public:
    void getData() {
        cout << "Enter A: ";
        cin >> a;
        cout << "Enter B: ";
        cin >> b;
    }
    friend int getSum(Class);
};

int getSum(Class c) {
    return (c.a+c.b);
}

int main() {
    Class c;
    c.getData();
    cout << "Sum: " << getSum(c) << endl;
    return 0;
}
