#include <iostream>

using namespace std;

int count_occurences(string s, char separator) {
  int count = 0;
  for (int i = 0; i < s.length(); ++i) {
    if (s.at(i) == separator) {
      count++;
    }
  }
  return count;
}

void split(string s, char separator, string parts[]) {
  for (int i = 0, j = 0; i < s.length(); ++i, ++j) {
    while ((i < s.length()) && (s.at(i) != separator)) {
      parts[j] += s.at(i);
      ++i;
    }
  }
}

bool present_in(string s, string set[], int count) {
  for (int i = 0; i < count; ++i) {
    if (!s.compare(set[i])) {
      return true;
    }
  }
  return false;
}

int main() {
  string keywords[2] = {"int", "float"};
  string meta_characters[2] = {",", ";"};
  string s = "int a ;";
  int count = count_occurences(s, ' ');
  string parts[++count];
  split(s, ' ', parts);
  for (int i = 0; i < count; ++i) {
    if (present_in(parts[i], keywords, 2)) {
      cout << parts[i] << " is a keyword" << endl;
    } else if (present_in(parts[i], meta_characters, 2)) {
      cout << parts[i] << " is a meta-character" << endl;
    } else {
      cout << parts[i] << " is a variable" << endl;
    }
  }
  return 0;
}