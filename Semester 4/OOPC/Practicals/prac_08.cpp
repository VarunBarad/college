//
// Created by vbarad on 22/4/15.
//

#include <iostream>
#include <string>

using namespace std;

namespace prac_08 {
    bool operator==(string a, string b) {
        if (a.length() != b.length()) {
            return false;
        } else {
            for (int i = 0; i < a.length(); i++) {
                if (a[i] != b[i]) {
                    return false;
                }
            }
            return true;
        }
    }

    void prac_08() {
        cout << ("abcd" == "abcd") << endl;
    }
}