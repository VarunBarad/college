//
// Created by vbarad on 24/4/15.
//

#include "iostream"

using namespace std;

namespace prac_09 {
    class Trapezium {
    private:
        int l1, l2;
        int h;
    public:
        Trapezium(int a, int b, int c) {
            l1 = a;
            l2 = b;
            h = c;
        }
        virtual int getArea() {
            return (0.5 * (l1+l2) * h);
        }
    };
    class Rectangle : public Trapezium {
    private:
        int l;
        int b;
    public:
        Rectangle(int x, int y) {
            l = x;
            b = y;
        }
        int getArea() {
            return (l * b);
        }
    };
    class Square : public Trapezium {
    private:
        int s;
    public:
        Square(int a) {
            s = a;
        }
        int getArea() {
            return (s * s);
        }
    };

    void prac_09() {
        Rectangle rectangle(88, 66);
        Square square(91);
        cout << "Rectangle: " << rectangle.getArea() << endl;
        cout << "Square: " << square.getArea() << endl;
    }
}