//
// Created by vbarad on 19/4/15.
//

#include <iostream>
#include <string>

using namespace std;

namespace prac_01
{
    class Item
    {
    private:
        string name;
        float cost;

    public:
        void setData()
        {
            cout << "Enter name of item: ";
            cin >> name ;
            cout << "Enter cost of item: ";
            cin >> cost ;
            cout << endl ;
        }

        void display()
        {
            cout << "Item Name: " << name << endl ;
            cout << "Item Cost: " << cost << endl ;
        }
    };

    void prac_01()
    {
        Item item;
        item.setData();
        item.display();
    }
}
