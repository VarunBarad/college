//
// Created by vbarad on 28/4/15.
//

#include "iostream"

using namespace std;

namespace prac_10 {
    class Match {
    private:
        int runs;
        int catches;
        int wickets;
    public:
        int getRuns() {
            return runs;
        }

        void setRuns(int r) {
            runs = r;
        }

        int getCatches() {
            return catches;
        }

        void setCatches(int c) {
            catches = c;
        }

        int getWickets() {
            return wickets;
        }

        void setWickets(int w) {
            wickets = w;
        }

        virtual int getPoints() = 0;
    };

    class Test : public Match {
    public:
        void setValues(int r, int c, int w) {
            setRuns(r);
            setCatches(c);
            setWickets(w);
        }

        int getPoints() {
            return ((getRuns() * 0.1) + (getCatches() * 1) + (getWickets() * 1));
        }
    };

    class OneDay : public Match {
    public:
        void setValues(int r, int c, int w) {
            setRuns(r);
            setCatches(c);
            setWickets(w);
        }

        int getPoints() {
            return ((getRuns() * 0.1) + (getCatches() * 1) + (getWickets() * 1));
        }
    };

    class Twenty : public Match {
    public:
        void setValues(int r, int c, int w) {
            setRuns(r);
            setCatches(c);
            setWickets(w);
        }

        int getPoints() {
            return ((getRuns() * 0.1) + (getCatches() * 1) + (getWickets() * 1));
        }
    };

    class Cricketer {
    private:
        string name;
        Match * match[3];
    public:
        Cricketer(string s) {
            name = s;
            match[0] = new Test(3000, 50, 300);
            match[1] = new OneDay(4000, 100, 400);
            match[2] = new Twenty(1500, 25, 100);
        }
        int getTestPoints() {
            return match[0]->getPoints();
        }
        int getOneDayPoints() {
            return match[1]->getPoints();
        }
        int getTwentyPoints() {
            return match[2]->getPoints();
        }
    };
}