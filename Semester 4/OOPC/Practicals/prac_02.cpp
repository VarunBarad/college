//
// Created by vbarad on 19/4/15.
//

#include <iostream>
#include "cmath"

using namespace std;

namespace prac_02
{
    class Complex
    {
    private:
        float real;
        float imaginary;

    public:
        void setData()
        {
            cout << "Enter real part: " ;
            cin >> real ;
            cout << "Enter imaginary part: " ;
            cin >> imaginary ;
            cout << endl ;
        }

        void display()
        {
            cout << "Value: " << real << ((imaginary < 0) ? (" - ") : (" + ")) << fabsf(imaginary) << "i" << endl ;
        }
    };

    void prac_02()
    {
        Complex complex;
        complex.setData();
        complex.display();
    }
}
