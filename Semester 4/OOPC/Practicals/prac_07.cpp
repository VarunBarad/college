//
// Created by bgandhi on 21/4/15.
//

#include "iostream"

namespace prac_07 {
    class Complex {
    private:
        int real;
        int imaginary;
    public:
        Complex() {
            real = 0;
            imaginary = 0;
        }
        Complex(int r, int i) {
            real = r;
            imaginary = i;
        }
        Complex operator++() {
            ++real;
            ++imaginary;
            return *this;
        }
        Complex operator++(int n) {
            Complex c = *this;
            ++real;
            ++imaginary;
            return c;
        }
        void display() {
            cout << "Value: " << real << ((imaginary < 0) ? (" - ") : (" + ")) << fabsf(imaginary) << "i" << endl;
        }
    };

    void prac_07() {
        Complex c(1, 2);
        c.display();
        (c++).display();
        (++c).display();
    }
}