//
// Created by vbarad on 20/4/15.
//

#include "iostream"
#include "cmath"

using namespace std;

namespace prac_04
{
    class Complex
    {
    private:
        float real;
        float imaginary;

    public:
        Complex()
        {
            cout << "Enter real part: " ;
            cin >> real ;
            cout << "Enter imaginary part: " ;
            cin >> imaginary ;
            cout << endl ;
        }

        Complex(float r, float i)
        {
            real = r;
            imaginary = i;
        }

        void display()
        {
            cout << "Value: " << real << ((imaginary < 0) ? (" - ") : (" + ")) << fabsf(imaginary) << "i" << endl ;
        }

        Complex add(Complex a)
        {
            Complex complex(0.0, 0.0);
            complex.real = this->real + a.real ;
            complex.imaginary = this->imaginary + a.imaginary ;
            return complex;
        }

        friend Complex operator+(Complex, Complex);
    };

    Complex operator+(Complex a, Complex b)
    {
        return a.add(b);
    }

    void prac_04()
    {
        Complex a ;
        a.display() ;
        Complex b(5.0, -7.0) ;
        b.display() ;
        Complex c = a + b ;
        c.display() ;
    }
}
