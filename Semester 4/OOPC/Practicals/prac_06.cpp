//
// Created by vbarad on 20/4/15.
//

#include "iostream"

using namespace std;

namespace prac_06 {
    class test {
    private:
        int a;
    public:
        test() {
            a = 0;
        }

        test(int x) {
            a = x;
        }

        int getA() {
            return a;
        }
    };

    void prac_06()
    {
        test t1;
        cout << "t1: " << t1.getA() << endl ;
        test t2(5);
        cout << "t2: " << t2.getA() << endl ;
    }
}
