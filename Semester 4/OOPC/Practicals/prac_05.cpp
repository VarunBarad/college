//
// Created by vbarad on 20/4/15.
//

#include "iostream"

using namespace std;

namespace prac_05 {
    class Time {
    private:
        int hours;
        int minutes;
        int seconds;

        static int toSeconds(Time t) {
            int seconds = (3600 * t.hours) + (60 * t.minutes) + t.seconds;
            return seconds;
        }

        static Time fromSeconds(int seconds) {
            Time t;
            t.hours = seconds / 3600;
            seconds = seconds % 3600;
            t.minutes = seconds / 60;
            seconds = seconds % 60;
            t.seconds = seconds;
            return t;
        }

    public:
        Time() {
            hours = 0;
            minutes = 0;
            seconds = 0;
        }

        Time(int h, int m, int s) {
            hours = h;
            minutes = m;
            seconds = s;
        }

        static Time add_time(Time a, Time b) {
            Time c = Time::fromSeconds(Time::toSeconds(a) + Time::toSeconds(b));
            return c;
        }

        void setTime() {
            cout << "Enter hours: ";
            cin >> hours;
            cout << "Enter minutes: ";
            cin >> minutes;
            cout << "Enter seconds: ";
            cin >> seconds;
            cout << endl;
        }

        string getTime() {
            string s = to_string(hours) + ":" + to_string(minutes) + ":" + to_string(seconds);
            return s;
        }
    };

    void prac_05() {
        Time t1;
        t1.setTime();
        cout << "T1: " << t1.getTime() << endl;
        Time t2(16, 53, 40);
        cout << "T2: " << t2.getTime() << endl;
        Time t3;
        t3 = Time::add_time(t1, t2);
        cout << "T3: " << t3.getTime() << endl ;
    }
}
