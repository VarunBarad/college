#include <iostream>
#include <cstdlib>

using namespace std;

struct node {
    int key;
    int value;
    struct node *left, *right;
};

class BinaryTree {
private:
    node *root;

    void in_order(node *t) {
        if (t != NULL) {
            in_order(t->left);
            cout << "\"" << t->key << "\": " << t->value << endl;
            in_order(t->right);
        }
    }

    node *minValueNode(node *p) {
        node *current = p;
        while (current->left != NULL) {
            current = current->left;
        }
        return (current);
    }

    node *removeNode(node *p, int key) {
        if (p == NULL) {
            return (p);
        }
        if (key < p->key) {
            p->left = removeNode(p->left, key);
        }
        else if (key > p->key) {
            p->right = removeNode(p->right, key);
        }
        else {
            if (p->left == NULL) {
                node *temp = p->right;
                delete(p);
                return (temp);
            }
            else if (p->right == NULL) {
                node *temp = p->left;
                delete(p);
                return (temp);
            }

            node *temp = minValueNode(p->right);
            p->key = temp->key;
            p->value = temp->value;
            p->right = removeNode(p->right, temp->key);
        }
        return (p);
    }

    node *_search(int key, node *t) {
        if (t != NULL) {
            if ((t->key) == key) {
                return (t);
            }
            else if (key < (t->key)) {
                return (_search(key, (t->left)));
            }
            else {
                return (_search(key, (t->right)));
            }
        }
        else {
            return (NULL);
        }
    }

    void cleanNode(node *p) {
        if (p != NULL) {
            cleanNode(p->left);
            cleanNode(p->right);
            delete(p);
        }
    }

public:

    BinaryTree() {
        this->root = NULL;
    }

    void insert(int key, int value) {
        node *p = new node();
        p->key = key;
        p->value = value;
        p->left = NULL;
        p->right = NULL;
        if ((this->root) == NULL) {
            this->root = p;
        }
        else {
            node *temp;
            temp = this->root;
            while (temp != NULL) {
                if (key < (temp->key)) {
                    if ((temp->left) == NULL) {
                        temp->left = p;
                        break;
                    }
                    else {
                        temp = temp->left;
                    }
                }
                else {
                    if ((temp->right) == NULL) {
                        temp->right = p;
                        break;
                    }
                    else {
                        temp = temp->right;
                    }
                }
            }
        }
    }

    void traverse() {
        if (this->root != NULL) {
            cout << "In-Order Traversal" << endl;
            in_order(this->root);
        }
        else {
            cout << "The tree is empty at the moment" << endl;
        }
    }

    void remove(int key) {
        this->root = this->removeNode(this->root, key);
    }

    node *search(int key) {
        return (this->_search(key, (this->root)));
    }

    void clean() {
        cleanNode(root);
    }
};

int main() {
    BinaryTree binaryTree;
    int choice, key, value;
    node *p;

    system("clear");

    while (true) {
        cout << "Select Operation:" << endl;
        cout << "1. Insert a node" << endl;
        cout << "2. Search for a key" << endl;
        cout << "3. Delete a node" << endl;
        cout << "4. Traverse the tree" << endl;
        cout << "5. Exit" << endl;
        cout << "Enter your choice: ";
        cin >> choice;
        cin.sync();
        cout << endl;

        if (choice == 1) {
            cout << "Enter the key: ";
            cin >> key;
            cin.sync();
            if (binaryTree.search(key) != NULL) {
                cout << "Sorry, the key already exists in tree" << endl;
            }
            else {
                cout << "Enter the value: ";
                cin >> value;
                cin.sync();

                binaryTree.insert(key, value);

                cout << "Entry inserted successfully" << endl;
            }
        }
        else if (choice == 2) {
            cout << "Enter the key: ";
            cin >> key;
            cin.sync();

            p = binaryTree.search(key);

            if (p == NULL) {
                cout << "Sorry, no entry corresponding to \"" << key << "\" exists in tree" << endl;
            }
            else {
                cout << "Entry found" << endl;
                cout << "Key: " << p->key << endl;
                cout << "Value: " << p->value << endl;
            }
        }
        else if (choice == 3) {
            cout << "Enter the key: ";
            cin >> key;
            cin.sync();

            if (binaryTree.search(key) != NULL) {
                binaryTree.remove(key);
                cout << "Entry  corresponding to \"" << key << "\" removed" << endl;
            }
            else {
                cout << "Sorry, no entry corresponding to \"" << key << "\" exists in tree" << endl;
            }
        }
        else if (choice == 4) {
            binaryTree.traverse();
        }
        else if (choice == 5) {
            binaryTree.clean();
            break;
        }
        else {
            cout << "Invalid Choice" << endl;
        }

        cout << endl;
    }

    cout << "Thank You" << endl;

    return (0);
}
