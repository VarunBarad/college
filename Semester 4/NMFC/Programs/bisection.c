// Name: Varun Barad
// Enrolment Number: 130070107008
// Topic: Bisection Method

#include <stdio.h>
#include <stdlib.h>

struct quadratic
{
    int a;
    int b;
    int c;
};

float calculate_f(struct quadratic f, float x)
{
    float result;
    result = ((f.a)*x*x) + ((f.b)*x) + (f.c);
    return(result);
}

float mod(float x)
{
    return((((float) x) > 0.0) ? (x) : (-x));
}

int main()
{
    struct quadratic f;
    float a, b, c;

    system("clear");

    printf("Enter the coefficients for Quadratic Function:\n");
    printf("a: ");
    scanf("%d", &(f.a));
    fflush(stdin);
    printf("b: ");
    scanf("%d", &(f.b));
    fflush(stdin);
    printf("c: ");
    scanf("%d", &(f.c));
    fflush(stdin);

    printf("\n");
    printf("Enter the value of:\n");
    printf("a: ");
    scanf("%f", &a);
    fflush(stdin);
    printf("b: ");
    scanf("%f", &b);
    fflush(stdin);

    if((calculate_f(f, a) == 0) || (calculate_f(f, b) == 0))
    {
        if(calculate_f(f, a) == 0)
        {
            printf("%f is a root of the equation\n", a);
        }
        if(calculate_f(f, b) == 0)
        {
            printf("%f is a root of the equation\n", b);
        }
    }
    else if((calculate_f(f, a)*calculate_f(f, b)) > 0)
    {
        printf("Incorrect values of a & b entered\n");
    }
    else
    {
        do
        {
            c = (a+b)/2;
            if(calculate_f(f, c) == 0)
            {
                break;
            }
            else
            {
                if((calculate_f(f, a)*calculate_f(f, c)) < 0)
                {
                    b = c;
                }
                else
                {
                    a = c;
                }
            }
        }while(mod(calculate_f(f, c)) > ((float) 0.00009));
        printf("Answer: %.3f\n", c);
    }

    return 0;
}
