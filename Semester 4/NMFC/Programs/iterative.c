// Name: Varun Barad
// Enrolment Number: 130070107008
// Topic: Iterative Method

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct quadratic
{
    float a;
    float b;
    float c;
};

float calculate_f(struct quadratic f, float x)
{
    float result;
    result = (f.a*x*x) + (f.b*x) + (f.c);
    return(result);
}

float calculate_g(struct quadratic f, float x)
{
    float result;
    result = ((-f.c) - (f.a*x*x)) / (f.b);
    return(result);
}

float calculate_g1(struct quadratic f, float x)
{
    float result;
    result = (-2*f.a*x) / (f.b);
    return(result);
}

float mod(float x)
{
    return((((float) x) > 0.0) ? (x) : (-x));
}

int main()
{
    struct quadratic f;
    float x, y, temp;

    system("clear");

    printf("Enter the value of a: ");
    scanf("%f", &f.a);
    fflush(stdin);
    printf("Enter the value of b: ");
    scanf("%f", &f.b);
    fflush(stdin);
    printf("Enter the value of c: ");
    scanf("%f", &f.c);
    fflush(stdin);

    printf("Enter the starting value for x: ");
    scanf("%f", &x);
    fflush(stdin);

    if(mod(calculate_g1(f, x)) >= 1.0)
    {
        printf("The given value of x is not appropriate\n");
    }
    else
    {
        do
        {
            y = calculate_g(f, x);
            temp = x;
            x = y;
        }while(mod(y-temp) > 0.0009);
        printf("Answer: %.3f\n", y);
    }

    return(0);
}
