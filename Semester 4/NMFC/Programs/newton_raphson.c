// Name: Varun Barad
// Enrolment Number: 130070107008
// Topic: Newton-Raphson Method

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct function_newton
{
    int n;
    int a;
};

float calculate_f(struct function_newton f, float x)
{
    float result;
    result = pow(x, f.n) - f.a;
    return(result);
}

float calculate_f1(struct function_newton f, float x)
{
    float result;
    result = f.n * pow(x, (f.n-1));
    return(result);
}

float calculate_f2(struct function_newton f, float x)
{
    float result;
    result = (f.n * (f.n-1)) * pow(x, (f.n-2));
    return(result);
}

float mod(float x)
{
    return((((float) x) > 0.0) ? (x) : (-x));
}

int main()
{
    struct function_newton f;
    float x, y, temp;

    system("clear");

    printf("Enter the value of n: ");
    scanf("%d", &f.n);
    fflush(stdin);
    printf("Enter the value of a: ");
    scanf("%d", &f.a);
    fflush(stdin);

    printf("Enter the starting value for x: ");
    scanf("%f", &x);
    fflush(stdin);

    if(mod((calculate_f(f, x)*calculate_f2(f, x))/(calculate_f1(f, x)*calculate_f1(f, x))) >= 1)
    {
        printf("The given value of x is not appropriate\n");
    }
    else
    {
        do
        {
            y = x - (calculate_f(f, x) / calculate_f1(f, x));
            temp = x;
            x = y;
        }while(mod(y-temp) > 0.0009);
        printf("Answer: %.3f\n", y);
    }

    return(0);
}
