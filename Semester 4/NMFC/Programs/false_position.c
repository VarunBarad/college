// Name: Varun Barad
// Enrolment Number: 130070107008
// Topic: False-Position Method

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

struct quadratic
{
    int a;
    int b;
    int c;
};

struct point
{
    int x;
    int y;
};

float calculate_f(struct quadratic f, float x)
{
    float result;
    result = (f.a*x*x)+(f.b*x)+(f.c);
    return(result);
}

float getC(struct quadratic f, float a, float b)
{
    float c;
    c = ((a * calculate_f(f, b)) - (b * calculate_f(f, a))) / (calculate_f(f, b) - calculate_f(f, a));
    return(c);
}

float mod(float x)
{
    return(((x < 0.0) ? (-x) : (x)));
}

int main()
{
    struct quadratic f;
    float a, b, c;

    system("clear");

    printf("Enter the coefficients for Quadratic Function:\n");
    printf("a: ");
    scanf("%d", &(f.a));
    fflush(stdin);
    printf("b: ");
    scanf("%d", &(f.b));
    fflush(stdin);
    printf("c: ");
    scanf("%d", &(f.c));
    fflush(stdin);

    printf("\n");
    printf("Enter value of:\n");
    printf("a: ");
    scanf("%f", &a);
    fflush(stdin);
    printf("b: ");
    scanf("%f", &b);
    fflush(stdin);

    if((calculate_f(f, a) == 0.0) || (calculate_f(f, b) == 0.0))
    {
        if(calculate_f(f, a) == 0.0)
        {
            printf("%f is a root of the equation\n", a);
        }
        if(calculate_f(f, b) == 0.0)
        {
            printf("%f is a root of the equation\n", b);
        }
    }
    else if((calculate_f(f, a)*calculate_f(f, b)) > 0.0)
    {
        printf("Incorrect values of a & b entered\n");
    }
    else
    {
        do
        {
            c = getC(f, a, b);
            if(calculate_f(f, c) == 0.0)
            {
                break;
            }
            else
            {
                if((calculate_f(f, c)*calculate_f(f, a)) < 0.0)
                {
                    b = c;
                }
                else
                {
                    a = c;
                }
            }
        }while(mod(calculate_f(f, c)) > 0.00009);

        printf("Answer is: %.3f\n", c);
    }

    return(0);
}
