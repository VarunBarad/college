#Practical 9.2


echo "Enter the student name:"
read student
echo "Enter the marks of student separated by a space:"
read -a marks
count=0
for i in "${marks[@]}"
do
	if [ $i -lt 50 ]
	then
		count=$(expr "$count" + 1) 
	fi
done
if [ $count -eq 0 ]
then
	echo "The Student Passed in all subjectss"
else
	echo "The Student Failed in $count subjects"
fi
