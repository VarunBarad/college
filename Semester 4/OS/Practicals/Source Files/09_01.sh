#Practical 9.1


echo "Enter the number of Electricity Units:"
read number_of_units
if [ $number_of_units -lt 101 -a $number_of_units -gt 0 ]
then
	answer=$number_of_units
	echo $answer
elif [ $number_of_units -gt 100 -a $number_of_units -lt 201 ]
then
	above_100=$(expr "$number_of_units" - 100)
	answer=$(expr "$above_100" "*" 2)
	answer=$(expr "$answer" + 100)
	echo $answer
else
	above_200=$(expr "$number_of_units" - 200)
	answer=$(expr "$above_200" "*" 3)
	answer=$(expr "$answer" + 300)
	echo $answer
fi
