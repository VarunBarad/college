#Practical 8.2


echo "Enter the name of the file:"
read filename
echo "Enter the text to be written in $filename:"
read text
echo $text | cat > $filename
echo "Enter the word to be found:"
read word
if grep "$word" -ac "$filename"
then 
	echo "The word was found"
else
	echo "The word not found"
fi
