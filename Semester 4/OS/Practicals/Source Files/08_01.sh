#Practical 8.1


echo "Enter the File Name: "
read name
while :
do
	echo "Choose command:"
	echo "1. Copy to New File"
	echo "2. Edit the File"
	echo "3. Rename the File"
	echo "4. Delete the File"
	echo "5. Exit"
	echo "Enter your Choice: "
	read choice
	case $choice in
	1)
	    echo "Enter name of new file:"
	    read new_name
	    cp $name $new_name
	;;
	2)
	    nano $name
	;;
	3)
	    echo "Enter name of new file:"
	    read new_name
	    mv $name $new_name
	;;
	4)
	    rm -f $name
	;;
	5)
	    exit
	;;
	esac
done
