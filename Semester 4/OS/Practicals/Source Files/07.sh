# Practical 7


echo "Enter File Name: "
read name
echo "Enter 5 names:"
for i in 1 2 3 4 5
do
    read a
    echo $a >> $name
done
echo "Enter New File Name: "
read new_name
cat < $name | sort | cat > $new_name
clear
echo "Unsorted File:"
cat < $name
echo ""
echo "Sorted File:"
cat < $new_name
rm -f $name $new_name
