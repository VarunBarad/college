#Practical 10.1


function factorial(){
	fact_answer=1
	fact_counter=1
	while [ $fact_counter -le $fact_limit ]
	do
		fact_answer=`expr $fact_answer \* $fact_counter`
		fact_counter=`expr $fact_counter + 1`
	done
	return $fact_answer
}
clear
echo "Enter the n value:"
read n
counter=1
ans=1
while [ $counter -le $n ]
do
	fact_limit=$counter
	factorial
	fact=$?
	term=`echo "scale = 2; $counter / $fact" | bc`
	ans=`echo "scale = 2; $ans + $term" | bc`
	counter=$(expr "$counter" + 1)
done 
echo $ans
