Practical 2

* File Oriented Commands
    
    01. cat
        
        This is used to display the contents of the specified file
        
        General Format
            cat [-OPTIONS] <filename1> [<filename2> ...]
        
        Options
            
            s
                Suppresses warning about non-existent files
            d
                Lists the sub-directory entries only
            b
                Numbers non-blank output lines
            n
                Numbers all output lines
        
        Examples
            
            $ cat a.c
                
                This will display the contents of the file 'a.c'.
            
            $ cat a.c b.c
                
                This will display the contents of the files, 'a.c' and 'b.c', one by one
        
        This command can be used with redirection operator (>) to create new files
        
        General Format
            cat > filename
            <Type the text>
            ^d (press [ctrl]+d at the end)
        
        Examples
            
            $ cat > x.txt
            Hi! This
            is
            a file. Press [^d]
                
                Then a file named x.txt will be created in the current working directory
    
    02. cp
        
        This is used to copy the contents of one file to other.
        If the destination is an existing file, the file is overwritten.
        If the destination is an existing directory, the file is copied into that directory.
        
        General Format
            cp [-OPTIONS] <source-file> <destination-file>
        
        Options
            
            i
                Prompt before overwriting destination files
            p
                Preserve all information, including owner, group, permissions and timestamps
            R
                recursively copies files in all subdirectories
        
        Example
            
            $ cp a.c b.c
                
                The content of 'a.c' will be copied in to 'b.c'
    
    
    
    03. rm
        
        This is used to delete a file from the specified directory.
        To remove a file, you must have write permission for the directory that contains the
        file, but you need not have permission on the file itself. If you do not have write
        permission on the file, the system will prompt before removing.
        
        General Format
            rm [-options] <filename>
        
        Options
            
            r
                Deletes all directories including the lower order directiories. Recursively
                deletes entire contents of the specified directory and the directory itself
            i
                Prompts before deleting
            f
                Removes write-protected files also, without prompting
        
        Examples
            
            $ rm a.c
                
                This command deletes the file named a.c from the current directory
            
            $ rm -rf /usr/ibrahim
                
                This command deletes all the files and subdirectories of the specified
                directory "/usr/ibrahim". Note that the directory 'ibrahim' also will be
                deleted.
    
    04. mv
        
        This is used to move/rename the specified files/directories.
        
        General Format
            mv <source> <destination>
        
        Note that to make the move, the user must have write permission on both source and
        destination
        
        Example
            
            $ mv a.c b.c
                
                The file "a.c" is renamed to "b.c"
    
    05. wc
        
        This command is used to display the number of lines, words and characters of
        information stored on the specified file.
        
        General Format
            wc [-OPTIONS] <filename>
        
        Options
            
            l
                Displays the number of lines in file
            w
                Displays the number of words in file
            c
                Displays the number of characters in file
        
        Examples
            
            $ wc a.c
                
                It displays the number of lines, words and characters in the file "a.c"
            
            $ wc -l a.c
                
                Displays the number of lines in "a.c"
    
    06. ln
        
        This is used to establish an additional filename to a specified file. It dosen't mean
        creating more copies of the specified file.
        
        General Format
            ln <filename> <additional_filename>
        
        The additional file names can be located on any directory. Thus, Linux allows a file
        to have more than one name, and yet maintain a single copy in the disk. But, changes
        to one of these files are also reflected to the others. If you delete one filename
        using rm command, then the other link-names will still exist.
        
        Example
            
            $ ln test1.txt test2.txt
                
                The above command would create a link, named "test2.txt", to the file
                "test1.txt".
    
    07. file
        
        This lists the general classification of a specified file. It lets you to
        know if the content of the specified file is ASCII text, C program text, separate
        executable, empty or others.
        
        General Format
            file <filename>
        
        Example
            $ file test1.txt
            test1.txt: ASCII text
    
    08. cmp
        
        This is used to compare two files. This reports the first instants of difference
        between the specified files.
        
        General Format
            cmp <filename1> <filename2>
        
        Example
            
            $ cat 1.txt
            I am Varun
            $ cat 2.txt
            I am Jarvis
            $ cmp 1.txt 2.txt
            1.txt 2.txt differ: byte 6, line 1
                
                The "1.txt" differs from "2.txt" at 6th byte, which occurs in first line
    
    09. comm
        
        This uses two sorted files as arguments and reports what is common. It compares each
        line of the first file with its corresponding line in the second file.
        
        The output of this command is in three columns as follows,
        Column1     Contains lines unique to filename1
        Column2     Contains lines unique to filename2
        Column3     Contains lines common for both filename1 and filename2
        
        General Format
            comm [-OPTIONS] <filename1> <filename2>
        
        Options
        1
            Suppresses listing of column1
        2
            Suppresses listing of column2
        3
            Suppresses listing of column3
        
        Example
            
            $ cat 1.txt
            I am ibrahim,
            What is your name?
            $ cat 2.txt
            I am ibrahim,
            What are you doing?
            $ comm 1.txt 2.txt
                        I am ibrahim,
                What are you doing?
            What is your name?
                
                This command displays the lines unique to "1.txt" in first column, the lines
                that are unique to "2.txt" in column 2 and the lines that are common for
                "1.txt" and "2.txt" in column3.







* File access permission commands
    
    > File Permissions:
        
        Files and directories are owned by a user. The owner determines the file's "owner"
        class. Distinct permissions apply to the owner. Files and directories are assigned a
        group, which define the file's "group" class. Distinct permissions apply to members of
        the file's group. The owner may be a member of the file's group. Users who are not the
        owner, nor a member of the group, comprise a file's "others" class. Distinct
        permissions apply to others.

        The effective permissions are determined based on the user's class.For example, the
        user who is the owner of the file will have the permissions given to the "owner" class
        regardless of the permissions assigned to the "group" class or "others" class.
        
        These systems apply 3 specific permissions that apply to each class:
            
            - The read permission grants the ability to read a file. When set for a directory,
              this permission grants the ability to read only the names of files in the
              directory.
            - The write permission grants the ability to modify a file. When set for a
              directory, this permission grants the ability to modify entries in the directory.
              This includes creating files, deleting files, and renaming files.
            - The execute permission grants the ability to execute a file. This permission must
              be set for executable programs, including shell scripts, in order to allow the
              operating system to run them. When set for a directory, this permission grants
              the ability to access file contents and meta-information if its name is known,
              but not list files inside the directory, unless read is set also.
    
    01. chmod
        
        This is used to change the file permissions for an existing file. We can use symbolic
        or octal notation to change file permissions.
        
        Symbolic mode:
            
            General Format
                chmod <user_symbols> <set/deny_symbol> <access_symbols> <filename>
            
            User Symbols
                
                u   User
                g   User group
                o   Others
            
            Set/Deny Symbol
                
                +   Assign the permissions
                -   Remove the permissions
                =   Assign absolute permissions
            
            Access Symbols
                
                r   Readable
                w   Writable
                x   Executable
            
            Example
                
                $ chmod u+x temp.out
                    
                    This adds execution permission for the owner user on the file "temp.out"
                
                More than one permissions can be specified in one "chmod" command
                
                $ chmod u+x,o-r temp.out
                    
                    This adds execution permission for owner user and removes read permission
                    for others
        
        Octal mode:
            
            General Format
                chmod <three_digit_number> <filename>
            
            Digits
                
                0   No permissions
                4   Read
                2   Write
                1   Execute
                
                These numbers can be summed to mix permissions
            
            Example
                
                $ chmod 740 file1
                    
                    In this case, the "user" can read, write and execute file1. The "usergroup"
                    can only read the file. And the "others" can't do anything with this file.
    
    02. chown
        
        This is used to change the owner of a specified file. Only the current owner of the
        file and "root" user can change the owner of a specified file.
        
        General Format
            chown <new_owner> <filename>
        
        Example
            
            $ chown apache test.db
                
                The above command makes the user "apache" the owner of file "test.db"
    
    03. chgrp
        
        This is used to change the owner_group of a specified file. Only the current owner of
        the file and "root" user can change the owner of a specified file.
        
        General Format
            chgrp <new_groupname> <filename>
        
        Example
            
            $ chgrp apache test.db
                
                The above command makes the group "apache" the owner_group of file "test.db"
    
    
    
    
    
    
    
    04. touch
        
        This is used to change the last modification and access time of a specified file to a
        specified time.
        
        General Format
            touch [MMDDHHmm] <filename>
        
        Here
            MM  Month(01-12)
            DD  Day(01-31)
            HH  Hour(00-23)
            mm  Minute(00-59)
        
        If MMDDHHmm expression is not used, the current date and time are taken by default.
    
    05. nl
        
        This command numbers all non-blank lines in specified text file and displays the same
        on screen.
        
        General Format
            nl <filename>
        
        Example
            
            $ cat test.txt
            This is first line.
            
            This is second line.
            
            This is third line.
            $ nl test.txt
                 1	This is the first line.
       
                 2	This is the second line.
       
                 3	This is the third line.
    06. tail
        
        This command displays the end of the specified file
        
        General Format
            tail (+-)n <filename>
        
        Options
            +n  display from line number n till end
            -n  display last n lines
        
        Example
            
            $ history | tail -5
              816  exit
              817  cat > test.txt
              818  nl test.txt 
              819  exit
              820  history | tail -5
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    07. head
        
        This command displays the top of the specified file
        
        General Format
            head [-n] <filename>
        
        Options
            
            -n  display n number of lines from top, default value is 10
        
        Example
            
            $ history | head -5
                1  cat < available | grep easytag
                2  su
                3  exit
                4  cd Documents/college/
                5  git log
