#include <stdio.h>
#include <stdlib.h>

#define MAX 10

int insert(int *arr , int *length , int n)
{
	int i , pos;
	if(*length < MAX)
	{
		for(i = 0 ; ((i < *length) && (*(arr+i) < n)) ; i++);
		if(i == *length)
		{
			*(arr+i) = n;
			pos = i;
			(*length)++;
		}
		else
		{
			pos = i;
			(*length)++;
			for(i = *length ; i > pos ; i--)
			{
				*(arr+i) = *(arr+i-1);
			}
			*(arr+pos) = n;
		}
		return(pos);
	}
	else
	{
		return(-1);
	}
}

void remove_duplicates(int *arr , int *length)
{
	int i , j , k;
	
	for(i = 0 ; i < *length ; i++)
	{
		for(j = (i+1) ; j < *length ; j++)
		{
			if(*(arr+j) == *(arr+i))
			{
				for(k = j ; k < (*length-1) ; k++)
				{
					*(arr+k) = *(arr+k+1);
				}
				(*length)--;
				j--;
			}
		}
	}
}

void display(int *arr , int length)
{
	int i;
	for(i = 0 ; i < length ; i++)
	{
		printf("%d->" , *(arr+i));
	}
	printf("\n\n");
}

int main(int argc , char *argv[])
{
	int array[MAX] , length = 0 , choice , n , result;
	
	system("clear");
	
	while(1)
	{
		printf("Select Operation\n");
		printf("1. Insert Element\n");
		printf("2. Remove Duplicates\n");
		printf("3. Display Contents\n");
		printf("4. Exit\n");
		printf("Enter Your Choice: ");
		fflush(stdin);
		scanf("%d" , &choice);
		printf("\n");
		
		if(choice == 1)
		{
			printf("Enter Value to Insert: ");
			fflush(stdin);
			scanf("%d" , &n);
			printf("\n");
			
			result = insert(array , &length , n);
			
			if(result == -1)
			{
				printf("Array Already Full\nFailed to Insert Element\n\n");
			}
			else
			{
				printf("Element Inserted at Position %d\n\n" , result);
			}
		}
		else if(choice == 2)
		{
			remove_duplicates(array , &length);
		}
		else if(choice == 3)
		{
			display(array , length);
		}
		else if(choice == 4)
		{
			break;
		}
		else
		{
			printf("Enter A Valid Option\n\n");
		}
	}
	printf("Thank You\n");
	
	return(0);
}
