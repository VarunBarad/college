#include <stdio.h>
#include <stdlib.h>

#define MAX 20

int insert(int *arr , int *length , int n)
{
	int i;
	if(*length == MAX)
	{
		return(-1);
	}
	else
	{
		*(arr + *length) = n;
		(*length)++;
		return((*length)-1);
	}
}

void sort(int *arr , int length)
{
	int i , j , temp;
	for(i = 0 ; i < (length-1) ; i++)
	{
		for(j = (i+1) ; j < length ; j++)
		{
			if(*(arr+i) > *(arr+j))
			{
				temp = *(arr+i);
				*(arr+i) = *(arr+j);
				*(arr+j) = temp;
			}
		}
	}
}

void display(int *arr , int length)
{
	int i;
	for(i = 0 ; i < length ; i++)
	{
		printf("%d->" , *(arr+i));
	}
	printf("\n\n");
}

int main(int argc , char *argv[])
{
	int array[MAX] , length = 0 , choice , n , result;
	
	system("clear");
	
	while(1)
	{
		printf("Select Operation\n");
		printf("1. Insert an Element\n");
		printf("2. Sort the Array\n");
		printf("3. Diplay Contents\n");
		printf("4. Exit\n");
		printf("Enter Your Choice: ");
		fflush(stdin);
		scanf("%d" , &choice);
		printf("\n");
		
		if(choice == 1)
		{
			printf("Enter Value to Insert: ");
			fflush(stdin);
			scanf("%d" , &n);
			printf("\n");
			
			result = insert(array , &length , n);
			
			if(result == -1)
			{
				printf("Array Already Full\nFailed to Insert Element\n\n");
			}
			else
			{
				printf("Element Inserted Successfully\n\n");
			}
		}
		else if(choice == 2)
		{
			sort(array , length);
		}
		else if(choice == 3)
		{
			display(array , length);
		}
		else if(choice == 4)
		{
			break;
		}
		else
		{
			printf("Enter a Valid Option\n\n");
		}
	}
	
	printf("Thank You\n");
	
	return(0);
}
