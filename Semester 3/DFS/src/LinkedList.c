#include <stdio.h>
#include <stdlib.h>

struct node
{
	int n;
	struct node *next;
};

struct node* getNode(int n)
{
	struct node *p = (struct node *) malloc(sizeof(struct node));
	p->n = n;
	p->next = NULL;
	return(p);
}

void insert(struct node **p , int n)
{
	struct node *q = getNode(n);
	struct node *temp = *p;
	if(temp == NULL)
	{
		*p = q;
	}
	else if(temp->n > n)
	{
		q->next = temp;
		*p = q;
	}
	else
	{
		while((temp->next != NULL) && ((temp->next)->n < n))
		{
			temp = temp->next;
		}
		q->next = temp->next;
		temp->next = q;
	}
}

int delete(struct node **p , int n)
{
	int pos = 1;
	struct node *temp = *p;
	struct node *q;
	if(temp->n == n)
	{
		*p = temp->next;
		free(temp);
		return(0);
	}
	else
	{
		while((temp->next != NULL) && ((temp->next)->n != n))
		{
			temp = temp->next;
			pos++;
		}
		if(temp->next == NULL)
		{
			return(-1);
		}
		else
		{
			q = temp->next;
			temp->next = (temp->next)->next;
			free(q);
			return(pos);
		}
	}
}

void display(struct node *p)
{
	if(p == NULL)
	{
		printf("List Is Empty\n\n");
	}
	else
	{
		while(p != NULL)
		{
			printf("%d->" , (p->n));
			p = p->next;
		}
		printf("\n\n");
	}
}

void close(struct node *p)
{
	struct node *temp;
	while(p != NULL)
	{
		temp = p;
		p = p->next;
		free(temp);
	}
}

int main(int argc , char *argv[])
{
	int choice , n , result;
	struct node *head = NULL;
	
	system("clear");
	
	printf("!!! Linked List Demonstration !!!\n\n");
	while(1)
	{
		printf("Select Your Operation\n");
		printf("1. Add an Element\n");
		printf("2. Delete an Element\n");
		printf("3. Display Contents of List\n");
		printf("4. Exit\n");
		printf("Enter Your Choice: ");
		fflush(stdin);
		scanf("%d" , &choice);
		printf("\n");
		
		if(choice == 1)
		{
			printf("Enter Element Value: ");
			fflush(stdin);
			scanf("%d" , &n);
			insert(&head , n);
			printf("Element Added Successfully\n\n");
		}
		else if(choice == 2)
		{
			printf("Enter Value of Element to Delete: ");
			fflush(stdin);
			scanf("%d" , &n);
			result = delete(&head , n);
			if(result == -1)
			{
				printf("Element Not Present in List\n\n");
			}
			else
			{
				printf("Element Deleted from Position %d\n\n" , result);
			}
		}
		else if(choice == 3)
		{
			display(head);
		}
		else if(choice == 4)
		{
			close(head);
			break;
		}
		else
		{
			printf("Please Enter A Valid Choice\n\n");
		}
	}
	
	printf("Thanks\n");
	
	return(0);
}
