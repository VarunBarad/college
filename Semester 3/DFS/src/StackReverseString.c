#include <stdio.h>
#include <stdlib.h>

#define MAX 20

struct stack
{
	char elements[MAX];
	int top;
};

void push(struct stack *s , char c , int *o)
{
	if(s->top == (MAX-1))
	{
		*o = 1;
	}
	else
	{
		*o = 0;
		(s->top)++;
		s->elements[s->top] = c;
	}
}

char pop(struct stack *s , int *u)
{
	char temp;
	if(s->top == (-1))
	{
		*u = 1;
		return('#');
	}
	else
	{
		*u = 0;
		temp = s->elements[s->top];
		(s->top)--;
		return(temp);
	}
}

int main(int argc , char *argv[])
{
	struct stack s;
	int choice , o , u , i , length;
	char n[21];
	
	system("clear");
	
	s.top = -1;
	
	while(1)
	{
		printf("What would You Like to Do\n");
		printf("1. Reverse a String\n");
		printf("2. Exit\n");
		printf("Enter Your Choice: ");
		fflush(stdin);
		scanf("%d" , &choice);
		printf("\n");
		
		if(choice == 1)
		{
			printf("Enter the String to Reverse(Max. 20 Characters): ");
			fflush(stdin);
			scanf("%s" , n);
			printf("\n");
			
			for(length = 0 ; n[length] != 0 ; length++);
			
			for(i = 0 ; i < length ; i++)
			{
				push(&s , n[i] , &o);
			}
			for(i = 0 ; i < length ; i++)
			{
				n[i] = pop(&s , &u);
			}
			
			printf("Reversed String: %s\n\n" , n);
		}
		else if(choice == 2)
		{
			break;
		}
		else
		{
			printf("Enter a Valid Choice\n\n");
		}
	}
	
	printf("Thank You\n");
	
	return(0);
}
