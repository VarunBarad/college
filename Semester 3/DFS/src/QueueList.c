#include <stdio.h>
#include <stdlib.h>

struct node
{
	int n;
	struct node *next;
};

struct node* getNode(int n)
{
	struct node *p = NULL;
	p = (struct node *) malloc(sizeof(struct node));
	p->n = n;
	p->next = NULL;
	return(p);
}

void insert(struct node **front , struct node **rear , int n , int *o)
{
	struct node *p = getNode(n);
	
	if(p == NULL)
	{
		*o = 1;
	}
	else
	{
		*o = 0;
		if(*front == NULL)
		{
			*front = p;
			*rear = p;
		}
		else
		{
			(*rear)->next = p;
			(*rear) = p;
		}
	}
}

int delete(struct node **front , struct node **rear , int *u)
{
	struct node *temp = *front;
	int n;
	
	if(*front == NULL)
	{
		*u = 1;
		return(-1);
	}
	else
	{
		*u = 0;
		
		n = temp->n;
		
		if(*front == *rear)
		{
			*front = NULL;
			*rear = NULL;
		}
		else
		{
			*front = (*front)->next;
		}
		
		free(temp);
		
		return(n);
	}
}

void display(struct node *front)
{
	if(front != NULL)
	{
		while((front->next) != NULL)
		{
			printf("%d --> " , (front->n));
			front = front->next;
		}
		printf("%d\n\n" , (front->n));
	}
}

int main(int argc , char *argv[])
{
	int choice , n , o , u;
	struct node *front = NULL , *rear = NULL;
	
	system("clear");
	
	while(1)
	{
		printf("Select Operation\n");
		printf("1. Insert an Element\n");
		printf("2. Delete an Element\n");
		printf("3. Display the Contents\n");
		printf("4. Exit\n");
		printf("Enter Your Choice: ");
		fflush(stdin);
		scanf("%d" , &choice);
		printf("\n");
		
		if(choice == 1)
		{
			printf("Enter the Element to Insert: ");
			fflush(stdin);
			scanf("%d" , &n);
			printf("\n");
			
			insert(&front , &rear , n , &o);
			
			if(o == 1)
			{
				printf("OVERFLOW\nFailure to Insert Element\n\n");
			}
			else
			{
				printf("Element Inserted Successfully\n\n");
			}
		}
		else if(choice == 2)
		{
			n = delete(&front , &rear , &u);
			
			if(u == 1)
			{
				printf("UNDERFLOW\nFailure to Delete Element\n\n");
			}
			else
			{
				printf("Element Successfully Deleted\nValue: %d\n\n" , n);
			}
		}
		else if(choice == 3)
		{
			display(front);
		}
		else if(choice == 4)
		{
			u = 0;
			while(u == 0)
			{
				n = delete(&front , &rear , &u);
			}
			break;
		}
		else
		{
			printf("Enter a Valid Choice\n\n");
		}
	}
	
	printf("Thank You\n");
	
	return(0);
}
