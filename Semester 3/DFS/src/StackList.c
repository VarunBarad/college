#include <stdio.h>
#include <stdlib.h>
#include <limits.h>

struct node
{
    int n;
    struct node *next;
};

int push(struct node **top , int n)
{
    struct node *ptr = NULL;
    ptr = (struct node *) malloc(sizeof(struct node));
    if(ptr == NULL)
    {
        return(-1);
    }
    else
    {
        ptr->n = n;
        ptr->next = *top;
        *top = ptr;
        return(0);
    }
}

int pop(struct node **top)
{
    int n;
    struct node *ptr = *top;

    if(*top == NULL)
    {
        return(INT_MIN);
    }
    else
    {
        n = (*top)->n;
        *top = (*top)->next;
        free(ptr);
        return(n);
    }
}

void display(struct node *top)
{
	printf("\n\n");
	if(top != NULL)
	{
		while((top->next) != NULL)
		{
			printf("%d --> " , (top->n));
			top = top->next;
		}
		printf("%d\n\n" , (top->n));
	}
	else
	{
		printf("The Stack Is Empty\n\n");
	}
}

int peep(struct node *top , int index)
{
	int i = 0;
	while(top != NULL)
	{
		if(i == index)
		{
			return(top->n);
		}
		top = top->next;
		i++;
	}
	
	return(INT_MIN);
}

int main(int argc , char *argv[])
{

    int choice , n , result;
    struct node *top=NULL;

    system("clear");
    
    while(1)
    {
        printf("1. Push an Element on to Stack\n2. Pop an Element from Stack\n3. Display All Elements\n4. Peep an Element\n5. Exit\n");
        printf("Enter Your Choice: ");  scanf("%d" , &choice);

        if(choice == 1)
        {
            printf("\n\nEnter the Element to Push: ");  scanf("%d" , &n);
            result = push(&top , n);
            if(result == 0)
            {
                printf("\n\nElement Successfully Stacked\n\n");
            }
            else
            {
                printf("\n\nOVERFLOW\n\n");
            }
        }
        else if(choice == 2)
        {
            result = pop(&top);
            if(result != INT_MIN)
            {
                printf("\n\nElement Popped from Stack: %d\n\n" , result);
            }
            else
            {
                printf("\n\nUNDERFLOW\n\n");
            }
        }
        else if(choice == 3)
        {
        	display(top);
        }
        else if(choice == 4)
        {
        	printf("\n\nEnter the Index to Display: ");	scanf("%d" , &n);
        	result = peep(top , (n-1));
        	if(result != INT_MIN)
        	{
        		printf("\n\nElement at %d: %d\n\n" , n , result);
        	}
        	else
        	{
        		printf("\n\nNo Element Present in the Stack at given Index\n\n");
        	}
        }
        else if(choice == 5)
        {
            while(pop(&top) != INT_MIN);
            break;
        }
        else
        {
            printf("\nPlease Select A Valid Option\n\n");
        }
    }

}
