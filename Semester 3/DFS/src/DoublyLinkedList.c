#include <stdio.h>
#include <stdlib.h>

struct node
{
    int n;
    struct node *previous;
    struct node *next;
};

struct node* getNode(int n)
{
    struct node *p = (struct node *) malloc(sizeof(struct node));
    p->n = n;
    p->previous = NULL;
    p->next = NULL;
    return(p);
}

void insert(struct node **p , int n)
{
    struct node *q , *temp;
    q = getNode(n);
    temp = *p;
    
    if(temp == NULL)
    {
        *p = q;
    }
    else if(temp->n > n)
    {
        q->next = temp;
        temp->previous = q;
        *p = q;
    }
    else
    {
        while((temp->next != NULL) && ((temp->next)->n < n))
        {
            temp = temp->next;
        }
        q->next = temp->next;
        temp->next = q;
        q->previous = temp;
        if(q->next != NULL)
        {
            (q->next)->previous = q;
        }
    }
}

int delete(struct node **p , int n)
{
    struct node *q , *temp;
    int pos;
    q = *p;
    pos = 0;
    
    if((q != NULL) && (q->n == n))
    {
        temp = q;
        *p = q->next;
        (*p)->previous = NULL;
        free(temp);
    }
    else
    {
        while((q != NULL) && (q->n != n))
        {
            q = q->next;
            pos++;
        }
        if(q == NULL)
        {
            pos = -1;
        }
        else
        {
            temp = q;
            (q->previous)->next = q->next;
            if(q->next != NULL)
            {
                (q->next)->previous = q->previous;
            }
            free(temp);
        }
    }
    
    return(pos);
}

void display(struct node *p)
{
    if(p != NULL)
    {
        while(p->next != NULL)
        {
            printf("%d-->" , p->n);
            p = p->next;
        }
        printf("%d\n\n" , p->n);
    }
    else
    {
        printf("List is Empty\n\n");
    }
}

void close(struct node *p)
{
    struct node *temp;
    while(p != NULL)
    {
        temp = p;
        p = p->next;
        free(temp);
    }
}

int main(int argc , char *argv[])
{
    int choice , result , n;
    struct node *head = NULL;
    
    system("clear");
    
    while(1)
    {
        printf("Select Your Operation\n");
        printf("1. Add an Element\n");
        printf("2. Delete an Element\n");
        printf("3. Display Contents of List\n");
        printf("4. Exit\n");
        printf("Enter Your Choice: ");
        fflush(stdin);
        scanf("%d" , &choice);
        printf("\n");
        
        if(choice == 1)
        {
            printf("Enter Element Value: ");
            fflush(stdin);
            scanf("%d" , &n);
            insert(&head , n);
            printf("Element Added Successfully\n\n");
        }
        else if(choice == 2)
        {
            printf("Enter Value of Element to Delete: ");
            fflush(stdin);
            scanf("%d" , &n);
            result = delete(&head , n);
            if(result == -1)
            {
                printf("Element Not Present in List\n\n");
            }
            else
            {
                printf("Element Deleted from Position %d\n\n" , result);
            }
        }
        else if(choice == 3)
        {
            display(head);
        }
        else if(choice == 4)
        {
            close(head);
            break;
        }
        else
        {
            printf("Please Enter A Valid Choice\n\n");
        }
    }
}
