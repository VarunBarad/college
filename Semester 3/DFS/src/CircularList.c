#include <stdio.h>
#include <stdlib.h>

struct node
{
    int n;
    struct node *next;
};

struct node* getNode(int n)
{
    struct node *p = (struct node *) malloc(sizeof(struct node));
    p->n = n;
    p->next = NULL;
    return(p);
}

void insert(struct node **end , int n)
{
    struct node *p , *q , *temp;
    
    q = getNode(n);
    
    if((*end) == NULL)
    {
        *end = q;
        q->next = q;
    }
    else
    {
        p = (*end)->next;
        (*end)->next = NULL;
        
        temp = p;
        if(temp->n > n)
        {
            q->next = temp;
            p = q;
        }
        else
        {
            while((temp->next != NULL) && ((temp->next)->n < n))
            {
                temp = temp->next;
            }
            q->next = temp->next;
            temp->next = q;
        }
        
        if(((*end)->next) == NULL)
        {
            (*end)->next = p;
        }
        else
        {
            ((*end)->next)->next = p;
            (*end) = (*end)->next;
        }
    }
}

void delete(struct node **end , int n)
{
    if((*end) != NULL)
    {
        if(((*end)->next == (*end)) && ((*end)->n == n))
        {
            *end = NULL;
        }
        else
        {
            struct node *p = (*end)->next;
            struct node *q = *end;
            while((p != (*end)) && ((p->n) != n))
            {
                q = p;
                p = p->next;
            }
            if(p == (*end))
            {
                if((p->n) == n)
                {
                    *end = q;
                    (*end)->next = p->next;
                    free(p);
                }
            }
            else
            {
                q ->next = p->next;
                free(p);
            }
        }
    }
}

void finish(struct node **end)
{
    struct node *p , *q;
    if((*end) != NULL)
    {
        p = (*end)->next;
        (*end)->next = NULL;
        while(p != NULL)
        {
            q = p;
            p = p->next;
            free(q);
        }
    }
}

void display(struct node *end)
{
    struct node *p;
    if(end != NULL)
    {
        p = end->next;
        while(p != end)
        {
            printf("%d --> " , (p->n));
            p = p->next;
        }
        printf("%d\n\n" , (p->n));
    }
    else
    {
        printf("List is Empty\n\n");
    }
}

int main(int argc , char *argv[])
{
    struct node *end;
    int choice , n;
    
    system("clear");
    
    end = NULL;
    
    while(1)
    {
        printf("Select Operation:\n\n");
        printf("1. Insert an Element\n");
        printf("2. Delete an Element\n");
        printf("3. Display Contents\n");
        printf("4. Exit\n\n");
        printf("Enter Your Choice: ");
        fflush(stdin);
        scanf("%d" , &choice);
        printf("\n");
        
        if(choice == 1)
        {
            printf("Enter Value of Element to Insert: ");
            fflush(stdin);
            scanf("%d" , &n);
            
            insert(&end , n);
            
            printf("Element Successfully Inserted\n\n");
        }
        else if(choice == 2)
        {
            printf("Enter Value to Remove from List: ");
            fflush(stdin);
            scanf("%d" , &n);
            
            delete(&end , n);
            
            printf("List Successfully Freed of %d\n\n" , n);
        }
        else if(choice == 3)
        {
            display(end);
        }
        else if(choice == 4)
        {
            break;
        }
        else
        {
            printf("Please Enter a Valid Choice\n\n");
        }
    }
    
    finish(&end);
    printf("Thank You\n\n");
    
    return(0);
}
