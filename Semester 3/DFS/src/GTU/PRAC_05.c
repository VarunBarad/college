// Name : Varun Barad
// Enrolment Number : 130070107008
// Subject : Data & File Structure
// Practical Number : 05

#include <stdio.h>
#include <stdlib.h>

#define MAX 20

struct queue
{
    int elements[MAX];
    int rear;
};

void queue_shift(struct queue *q)
{
    int i;
    if(q->rear != -1)
    {
        for(i=0; i <= q->rear; i++)
        {
            q->elements[i] = q->elements[i] + q->elements[i+1];
            q->elements[i+1] = q->elements[i] - q->elements[i+1];
            q->elements[i] = q->elements[i] - q->elements[i+1];
        }
    }
}

void queue_insert(struct queue *q, int n, int *o)
{
    if(q->rear == (MAX-1))
    {
        *o = 1;
    }
    else
    {
        *o = 0;
        q->rear++;
        q->elements[q->rear] = n;
    }
}

int queue_remove(struct queue *q, int *u)
{
    int value;
    if(q->rear == (-1))
    {
        *u = 1;
        value = -1;
    }
    else
    {
        *u = 0;
        value = q->elements[0];
        q->rear--;
        queue_shift(q);
    }
    return(value);
}

void queue_display(struct queue q)
{
    int i;
    
    if(q.rear == -1)
    {
        printf("Queue Empty\n\n");
    }
    else
    {
        for(i=0; i < q.rear; i++)
        {
            printf("%d-->", q.elements[i]);
        }
        printf("%d\n\n", q.elements[i]);
    }
}

int main(int argc, char *argv[])
{
    int n, u, o, choice;
    struct queue q;
    
    system("clear");
    
    q.rear = -1;
    
    while(1)
    {
        printf("Select From Following:\n");
        printf("1. Insert an Element\n");
        printf("2. Remove an Element\n");
        printf("3. Display Contents\n");
        printf("4. Exit\n");
        printf("Enter Your Selection: ");
        fflush(stdin);
        scanf("%d" , &choice);
        printf("\n");
        
        if(choice == 1)
        {
            printf("Enter Your Element: ");
            fflush(stdin);
            scanf("%d" , &n);
            
            queue_insert(&q , n , &o);
            
            if(o == 1)
            {
                printf("Queue is Full.\nCan\'t Insert New Elements\n\n");
            }
            else
            {
                printf("Element Successfully Inserted into Queue\n\n");
            }
        }
        
        else if(choice == 2)
        {
            n = queue_remove(&q , &u);
            
            if(u == 1)
            {
                printf("Queue is empty.\nNo element to remove.\n\n");
            }
            else
            {
                printf("%d Removed From the Queue\n\n" , n);
            }
        }
        else if(choice == 3)
        {
            queue_display(q);
        }
        else if(choice == 4)
        {
            break;
        }
        else
        {
            printf("Enter a Valid Choice\n\n");
        }
    }
    
    return(0);
}
