// Name : Varun Barad
// Enrolment Number : 130070107008
// Subject : Data & File Structure
// Practical Number : 03

#include <stdio.h>
#include <stdlib.h>

#define MAX 20

struct stack
{
    int elements[MAX];
    int top;
};

// Function to Push an Element on to Stack
void push(struct stack *s , int n , int *o)
{
    if(s->top == (MAX-1))
    {
        *o = 1;
    }
    else
    {
        *o = 0;
        (s->top)++;
        s->elements[s->top] = n;
    }
}

// Function to Pop the Top-Element from the Stack
int pop(struct stack *s, int *u)
{
    int temp;
    if(s->top == (-1))
    {
        *u = 1;
        return(-1);
    }
    else
    {
        *u = 0;
        temp = s->elements[s->top];
        (s->top)--;
        return(temp);
    }
}

// Function to Peep the Top-Element from the Stack
int peep(struct stack s, int *u)
{
    if(s.top == (-1))
    {
        *u = 1;
        return(-1);
    }
    else
    {
        *u = 0;
        return(s.elements[s.top]);
    }
}

// Function to Change the Top-Element from the Stack
void change(struct stack *s, int *u)
{
    if(s->top == (-1))
    {
        *u = 1;
    }
    else
    {
        *u = 0;
        printf("Current Value of Top-Element: %d\n", s->elements[s->top]);
        printf("Enter the New Value to Store in Top-Element: ");
        scanf("%d", &(s->elements[s->top]));
    }
}

// Function to Display the Contents of Stack
void display(struct stack s)
{
    if(s.top > -1)
    {
        for(; s.top > 0 ; s.top--)
        {
            printf("%d->" , s.elements[s.top]);
        }
        printf("%d\n\n" , s.elements[s.top]);
    }
    else
    {
        printf("Stack is Empty\n\n");
    }
}

int main(int argc , char *argv[])
{
    struct stack s;
    int choice , n , o , u;
    
    system("clear");
    
    s.top = -1;
    
    while(1)
    {
        printf("Select Operation\n");
        printf("1. Push an Element\n");
        printf("2. Pop an Element\n");
        printf("3. Peep the Top-Element\n");
        printf("4. Change the Value of Top-Element\n");
        printf("5. Display the Contents\n");
        printf("6. Exit\n");
        printf("Enter Your Choice: ");
        fflush(stdin);
        scanf("%d" , &choice);
        printf("\n");
        
        if(choice == 1)
        {
            printf("Enter the Element to Push: ");
            fflush(stdin);
            scanf("%d" , &n);
            printf("\n");
            
            push(&s , n , &o);
            
            if(o == 1)
            {
                printf("OVERFLOW\nFailure to Push Element\n\n");
            }
            else
            {
                printf("Element Pushed Successfully\n\n");
            }
        }
        else if(choice == 2)
        {
            n = pop(&s , &u);
            
            if(u == 1)
            {
                printf("UNDERFLOW\nFailure to Pop Element\n\n");
            }
            else
            {
                printf("Element Successfully Popped\nValue: %d\n\n" , n);
            }
        }
        else if(choice == 3)
        {
            n = peep(s, &u);
            
            if(u == 1)
            {
                printf("UNDERFLOW\nFailure to Peep Element\n\n");
            }
            else
            {
                printf("Element Successfully Peeped\nValue: %d\n\n", n);
            }
        }
        else if(choice == 4)
        {
            change(&s, &u);
            
            if(u == 1)
            {
                printf("UNDERFLOW\nFailure to Change the Top-Element\n\n");
            }
            else
            {
                printf("Top-Element Successfully Changed\n\n");
            }
        }
        else if(choice == 5)
        {
            display(s);
        }
        else if(choice == 6)
        {
            break;
        }
        else
        {
            printf("!!! Enter Something Sensible You Moron !!!\n\n");
        }
    }
    
    printf("Thank You\n\n");
    
    return(0);
}
