#!/bin/python3

# Name : Varun Barad
# Enrollment Number : 130070107008
# Subject : Data & File Structure
# Practical Number : 15

def sort_merge(numbers):
    left = []
    right = []
    
    if len(numbers) > 1:
        left = sort_merge(numbers[0:int(len(numbers)/2)])
        right = sort_merge(numbers[(int(len(numbers)/2)):])
        
        del numbers
        numbers = []
        
        i = 0
        j = 0
        
        while (i < len(left)) and (j < len(right)):
            if left[i] < right[j]:
                numbers.append(left[i])
                i += 1
            else:
                numbers.append(right[j])
                j += 1
        
        if i == len(left):
            while j < len(right):
                numbers.append(right[j])
                j += 1
        else:
            while i < len(left):
                numbers.append(left[i])
                i += 1
    
    del left
    del right
    
    return numbers

def main():
    
    numbers = [25, 37, 15, 65, 42, 5, 98]
    
    print('')
    
    print('Unsorted List:')
    for num in numbers:
        print(num)
    
    print('\n')
    
    numbers = sort_merge(numbers)
    
    print('Sorted List:')
    for num in numbers:
        print(num)
    
    print('\n')
    print('Thank You\n')


if __name__ == '__main__':
    main()
