// Name : Varun Barad
// Enrolment Number : 130070107008
// Subject : Data & File Structure
// Practical Number : 10

#include <stdio.h>
#include <stdlib.h>

struct node
{
    int n;
    struct node *previous;
    struct node *next;
};

struct node* getNode(int n)
{
    struct node *p = NULL;
    p = (struct node *) malloc(sizeof(struct node));
    p->n = n;
    p->previous = NULL;
    p->next = NULL;
    return(p);
}

void list_insert(struct node **start, int n, int pos, int *o)
{
    struct node *p, *temp;
    
    p = getNode(n);
    
    if(p == NULL)
    {
        *o = 1;
    }
    else
    {
        *o = 0;
        
        if(pos == 0)
        {
            if((*start) == NULL)
            {
                *start = p;
            }
            else
            {
                (*start)->previous = p;
                p->next = (*start);
                (*start) = p;
            }
        }
        else
        {
            if((*start) == NULL)
            {
                *start = p;
            }
            else
            {
                temp = (*start);
                while((temp->next) != NULL)
                {
                    temp = temp->next;
                }
                temp->next = p;
                p->previous = temp;
            }
        }
    }
}

int list_remove(struct node **start, int index, int *u)
{
    struct node *temp;
    int i, value;
    
    if(((*start) == NULL) || (index == 0))
    {
        *u = 1;
        value = -1;
    }
    else
    {
        *u = 0;
        
        temp = (*start);
        
        if(index == (-1))
        {
            if((temp->next) == NULL)
            {
                value = temp->n;
                free(temp);
                *start = NULL;
            }
            else
            {
                while((temp->next) != NULL)
                {
                    temp = temp->next;
                }
                (temp->previous)->next = NULL;
                value = temp->n;
                free(temp);
            }
        }
        else
        {
            if(index == 1)
            {
                temp = *start;
                *start = (*start)->next;
                value = temp->n;
                free(temp);
            }
            else
            {
                for(i=0; (i < index) && (temp != NULL); i++)
                {
                    temp = temp->next;
                }
                
                if(temp == NULL)
                {
                    *u = 1;
                }
                else
                {
                    temp = temp->previous;
                    value = temp->n;
                    (temp->next)->previous = temp->previous;
                    if((temp->previous) != NULL)
                    {
                        (temp->previous)->next = temp->next;
                    }
                    free(temp);
                }
            }
        }
    }
    
    return(value);
}

void list_display(struct node *start)
{
    if(start == NULL)
    {
        printf("List Empty\n\n");
    }
    else
    {
        while((start->next) != NULL)
        {
            printf("%d-->", (start->n));
            start = start->next;
        }
        printf("%d\n\n", (start->n));
    }
}

void list_finish(struct node **start)
{
    struct node *temp;
    while((*start) != NULL)
    {
        temp = *start;
        *start = (*start)->next;
        free(temp);
    }
}

int main(int argc, char *argv[])
{
    int choice, n, index, o, u;
    struct node *start;
    
    system("clear");
    
    start = NULL;
    
    while(1)
    {
        printf("Select From Following:\n");
        printf("1. Insert Node at Front of List\n");
        printf("2. Insert Node at End of List\n");
        printf("3. Remove Last Node of List\n");
        printf("4. Remove Node Before a Specified Position\n");
        printf("5. Display Contents of List\n");
        printf("6. Exit\n");
        printf("Enter Your Choice: ");
        fflush(stdin);
        scanf("%d", &choice);
        printf("\n");
        
        if(choice == 1)
        {
            printf("Enter Value to Insert: ");
            fflush(stdin);
            scanf("%d", &n);
            
            index = 0;
            
            list_insert(&start, n, index, &o);
            
            if(o == 1)
            {
                printf("OVERFLOW\nFailure to Insert Element\n\n");
            }
            else
            {
                printf("Element Inserted Successfully\n\n");
            }
        }
        else if(choice == 2)
        {
            printf("Enter Value to Insert: ");
            fflush(stdin);
            scanf("%d", &n);
            
            index = 1;
            
            list_insert(&start, n, index, &o);
            
            if(o == 1)
            {
                printf("OVERFLOW\nFailure to Insert Element\n\n");
            }
            else
            {
                printf("Element Inserted Successfully\n\n");
            }
        }
        else if(choice == 3)
        {
            index = -1;
            n = list_remove(&start, index, &u);
            
            if(u == 1)
            {
                printf("UNDERFLOW\nFailure to Remove Element\n\n");
            }
            else
            {
                printf("Element Removed Successfully\nValue: %d\n\n", n);
            }
        }
        else if(choice == 4)
        {
            printf("Enter Index(Starts from 0): ");
            fflush(stdin);
            scanf("%d", &index);
            printf("\n");
            
            n = list_remove(&start, index, &u);
            
            if(u == 1)
            {
                printf("UNDERFLOW\nFailure to Remove Element\n\n");
            }
            else
            {
                printf("Element Removed Successfully\nValue: %d\n\n", n);
            }
        }
        else if(choice == 5)
        {
            list_display(start);
        }
        else if(choice == 6)
        {
            break;
        }
        else
        {
            printf("Do You Just Mash The Buttons??\n\n");
        }
    }
    
    list_finish(&start);
    printf("Thank You\n");
    
    return(0);
}
