// Name : Varun Barad
// Enrolment Number : 130070107008
// Subject : Data & File Structure
// Practical Number : 02

#include <stdio.h>
#include <stdlib.h>

#define MAX 10

void display(int *n)
{
    int i;
    
    printf("Indices: ");
    for(i=0; i<(MAX-1); i++)
    {
        printf("%3d, ", i);
    }
    printf("%3d\n", i);
    
    printf("Values : ");
    for(i=0; i<(MAX-1); i++)
    {
        printf("%3d, ", *(n+i));
    }
    printf("%3d\n\n", *(n+i));
}

int main(int argc, char *argv[])
{
    int *m, *c, i;
    
    system("clear");
    
    printf("Usage of malloc():\n\n");
    
    m = (int *) malloc(MAX*sizeof(int));
    
    for(i=0; i<MAX; i++)
    {
        *(m+i) = i;
    }
    
    display(m);
    printf("\n");
    
    printf("Usage of calloc():\n\n");
    
    c = (int *) calloc(sizeof(int), MAX);
    
    for(i=0; i<MAX; i++)
    {
        *(c+i) = (2*i);
    }
    
    display(c);
    printf("\n");
    
    printf("Usage of free():\n\n");
    free(m);
    printf("m freed\n");
    free(c);
    printf("c freed\n");
    
    printf("\n");
    return(0);
}
