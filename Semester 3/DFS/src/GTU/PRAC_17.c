// Name : Varun Barad
// Enrolment Number : 130070107008
// Subject : Data & File Structure
// Practical Number : 15

#include <stdio.h>
#include <stdlib.h>

#define MAX 5

int* sort_bubble(int *a, int n)
{
    int i, j;
    for(i=0; i < (n-1); i++)
    {
        for(j=(i+1); j < n; j++)
        {
            if(*(a+i) > *(a+j))
            {
                *(a+i) = *(a+i) + *(a+j);
                *(a+j) = *(a+i) - *(a+j);
                *(a+i) = *(a+i) - *(a+j);
            }
        }
    }
    return(a);
}

int search_binary(int *a, int ele, int beg, int end)
{
    int mid, index;
    if(beg > end)
    {
        return(-1);
    }
    else
    {
        mid = (end + beg) / 2;
        if(*(a+mid) == ele)
        {
            index = mid;
        }
        else if(*(a+mid) > ele)
        {
            end = mid-1;
            index = search_binary(a, ele, beg, end);
        }
        else
        {
            beg = mid+1;
            index = search_binary(a, ele, beg, end);
        }
        return(index);
    }
}

int main(int argc, char *argv[])
{
    int *nums, i, element, index;
    
    system("clear");
    
    nums = (int *) malloc(MAX*sizeof(int));
    
    printf("Enter %d Numbers to Insert in Array:\n", MAX);
    
    for(i=0; i < MAX; i++)
    {
        scanf("%d", (nums+i));
    }
    printf("\n");
    
    nums = sort_bubble(nums, MAX);
    
    printf("Enter Element to Search for: ");
    scanf("%d", &element);
    printf("\n");
    
    index = search_binary(nums, element, 0, (MAX-1));
    
    if(index == -1)
    {
        printf("Element not Found in Array\n");
    }
    else
    {
        printf("Element Found at Position %d\n", (index+1));
    }
    
    return(0);
}
