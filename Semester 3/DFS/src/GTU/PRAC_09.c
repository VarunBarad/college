// Name : Varun Barad
// Enrolment Number : 130070107008
// Subject : Data & File Structure
// Practical Number : 09

#include <stdio.h>
#include <stdlib.h>

struct node
{
    int n;
    struct node *next;
};

struct node* getNode(int n)
{
    struct node *p = NULL;
    p = (struct node *) malloc(sizeof(struct node));
    p->n = n;
    p->next = NULL;
    return(p);
}

void queue_insert(struct node **front, struct node **rear, int n, int *o)
{
    struct node *p = getNode(n);
    
    if(p == NULL)
    {
        *o = 1;
    }
    else
    {
        *o = 0;
        
        if(*front == NULL)
        {
            *front = p;
            *rear = p;
        }
        else
        {
            (*rear)->next = p;
            *rear = p;
        }
    }
}

int queue_remove(struct node **front, struct node **rear, int *u)
{
    int value;
    struct node *temp = *front;
    
    if(*front == NULL)
    {
        *u = 1;
        value = -1;
    }
    else
    {
        *u = 0;
        value = temp->n;
        
        if(*front == *rear)
        {
            *front = NULL;
            *rear = NULL;
        }
        else
        {
            *front = (*front)->next;
        }
    }
    
    return(value);
}

void queue_display(struct node *front)
{
    if(front == NULL)
    {
        printf("Queue Empty\n\n");
    }
    else
    {
        while((front->next) != NULL)
        {
            printf("%d-->", front->n);
            front = front->next;
        }
        printf("%d\n\n", front->n);
    }
}

void finish(struct node **front, struct node **rear)
{
    while((*front) != NULL)
    {
        *rear = *front;
        *front = (*front)->next;
        free(*rear);
    }
    *rear = NULL;
}

int main(int argc, char *argv[])
{
    int n, choice, o, u;
    struct node *front, *rear;
    
    system("clear");
    
    front = NULL;
    rear = NULL;
    
    while(1)
    {
        printf("Select Operation:\n");
        printf("1. Insert an Element\n");
        printf("2. Remove an Element\n");
        printf("3. Display Contents\n");
        printf("4. Exit\n");
        printf("Enter Your Choice: ");
        fflush(stdin);
        scanf("%d", &choice);
        printf("\n");
        
        if(choice == 1)
        {
            printf("Enter the Element to Insert: ");
            fflush(stdin);
            scanf("%d", &n);
            
            queue_insert(&front, &rear, n, &o);
            
            if(o == 1)
            {
                printf("OVERFLOW\nFailure to Insert Element\n\n");
            }
            else
            {
                printf("Element Successfully Inserted\n\n");
            }
        }
        else if(choice == 2)
        {
            n = queue_remove(&front, &rear, &u);
            
            if(u == 1)
            {
                printf("UNDERFLOW\nFailure to Remove Element\n\n");
            }
            else
            {
                printf("Element Successfully Removed\nValue: %d\n\n", n);
            }
        }
        else if(choice == 3)
        {
            queue_display(front);
        }
        else if(choice == 4)
        {
            break;
        }
        else
        {
            printf("Can\'t You See The Choices\n\n");
        }
    }
    
    finish(&front, &rear);
    printf("Thank You\n");
    
    return(0);
}
