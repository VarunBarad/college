// Name : Varun Barad
// Enrolment Number : 130070107008
// Subject : Data & File Structure
// Practical Number : 14

#include <stdio.h>
#include <stdlib.h>

#define MAX 5

int* sort_bubble(int *a, int n)
{
    int i, j;
    for(i=0; i < (n-1); i++)
    {
        for(j=(i+1); j < n; j++)
        {
            if(*(a+i) > *(a+j))
            {
                *(a+i) = *(a+i) + *(a+j);
                *(a+j) = *(a+i) - *(a+j);
                *(a+i) = *(a+i) - *(a+j);
            }
        }
    }
    return(a);
}

void display_array(int *a, int n)
{
    int i;
    for(i=0; i < (n-1); i++)
    {
        printf("%d->", *(a+i));
    }
    printf("%d\n", *(a+i));
}

int main(int argc, char *argv[])
{
    int *nums, i;
    
    system("clear");
    
    nums = (int *) malloc(MAX * sizeof(int));
    
    printf("Enter %d Numbers to Insert in Array:\n", MAX);
    
    for(i=0; i < MAX; i++)
    {
        scanf("%d", (nums+i));
    }
    printf("\n");
    
    printf("Original Array:\n");
    display_array(nums, MAX);
    
    printf("\n");
    
    nums = sort_bubble(nums, MAX);
    
    printf("Sorted Array:\n");
    display_array(nums, MAX);
    
    return(0);
}
