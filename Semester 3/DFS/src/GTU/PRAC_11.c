// Name : Varun Barad
// Enrolment Number : 130070107008
// Subject : Data & File Structure
// Practical Number : 11

#include <stdio.h>
#include <stdlib.h>

struct node
{
    int n;
    struct node *next;
};

struct node* getNode(int n)
{
    struct node *p = NULL;
    p = (struct node *) malloc(sizeof(struct node));
    if(p != NULL)
    {
        p->n = n;
        p->next = NULL;
    }
    return(p);
}

void list_insert(struct node **end, int n, int index, int *o)
{
    int i;
    struct node *p, *temp, *start;
    
    p = getNode(n);
    
    if(p == NULL)
    {
        *o = 1;
    }
    else
    {
        *o = 0;
        
        if(index == (-1))
        {
            if((*end) == NULL)
            {
                p->next = p;
                *end = p;
            }
            else
            {
                p->next = (*end)->next;
                (*end)->next = p;
                *end = p;
            }
        }
        else
        {
             if((*end) == NULL)
             {
                *o = 1;
             }
             else
             {
                start = (*end)->next;
                 (*end)->next = NULL;
                 temp = start;
                 if(index == 0)
                 {
                    p->next = start;
                    start = p;
                 }
                 else
                 {
                    for(i=1; (i < index) && (temp != NULL); i++)
                    {
                        temp = temp->next;
                    }
                    if((temp == NULL) || ((temp->next) == NULL))
                    {
                        *o = 2;
                    }
                    else
                    {
                        p->next = temp->next;
                        temp->next = p;
                    }
                 }
                 
                 (*end)->next = start;
             }
        }
    }
}

int list_remove(struct node **end, int index, int *u)
{
    struct node *temp, *start;
    int i, value;
    
    if((*end) == NULL)
    {
        *u = 1;
        value = -1;
    }
    else
    {
        *u = 0;
        
        start = (*end)->next;
        (*end)->next = NULL;
        
        if(index == (-1))
        {
            if(start == (*end))
            {
                temp = start;
                value = temp->n;
                start = NULL;
                *end = NULL;
                free(temp);
            }
            else
            {
                temp = start;
                value = temp->n;
                start = start->next;
                free(temp);
            }
        }
        else
        {
            temp = start;
            for(i=0; (i < index) && (temp != NULL); i++)
            {
                temp = temp->next;
            }
            if((temp == NULL) || ((temp->next) == NULL))
            {
                *u = 1;
                value = -1;
            }
            else
            {
                value = (temp->next)->n;
                temp->next = (temp->next)->next;
            }
        }
        
        (*end)->next = start;
    }
    
    return(value);
}

void list_display(struct node *end)
{
    struct node *start, *temp;
    
    if(end == NULL)
    {
        printf("List Empty\n\n");
    }
    else
    {
        start = end->next;
        end->next = NULL;
        temp = start;
        
        while((temp->next) != NULL)
        {
            printf("%d-->", (temp->n));
            temp = temp->next;
        }
        printf("%d\n\n", (temp->n));
        
        end->next = start;
    }
}

void list_finish(struct node **end)
{
    struct node *start, *temp;
    if((*end) != NULL)
    {
        start = (*end)->next;
        (*end)->next = NULL;
        while(start != NULL)
        {
            temp = start;
            start = start->next;
            free(temp);
        }
        (*end) = NULL;
    }
}

int main(int argc, char *argv[])
{
    int choice, n, index, u, o;
    struct node *end;
    
    system("clear");
    
    end = NULL;
    
    while(1)
    {
        printf("Select From Following:\n");
        printf("1. Insert Element at End\n");
        printf("2. Insert Element Before Specified Position\n");
        printf("3. Delete First Element\n");
        printf("4. Delete Element After Specified Position\n");
        printf("5. Display Contents\n");
        printf("6. Exit\n");
        printf("Enter Your Choice: ");
        fflush(stdin);
        scanf("%d", &choice);
        printf("\n");
        
        if(choice == 1)
        {
            printf("Enter Value of Element: ");
            fflush(stdin);
            scanf("%d", &n);
            
            index = -1;
            
            list_insert(&end, n, index, &o);
            
            if(o == 1)
            {
                printf("OVERFLOW\nFailure to Insert Element\n\n");
            }
            else if(o == 2)
            {
                printf("INVALID INDEX\nFailure to Insert Element\n\n");
            }
            else
            {
                printf("Element Inserted Successfully\n\n");
            }
        }
        else if(choice == 2)
        {
            printf("Enter Value of Element: ");
            fflush(stdin);
            scanf("%d", &n);
            
            printf("Enter Index(Starts from 0): ");
            fflush(stdin);
            scanf("%d", &index);
            
            list_insert(&end, n, index, &o);
            
            if(o == 1)
            {
                printf("OVERFLOW\nFailure to Insert Element\n\n");
            }
            else if(o == 2)
            {
                printf("INVALID INDEX\nFailure to Insert Element\n\n");
            }
            else
            {
                printf("Element Inserted Successfully\n\n");
            }
        }
        else if(choice == 3)
        {
            index = -1;
            n = list_remove(&end, index, &u);
            
            if(u == 1)
            {
                printf("UNDERFLOW\nFailure to Remove Element\n\n");
            }
            else
            {
                printf("Element Removed Successfully\nValue: %d\n\n", n);
            }
        }
        else if(choice == 4)
        {
            printf("Enter Index(Starts from 0): ");
            fflush(stdin);
            scanf("%d", &index);
            
            n = list_remove(&end, index, &u);
            
            if(u == 1)
            {
                printf("UNDERFLOW\nFailure to Remove Element\n\n");
            }
            else
            {
                printf("Element Removed Successfully\nValue: %d\n\n", n);
            }
        }
        else if(choice == 5)
        {
            list_display(end);
        }
        else if(choice == 6)
        {
            break;
        }
        else
        {
            printf("See Before You Type\n\n");
        }
    }
    
    list_finish(&end);
    printf("Thank You\n");
    
    return(0);
}
