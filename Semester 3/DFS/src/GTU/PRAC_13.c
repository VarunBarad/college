// Name : Varun Barad
// Enrolment Number : 130070107008
// Subject : Data & File Structure
// Practical Number : 13

#include <stdio.h>
#include <stdlib.h>

#define MAX 10

struct node
{
    int n;
    struct node *left;
    struct node *right;
    int flag;
};

struct stack
{
    struct node* elements[MAX];
    int top;
};

void stack_push(struct stack *s, struct node *n)
{
    (s->top)++;
    s->elements[s->top] = n;
}

int stack_isEmpty(struct stack s)
{
    if(s.top == (-1))
    {
        return(1);
    }
    else
    {
        return(0);
    }
}

struct node* stack_pop(struct stack *s)
{
    if((s->top) == (-1))
    {
        return(NULL);
    }
    else
    {
        (s->top)--;
        return(s->elements[(s->top)+1]);
    }
}

struct node* get_node(int n)
{
    struct node *p = NULL;
    p = (struct node *) malloc(sizeof(struct node));
    if(p != NULL)
    {
        p->n = n;
        p->left = NULL;
        p->right = NULL;
        p->flag = 0;
    }
    return(p);
}

void insert(struct node **t, int n)
{
    struct node *p, *q;
    if(*t == NULL)
    {
        *t = get_node(n);
    }
    else
    {
        p = get_node(n);
        q = *t;
        while(((q->left != NULL) && (q->n > n)) || ((q->right != NULL) && (q->n <= n)))
        {
            if(q->n > n)
            {
                q = q->left;
            }
            else
            {
                q = q->right;
            }
        }
        if(q->n > n)
        {
            q->left = p;
        }
        else
        {
            q->right = p;
        }
    }
}

struct node* create_tree(int *a, int n)
{
    struct node *tree = NULL;
    int i;
    for(i=0; i < n; i++)
    {
        insert(&tree, *(a+i));
    }
    return(tree);
}

void reset_flags(struct node *n)
{
    if(n != NULL)
    {
        reset_flags(n->left);
        n->flag = 0;
        reset_flags(n->right);
    }
}

void traversal_recursive(struct node *root, int order, int flag)
{
    if((flag == 0) && (root == NULL))
    {
        printf("Empty Tree\n\n");
    }
    else
    {
        if(root != NULL)
        {
            if(order == (-1))
            {
                printf("%d  ", (root->n));
                traversal_recursive((root->left), -1, 1);
                traversal_recursive((root->right), -1, 1);
            }
            else if(order == 0)
            {
                traversal_recursive((root->left), 0, 1);
                printf("%d  ", (root->n));
                traversal_recursive((root->right), 0, 1);
            }
            else
            {
                traversal_recursive((root->left), 1, 1);
                traversal_recursive((root->right), 1, 1);
                printf("%d  ", (root->n));
            }
        }
    }
}

void traversal_non_recursive(struct node *root, int order)
{
    struct node *temp = root;
    struct stack s;
    s.top = -1;
    
    if(root == NULL)
    {
        printf("Empty Tree\n\n");
    }
    else
    {
        if(order == (-1))
        {
            while((root != NULL) || (!stack_isEmpty(s)))
            {
                if(root == NULL)
                {
                    root = stack_pop(&s);
                }
                else
                {
                    if((root->flag) == 0)
                    {
                        printf("%d  ", (root->n));
                    }
                    
                    if((root->left) != NULL)
                    {
                        if((root->flag) == 0)
                        {
                            root->flag = 1;
                            stack_push(&s, root);
                            root = root->left;
                        }
                        else
                        {
                            if((root->right) == NULL)
                            {
                                root = stack_pop(&s);
                            }
                            else
                            {
                                root = root->right;
                            }
                        }
                    }
                    else
                    {
                        if((root->right) == NULL)
                        {
                            root = stack_pop(&s);
                        }
                        else
                        {
                            root = root->right;
                        }
                    }
                }
            }
        }
        else if(order == 0)
        {
            while((root != NULL) || (!stack_isEmpty(s)))
            {
                if(root == NULL)
                {
                    root = stack_pop(&s);
                }
                else
                {
                    if((root->left) != NULL)
                    {
                        if((root->flag) == 0)
                        {
                            root->flag = 1;
                            stack_push(&s, root);
                            root = root->left;
                        }
                        else
                        {
                            printf("%d  ", (root->n));
                            if((root->right) == NULL)
                            {
                                root = stack_pop(&s);
                            }
                            else
                            {
                                root = root->right;
                            }
                        }
                    }
                    else
                    {
                        printf("%d  ", (root->n));
                        if((root->right) == NULL)
                        {
                            root = stack_pop(&s);
                        }
                        else
                        {
                            root = root->right;
                        }
                    }
                }
            }
        }
        else
        {
            while((root != NULL) || (!stack_isEmpty(s)))
            {
                if(root == NULL)
                {
                    root = stack_pop(&s);
                }
                else
                {
                    if((root->flag) == 2)
                    {
                        printf("%d  ", (root->n));
                        root = stack_pop(&s);
                    }
                    else
                    {
                        if((root->left) != NULL)
                        {
                            if((root->flag) == 0)
                            {
                                root->flag = 1;
                                stack_push(&s, root);
                                root = root->left;
                            }
                            else if((root->flag) == 1)
                            {
                                if((root->right) == NULL)
                                {
                                    printf("%d  ", (root->n));
                                    root = stack_pop(&s);
                                }
                                else
                                {
                                    root->flag = 2;
                                    stack_push(&s, root);
                                    root = root->right;
                                }
                            }
                        }
                        else
                        {
                            if((root->right) == NULL)
                            {
                                printf("%d  ", (root->n));
                                root = stack_pop(&s);
                            }
                            else
                            {
                                root->flag = 2;
                                stack_push(&s, root);
                                root = root->right;
                            }
                        }
                    }
                }
            }
        }
    }
    
    reset_flags(temp);
}

int main(int argc, char *argv[])
{
    int nums[5], i;
    struct node *tree;
    
    system("clear");
    
    printf("Enter %d Numbers to Insert in Tree:\n", MAX);
    for(i=0; i < MAX; i++)
    {
        fflush(stdin);
        scanf("%d", (nums+i));
    }
    
    tree = create_tree(nums, MAX);
    
    printf("\n\n");
    
    printf("Recursive Traversal of Tree:\n\n");
    printf("Pre-Order  : ");
    traversal_recursive(tree, -1, 0);
    printf("\n");
    printf("In-Order   : ");
    traversal_recursive(tree, 0, 0);
    printf("\n");
    printf("Post-Order : ");
    traversal_recursive(tree, 1, 0);
    
    printf("\n\n\n");
    printf("Non-Recursive Traversal:\n\n");
    printf("Pre-Order  : ");
    traversal_non_recursive(tree, -1);
    printf("\n");
    printf("In-Order   : ");
    traversal_non_recursive(tree, 0);
    printf("\n");
    printf("Post-Order : ");
    traversal_non_recursive(tree, 1);
    printf("\n");
    
    return(0);
}
