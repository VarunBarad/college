// Name : Varun Barad
// Enrolment Number : 130070107008
// Subject : Data & File Structure
// Practical Number : 04

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#define MAX 100

struct stack
{
    char data[MAX];
    int top;
};

int priority(char c)
{
    if(c == '(')
    {
        return(0);
    }
    else if((c == '+') || (c == '-'))
    {
        return(1);
    }
    else if((c == '*') || (c == '/'))
    {
        return(2);
    }
    else if(c == '%')
    {
        return(3);
    }
    else
    {
        return(4);
    }
}

void stack_init(struct stack *s)
{
    if(s != NULL)
    {
        s->top = -1;
    }
}

int stack_isEmpty(struct stack *s)
{
    if(s == NULL)
    {
        return(1);
    }
    else
    {
        if((s->top) == (-1))
        {
            return(1);
        }
        else
        {
            return(0);
        }
    }
}

void stack_push(struct stack *s, char c)
{
    (s->top)++;
    s->data[s->top] = c;
}

char stack_pop(struct stack *s)
{
    char c = s->data[s->top];
    (s->top)--;
    return(c);
}

char stack_peep(struct stack s)
{
    return(s.data[s.top]);
}

int main(int argc, char *argv[])
{
    struct stack s;
    char c, token;
    
    system("clear");
    
    stack_init(&s);
    
    printf("##################################\n");
    printf(" DISCLAIMER:\n");
    printf(" Please Provide Valid Input\n");
    printf(" This Thing Isn't Idiot-Proof Yet\n");
    printf("##################################\n");
    
    printf("Enter Infix Expression(Terminate with '.'): ");
    while((token = getchar()) != '.')
    {
        if(isalnum(token))
        {
            printf("%c", token);
        }
        else
        {
            if(token == '(')
            {
                stack_push(&s, token);
            }
            else
            {
                if(token == ')')
                {
                    while((c = stack_pop(&s)) != '(')
                    {
                        printf("%c", c);
                    }
                }
                else
                {
                    while((priority(token) <= priority(stack_peep(s))) && (!stack_isEmpty(&s)))
                    {
                        c = stack_pop(&s);
                        printf("%c", c);
                    }
                    stack_push(&s, token);
                }
            }
        }
    }
    
    while(!stack_isEmpty(&s))
    {
        c = stack_pop(&s);
        printf("%c", c);
    }
    
    printf("\n\n");
    
    return(0);
}
