// Name : Varun Barad
// Enrolment Number : 130070107008
// Subject : Data & File Structure
// Practical Number : 07

#include <stdio.h>
#include <stdlib.h>

struct node
{
    int n;
    struct node *next;
};

struct node* getNode(int n)
{
    struct node *p = NULL;
    p = (struct node *) malloc(sizeof(struct node));
    p->n = n;
    p->next = NULL;
    return(p);
}

void list_sort(struct node *start)
{
    struct node *i, *j;
    
    if(start != NULL)
    {
        for(i=start; (i->next) != NULL; i=i->next)
        {
            for(j=(i->next); j != NULL; j=j->next)
            {
                if((i->n) > (j->n))
                {
                    i->n = i->n + j->n;
                    j->n = i->n - j->n;
                    i->n = i->n - j->n;
                }
            }
        }
    }
}

void list_insert(struct node **start, int n, int pos, int *o)
{
    struct node *p = getNode(n);
    struct node *temp;
    
    if(p == NULL)
    {
        *o = 1;
    }
    else
    {
        *o = 0;
        
        if(pos == 0)
        {
            p->next = *start;
            *start = p;
        }
        else if(pos == 1)
        {
            temp = *start;
            
            if(temp == NULL)
            {
                *start = p;
            }
            else
            {
                if((temp->n) > n)
                {
                    p->next = *start;
                    *start = p;
                }
                else
                {
                    while((temp->next != NULL) && ((temp->next)->n < n))
                    {
                        temp = temp->next;
                    }
                    p->next = temp->next;
                    temp->next = p;
                }
            }
        }
        else
        {
            if((*start) == NULL)
            {
                *start = p;
            }
            else
            {
                temp = *start;
                while((temp->next) != NULL)
                {
                    temp = temp->next;
                }
                temp->next = p;
            }
        }
    }
}

int list_remove(struct node **start, int index, int pos, int *u)
{
    int value, i;
    struct node *temp, *parent;
    
    parent = NULL;
    
    if((*start) == NULL)
    {
        *u = 1;
        value = -1;
    }
    else
    {
        if((index == 0) && (pos == 1))
        {
            temp = *start;
            value = temp->n;
            *start = (*start)->next;
            free(temp);
        }
        else if((index == (-1)) && (pos == 1))
        {
            temp = *start;
            if((temp->next) == NULL)
            {
                *start = NULL;
                value = temp->n;
                free(temp);
            }
            else
            {
                while(((temp->next)->next) != NULL)
                {
                    temp = temp->next;
                }
                value = (temp->next)->n;
                parent = temp;
                temp = temp->next;
                free(temp);
                parent->next = NULL;
            }
        }
        else
        {
            temp = *start;
            
            if(pos == 0)
            {
                if(index == 0)
                {
                    *u = 1;
                    value = -1;
                }
                else
                {
                    for(i=0; (i < (index-1)) && (temp != NULL); i++)
                    {
                        parent = temp;
                        temp = temp->next;
                    }
                    if(temp == NULL)
                    {
                        *u = 1;
                        value = -1;
                    }
                    else
                    {
                        *u = 0;
                        
                        if(parent == NULL)
                        {
                            value = (*start)->n;
                            temp = *start;
                            *start = (*start)->next;
                            free(temp);
                        }
                        else
                        {
                            value = temp->n;
                            parent->next = temp->next;
                            free(temp);
                        }
                    }
                }
            }
            else if(pos == 1)
            {
                for(i=0; (i < index) && (temp != NULL); i++)
                {
                    parent = temp;
                    temp = temp->next;
                }
                if(temp == NULL)
                {
                    *u = 1;
                    value = -1;
                }
                else
                {
                    *u = 0;
                    
                    if(parent == NULL)
                        {
                            value = (*start)->n;
                            temp = *start;
                            *start = (*start)->next;
                            free(temp);
                        }
                        else
                        {
                            value = temp->n;
                            parent->next = temp->next;
                            free(temp);
                        }
                }
            }
            else
            {
                for(i=0; (i < index) && (temp != NULL); i++)
                {
                    temp = temp->next;
                }
                if((temp == NULL) || ((temp->next) == NULL))
                {
                    *u = 1;
                    value = -1;
                }
                else
                {
                    *u = 0;
                    value = (temp->next)->n;
                    parent = temp->next;
                    temp->next = (temp->next)->next;
                    free(parent);
                }
            }
        }
    }
    
    return(value);
}

void list_display(struct node *start)
{
    if(start == NULL)
    {
        printf("List Empty\n\n");
    }
    else
    {
        while((start->next) != NULL)
        {
            printf("%d-->", (start->n));
            start = start->next;
        }
        printf("%d\n\n", (start->n));
    }
}

void list_finish(struct node **start)
{
    struct node *temp;
    while((*start) != NULL)
    {
        temp = *start;
        *start = (*start)->next;
        free(temp);
    }
}

int main(int argc, char *argv[])
{
    int choice, n, index, pos, o, u;
    struct node *start;
    
    system("clear");
    
    start = NULL;
    
    while(1)
    {
        printf("Select From Following:\n");
        printf("1. Insert an Element at Front of List\n");
        printf("2. Insert an Element at End of List\n");
        printf("3. Insert an Element in Ascending Order\n");
        printf("4. Delete First Element from List\n");
        printf("5. Delete Element Before the Specified Position\n");
        printf("6. Delete Element After the Specified Position\n");
        printf("7. Display Contents of List\n");
        printf("8. Exit\n");
        printf("Enter Your Choice: ");
        fflush(stdin);
        scanf("%d", &choice);
        printf("\n");
        
        if(choice == 1)
        {
            printf("Enter Value to Insert: ");
            fflush(stdin);
            scanf("%d", &n);
            
            pos = 0;
            
            list_insert(&start, n, pos, &o);
            
            if(o == 1)
            {
                printf("OVERFLOW\nFailure to Insert Element\n\n");
            }
            else
            {
                printf("Element Successfully Inserted at Front\n\n");
            }
        }
        else if(choice == 2)
        {
            printf("Enter Value to Insert: ");
            fflush(stdin);
            scanf("%d", &n);
            
            pos = 2;
            
            list_insert(&start, n, pos, &o);
            
            if(o == 1)
            {
                printf("OVERFLOW\nFailure to Insert Element\n\n");
            }
            else
            {
                printf("Element Successfully Inserted at End\n\n");
            }
        }
        else if(choice == 3)
        {
            printf("Enter Value to Insert: ");
            fflush(stdin);
            scanf("%d", &n);
            
            pos = 1;
            
            list_sort(start);
            list_insert(&start, n, pos, &o);
            
            if(o == 1)
            {
                printf("OVERFLOW\nFailure to Insert Element\n\n");
            }
            else
            {
                printf("Element Successfully Inserted in Ascending Order\n\n");
            }
        }
        else if(choice == 4)
        {
            index = 0;
            pos = 1;
            n = list_remove(&start, index, pos, &u);
            
            if(u == 1)
            {
                printf("UNDEFLOW\nFailure to Remove Element\n\n");
            }
            else
            {
                printf("Element Removed Successfully\nValue: %d\n\n", n);
            }
        }
        else if(choice == 5)
        {
            printf("Enter Index(Starting from 0): ");
            fflush(stdin);
            scanf("%d", &index);
            
            pos = 0;
            n = list_remove(&start, index, pos, &u);
            
            if(u == 1)
            {
                printf("UNDEFLOW\nFailure to Remove Element\n\n");
            }
            else
            {
                printf("Element Removed Successfully\nValue: %d\n\n", n);
            }
        }
        else if(choice == 6)
        {
            printf("Enter Index(Starting from 0): ");
            fflush(stdin);
            scanf("%d", &index);
            
            pos = 2;
            n = list_remove(&start, index, pos, &u);
            
            if(u == 1)
            {
                printf("UNDEFLOW\nFailure to Remove Element\n\n");
            }
            else
            {
                printf("Element Removed Successfully\nValue: %d\n\n", n);
            }
        }
        else if(choice == 7)
        {
            list_display(start);
        }
        else if(choice == 8)
        {
            break;
        }
        else
        {
            printf("!!! Type Something Sensible Moron !!!\n\n");
        }
    }
    
    list_finish(&start);
    printf("Thank You\n");
    
    return(0);
}
