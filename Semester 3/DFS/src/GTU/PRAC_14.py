#!/bin/python3

# Name : Varun Barad
# Enrollment Number : 130070107008
# Subject : Data & File Structure
# Practical Number : 14

def sort_quick(numbers):
    small = []
    big = []
    
    if len(numbers) > 0:
        key = numbers[-1]
        
        for n in numbers[0:-1]:
            if n < key:
                small.append(n)
            else:
                big.append(n)
        
        del numbers
        
        small = sort_quick(small)
        big = sort_quick(big)
        
        small.append(key)
    
    return (small+big)

def main():
    
    numbers = [25, 37, 15, 65, 42, 5, 98]
    
    print('')
    
    print('Unsorted List:')
    for num in numbers:
        print(num)
    
    print('\n')
    
    numbers = sort_quick(numbers)
    
    print('Sorted List:')
    for num in numbers:
        print(num)
    
    print('\n')
    print('Thank You\n')


if __name__ == '__main__':
    main()
