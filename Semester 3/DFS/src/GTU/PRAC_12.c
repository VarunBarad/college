// Name : Varun Barad
// Enrolment Number : 130070107008
// Subject : Data & File Structure
// Practical Number : 12

#include <stdio.h>
#include <stdlib.h>

#define MAX 5

struct node
{
    int n;
    struct node *left;
    struct node *right;
};

struct node* get_node(int n)
{
    struct node *p = NULL;
    p = (struct node *) malloc(sizeof(struct node));
    p->n = n;
    p->left = NULL;
    p->right = NULL;
    return(p);
}

void insert(struct node **t, int n)
{
    struct node *p, *q;
    if(*t == NULL)
    {
        *t = get_node(n);
    }
    else
    {
        p = get_node(n);
        q = *t;
        while(((q->left != NULL) && (q->n > n)) || ((q->right != NULL) && (q->n <= n)))
        {
            if(q->n > n)
            {
                q = q->left;
            }
            else
            {
                q = q->right;
            }
        }
        if(q->n > n)
        {
            q->left = p;
        }
        else
        {
            q->right = p;
        }
    }
}

struct node* create_tree(int *a, int n)
{
    struct node *tree = NULL;
    int i;
    for(i=0; i < n; i++)
    {
        insert(&tree, *(a+i));
    }
    return(tree);
}

void inorder(struct node *tree)
{
    if(tree != NULL)
    {
        inorder(tree->left);
        printf("%d  ", tree->n);
        inorder(tree->right);
    }
}

int main(int argc, char *argv[])
{
    int nums[5], i;
    struct node *tree;
    
    system("clear");
    
    printf("Enter %d Numbers to Insert in Tree:\n", MAX);
    for(i=0; i < MAX; i++)
    {
        fflush(stdin);
        scanf("%d", (nums+i));
    }
    
    tree = create_tree(nums, MAX);
    
    printf("\n");
    printf("In-Order Traversal of the Tree:\n");
    inorder(tree);
    printf("\n");
    
    return(0);
}
