// Name : Varun Barad
// Enrolment Number : 130070107008
// Subject : Data & File Structure
// Practical Number : 01

#include <stdio.h>
#include <stdlib.h>

void call_by_value(int a, int b)
{
    a = a + b;
    b = a - b;
    a = a - b;
    printf("Swapped Values in call_by_value()\n");
    printf("a : %d\n", a);
    printf("b : %d\n", b);
}

void call_by_referrence(int *a, int *b)
{
    *a = *a + *b;
    *b = *a - *b;
    *a = *a - *b;
    printf("Swapped Values in call_by_referrence()\n");
    printf("a : %d\n", *a);
    printf("b : %d\n", *b);
}

int main(int argc, char *argv[])
{
    int a, b;
    
    system("clear");
    
    a = 10;
    b = 20;
    
    printf("Original Values in main()\n");
    printf("a : %d\n", a);
    printf("b : %d\n", b);
    
    call_by_value(a, b);
    
    printf("Values in main() after call_by_value\n");
    printf("a : %d\n", a);
    printf("b : %d\n", b);
    
    call_by_referrence(&a, &b);
    
    printf("Values in main() after call_by_referrence\n");
    printf("a : %d\n", a);
    printf("b : %d\n", b);
    
    printf("Thank You\n");
    
    exit(0);
}
