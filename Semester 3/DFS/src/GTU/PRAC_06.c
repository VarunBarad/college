// Name : Varun Barad
// Enrolment Number : 130070107008
// Subject : Data & File Structure
// Practical Number : 06

#include <stdio.h>
#include <stdlib.h>

#define MAX 100

struct queue
{
    int elements[MAX];
    int front;
    int rear;
    int size;
};

int increment(int n)
{
    if(n == (MAX - 1))
    {
        return(0);
    }
    else
    {
        return(n+1);
    }
}

int decrement(int n)
{
    if(n == 0)
    {
        return(MAX - 1);
    }
    else
    {
        return(n - 1);
    }
}

void queue_insert(struct queue *q , int n , int *o)
{
    if(q->size < MAX)
    {
        if(q->size != 0)
        {
            q->rear = increment(q->rear);
        }
        q->elements[q->rear] = n;
        q->size++;
        *o = 0;
    }
    else
    {
        *o = 1;
    }
}

int queue_remove(struct queue *q , int *u)
{
    int n;
    
    if(q->size > 0)
    {
        n = q->elements[q->front];
        if(q->size != 1)
        {
            q->front = increment(q->front);
        }
        q->size--;
        *u = 0;
    }
    else
    {
        n = 0;
        *u = 1;
    }
    
    return(n);
}

void queue_display(struct queue q)
{
    int i;
    
    if(q.size == 0)
    {
        printf("Queue Empty\n\n");
    }
    else
    {
        for(i = q.size ; i > 1 ; i--)
        {
            printf("%d --> " , q.elements[q.front]);
            q.front = increment(q.front);
        }
        printf("%d\n\n" , q.elements[q.front]);
    }
}

int main()
{
    struct queue q;
    int choice , n , o , u;
    
    system("clear");
    
    q.front = 0;
    q.rear = 0;
    q.size = 0;
    
    while(1)
    {
        printf("Select From Following:\n");
        printf("1. Insert an Element\n");
        printf("2. Remove an Element\n");
        printf("3. Display Contents\n");
        printf("4. Exit\n");
        printf("Enter Your Selection: ");
        fflush(stdin);
        scanf("%d" , &choice);
        printf("\n");
        
        if(choice == 1)
        {
            printf("Enter Your Element: ");
            fflush(stdin);
            scanf("%d" , &n);
            
            queue_insert(&q , n , &o);
            
            if(o == 1)
            {
                printf("Queue is Full.\nCan\'t Insert New Elements\n\n");
            }
            else
            {
                printf("Element Successfully Inserted into Queue\n\n");
            }
        }
        
        else if(choice == 2)
        {
            n = queue_remove(&q , &u);
            
            if(u == 1)
            {
                printf("Queue is empty.\nNo element to remove.\n\n");
            }
            else
            {
                printf("%d Removed From the Queue\n\n" , n);
            }
        }
        else if(choice == 3)
        {
            queue_display(q);
        }
        else if(choice == 4)
        {
            break;
        }
        else
        {
            printf("Enter a Valid Choice\n\n");
        }
    }
    
    printf("\n");
    return(0);
}
