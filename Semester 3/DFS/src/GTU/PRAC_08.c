// Name : Varun Barad
// Enrolment Number : 130070107008
// Subject : Data & File Structure
// Practical Number : 08

#include <stdio.h>
#include <stdlib.h>

struct node
{
    int n;
    struct node *next;
};

// Helper Function to Create and Return the Address of a New Node
struct node* getNode(int n)
{
    struct node *p = NULL;
    p = (struct node *) malloc(sizeof(struct node));
    if(p != NULL)
    {
        p->n = n;
        p->next = NULL;
    }
    return(p);
}

// Function to Push a New Element on to the Stack
void push(struct node **top , int n , int *o)
{
    struct node *p = getNode(n);
    if(p == NULL)
    {
        *o = 1;
    }
    else
    {
        *o = 0;
        p->next = *top;
        *top = p;
    }
}

// Function to Pop the Top-most Element from Stack and Return it's Value
int pop(struct node **top , int *u)
{
    struct node *temp;
    int n;
    temp = *top;
    if(temp == NULL)
    {
        *u = 1;
        return(-1);
    }
    else
    {
        *u = 0;
        *top = temp->next;
        n = temp->n;
        free(temp);
        return(n);
    }
}

// Function to Display Complete Contents of the Stack
void display(struct node *top)
{
    if(top == NULL)
    {
        printf("The Stack is Empty\n\n");
    }
    else
    {
        while((top->next) != NULL)
        {
            printf("%d --> " , (top->n));
            top = top->next;
        }
        printf("%d\n\n" , (top->n));
    }
}

// Helper Function to Free the Memory Occupied by the Stack
void finish(struct node *p)
{
    struct node *temp;
    while(p != NULL)
    {
        temp = p;
        free(temp);
        p = p->next;
    }
}

int main(int argc , char *argv[])
{
    struct node *top;
    int choice , n , o , u;
    
    system("clear");
    
    top = NULL;
    
    while(1)
    {
        printf("Select Operation\n");
        printf("1. Push an Element\n");
        printf("2. Pop an Element\n");
        printf("3. Display the Contents\n");
        printf("4. Exit\n");
        printf("Enter Your Choice: ");
        fflush(stdin);
        scanf("%d" , &choice);
        printf("\n");
        
        if(choice == 1)
        {
            printf("Enter Element to Push on Stack: ");
            fflush(stdin);
            scanf("%d" , &n);
            
            push(&top , n , &o);
            
            if(o == 1)
            {
                printf("OVERFLOW\nFailure to Push Element\n\n");
            }
            else
            {
                printf("Element Pushed Successfully\n\n");
            }
        }
        else if(choice == 2)
        {
            n = pop(&top , &u);
            
            if(u == 1)
            {
                printf("UNDERFLOW\nFailure to Pop Element\n\n");
            }
            else
            {
                printf("Element Popped: %d\n\n" , n);
            }
        }
        else if(choice == 3)
        {
            display(top);
        }
        else if(choice == 4)
        {
            finish(top);
            break;
        }
        else
        {
            printf("!!! Can\'t You Read the Options !!!");
        }
    }
    
    printf("Thank You\n\n");
    
    return(0);
}
