#include <stdio.h>
#include <stdlib.h>

#define MAX 10

int insert(int *arr , int *length , int n)
{
	int i , pos;
	if(*length < MAX)
	{
		for(i = 0 ; ((i < *length) && (*(arr+i) < n)) ; i++);
		if(i == *length)
		{
			*(arr+i) = n;
			pos = i;
			(*length)++;
		}
		else
		{
			pos = i;
			(*length)++;
			for(i = *length ; i > pos ; i--)
			{
				*(arr+i) = *(arr+i-1);
			}
			*(arr+pos) = n;
		}
		return(pos);
	}
	else
	{
		return(-1);
	}
}

void display(int *arr , int length)
{
	int i;
	for(i = 0 ; i < length ; i++)
	{
		printf("%d->" , *(arr+i));
	}
	printf("\n\n");
}

int main(int argc , char *argv[])
{
	int array[MAX] , length = 0 , choice , n , result;
	
	system("clear");
	
	while(1)
	{
		printf("Select Operation\n");
		printf("1. Insert Element\n");
		printf("2. Display Contents\n");
		printf("3. Exit\n");
		printf("Enter Your Choice: ");
		fflush(stdin);
		scanf("%d" , &choice);
		printf("\n");
		
		if(choice == 1)
		{
			printf("Enter Value to Insert: ");
			fflush(stdin);
			scanf("%d" , &n);
			printf("\n");
			
			result = insert(array , &length , n);
			
			if(result == -1)
			{
				printf("Array Already Full\nFailed to Insert Element\n\n");
			}
			else
			{
				printf("Element Inserted at Position %d\n\n" , result);
			}
		}
		else if(choice == 2)
		{
			display(array , length);
		}
		else if(choice == 3)
		{
			break;
		}
		else
		{
			printf("Enter A Valid Option\n\n");
		}
	}
	printf("Thank You\n");
	
	return(0);
}
