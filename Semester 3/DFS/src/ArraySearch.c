#include <stdio.h>
#include <stdlib.h>

#define MAX 10

int insert(int *arr , int *length , int n)
{
	int i , pos;
	if(*length < MAX)
	{
		for(i = 0 ; ((i < *length) && (*(arr+i) < n)) ; i++);
		if(i == *length)
		{
			*(arr+i) = n;
			pos = i;
			(*length)++;
		}
		else
		{
			pos = i;
			(*length)++;
			for(i = *length ; i > pos ; i--)
			{
				*(arr+i) = *(arr+i-1);
			}
			*(arr+pos) = n;
		}
		return(pos);
	}
	else
	{
		return(-1);
	}
}

int delete(int *arr , int *length , int n)
{
	int i , pos;
	for(i = 0 ; ((i < *length) && (*(arr+i) != n)) ; i++);
	if(i == *length)
	{
		return(-1);
	}
	else
	{
		pos = i;
		for(; i < (*length - 1) ; i++)
		{
			*(arr + i) = *(arr + i + 1);
		}
		(*length)--;
		return(pos);
	}
}

int search(int *arr , int length , int n)
{
	int i;
	for(i = 0 ; i < length ; i++)
	{
		if(*(arr+i) == n)
		{
			return(i);
		}
	}
	return(-1);
}

void display(int *arr , int length)
{
	int i;
	for(i = 0 ; i < length ; i++)
	{
		printf("%d->" , *(arr+i));
	}
	printf("\n\n");
}

int main(int argc , char *argv[])
{
	int array[MAX] , length = 0 , choice , n , result;
	
	system("clear");
	
	while(1)
	{
		printf("Select Operation\n");
		printf("1. Insert Element\n");
		printf("2. Delete an Element\n");
		printf("3. Search for an Element\n");
		printf("4. Display Contents\n");
		printf("5. Exit\n");
		printf("Enter Your Choice: ");
		fflush(stdin);
		scanf("%d" , &choice);
		printf("\n");
		
		if(choice == 1)
		{
			printf("Enter Value to Insert: ");
			fflush(stdin);
			scanf("%d" , &n);
			printf("\n");
			
			result = insert(array , &length , n);
			
			if(result == -1)
			{
				printf("Array Already Full\nFailed to Insert Element\n\n");
			}
			else
			{
				printf("Element Inserted at Position %d\n\n" , result);
			}
		}
		else if(choice == 2)
		{
			printf("Enter Element to Delete: ");
			fflush(stdin);
			scanf("%d" , &n);
			printf("\n");
			
			result = delete(array , &length , n);
			
			if(result == -1)
			{
				printf("Element Not Found in Array\nFailed to Delete Element\n\n");
			}
			else
			{
				printf("Element Deleted from Position %d\n\n" , result);
			}
		}
		else if(choice == 3)
		{
			printf("Enter Element to Search for: ");
			fflush(stdin);
			scanf("%d" , &n);
			printf("\n");
			
			result = search(array , length , n);
			
			if(result == -1)
			{
				printf("Element Not Found\nFailure to Retrieve Index\n\n");
			}
			else
			{
				printf("Element Found at Position %d\n\n" , result);
			}
		}
		else if(choice == 4)
		{
			display(array , length);
		}
		else if(choice == 5)
		{
			break;
		}
		else
		{
			printf("Enter A Valid Option\n\n");
		}
	}
	printf("Thank You\n");
	
	return(0);
}
