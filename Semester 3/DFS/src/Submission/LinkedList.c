// Author: Varun B Barad
// Enrolment No.: 130070107008
// Batch: 2A3

// Subject: Data and File Structure
// Practical: 04
// Topic: Basic Operations on a Singly-Linked List

#include <stdio.h>
#include <stdlib.h>

struct node
{
    int n;
    struct node *next;
};

// Helper Function which Provides a New Node with the Supplied Information Stored in it
struct node* getNode(int n)
{
    struct node *p = (struct node *) malloc(sizeof(struct node));
    p->n = n;
    p->next = NULL;
    return(p);
}

// Function to Insert the Given Value at Appropriate Position in the Sorted List
void insert(struct node **p , int n)
{
    struct node *q = getNode(n);
    struct node *temp = *p;
    if(temp == NULL)
    {
        *p = q;
    }
    else if(temp->n > n)
    {
        q->next = temp;
        *p = q;
    }
    else
    {
        while((temp->next != NULL) && ((temp->next)->n < n))
        {
            temp = temp->next;
        }
        q->next = temp->next;
        temp->next = q;
    }
}

// Function to Delete the First Instance of the Supplied Value from the List
int delete(struct node **p , int n)
{
    int pos = 1;
    struct node *temp = *p;
    struct node *q;
    if(temp != NULL)
    {
        if(temp->n == n)
        {
            *p = temp->next;
            free(temp);
            return(0);
        }
        else
        {
            while((temp->next != NULL) && ((temp->next)->n != n))
            {
                temp = temp->next;
                pos++;
            }
            if(temp->next == NULL)
            {
                return(-1);
            }
            else
            {
                q = temp->next;
                temp->next = (temp->next)->next;
                free(q);
                return(pos);
            }
        }
    }
    else
    {
        return(-1);
    }
}

// Function to Find the Index of an element in a List
int search(struct node *p , int n)
{
    int pos = 1;
    if(p != NULL)
    {
        if(p->n == n)
        {
            return(0);
        }
        else
        {
            while((p->next != NULL) && ((p->next)->n != n))
            {
                p = p->next;
                pos++;
            }
            if(p->next == NULL)
            {
                return(-1);
            }
            else
            {
                return(pos);
            }
        }
    }
    else
    {
        return(-1);
    }
}

// Function to Display the Contents of the List
void display(struct node *p)
{
    if(p == NULL)
    {
        printf("List Is Empty\n\n");
    }
    else
    {
        while(p != NULL)
        {
            printf("%d->" , (p->n));
            p = p->next;
        }
        printf("\n\n");
    }
}

// Helper Function to Free the Memory Occupied by the List
void close(struct node *p)
{
    struct node *temp;
    while(p != NULL)
    {
        temp = p;
        p = p->next;
        free(temp);
    }
}

int main(int argc , char *argv[])
{
    int choice , n , result;
    struct node *head = NULL;
    
    system("clear");
    
    while(1)
    {
        printf("Select Your Operation\n");
        printf("1. Add an Element\n");
        printf("2. Delete an Element\n");
        printf("3. Search for an Element\n");
        printf("4. Display Contents of List\n");
        printf("5. Exit\n");
        printf("Enter Your Choice: ");
        fflush(stdin);
        scanf("%d" , &choice);
        printf("\n");
        
        if(choice == 1)
        {
            printf("Enter Element Value: ");
            fflush(stdin);
            scanf("%d" , &n);
            insert(&head , n);
            printf("Element Added Successfully\n\n");
        }
        else if(choice == 2)
        {
            printf("Enter Value of Element to Delete: ");
            fflush(stdin);
            scanf("%d" , &n);
            result = delete(&head , n);
            if(result == -1)
            {
                printf("Element Not Present in List\n\n");
            }
            else
            {
                printf("Element Deleted from Position %d\n\n" , result);
            }
        }
        else if(choice == 3)
        {
            printf("Enter Value of Element to Search for: ");
            fflush(stdin);
            scanf("%d" , &n);
            result = search(head , n);
            if(result == -1)
            {
                printf("Element Not Present in List\n\n");
            }
            else
            {
                printf("Element Found at Position %d\n\n" , result);
            }
        }
        else if(choice == 4)
        {
            display(head);
        }
        else if(choice == 5)
        {
            close(head);
            break;
        }
        else
        {
            printf("!!! Do Use Your Brain Before Typing !!!\n");
            printf("  (No Pressure If You Don't Have One)\n\n");
        }
    }
    
    printf("Thank You\n\n");
    
    return(0);
}
