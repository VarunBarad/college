// Author: Varun B Barad
// Enrolment No.: 130070107008
// Batch: 2A3

// Subject: Data and File Structure
// Practical: 02
// Topic: Implementation of a Stack Using Array

#include <stdio.h>
#include <stdlib.h>

#define MAX 20

struct stack
{
    int elements[MAX];
    int top;
};

// Function to Push an Element on to Stack
void push(struct stack *s , int n , int *o)
{
    if(s->top == (MAX-1))
    {
        *o = 1;
    }
    else
    {
        *o = 0;
        (s->top)++;
        s->elements[s->top] = n;
    }
}

// Function to Pop the Top-Element from the Stack
int pop(struct stack *s , int *u)
{
    int temp;
    if(s->top == (-1))
    {
        *u = 1;
        return(-1);
    }
    else
    {
        *u = 0;
        temp = s->elements[s->top];
        (s->top)--;
        return(temp);
    }
}

// Function to Display the Contents of Stack
void display(struct stack s)
{
    if(s.top > -1)
    {
        for(; s.top > 0 ; s.top--)
        {
            printf("%d->" , s.elements[s.top]);
        }
        printf("%d\n\n" , s.elements[s.top]);
    }
    else
    {
        printf("Stack is Empty\n\n");
    }
}

int main(int argc , char *argv[])
{
    struct stack s;
    int choice , n , o , u;
    
    system("clear");
    
    s.top = -1;
    
    while(1)
    {
        printf("Select Operation\n");
        printf("1. Push an Element\n");
        printf("2. Pop an Element\n");
        printf("3. Display the Contents\n");
        printf("4. Exit\n");
        printf("Enter Your Choice: ");
        fflush(stdin);
        scanf("%d" , &choice);
        printf("\n");
        
        if(choice == 1)
        {
            printf("Enter the Element to Push: ");
            fflush(stdin);
            scanf("%d" , &n);
            printf("\n");
            
            push(&s , n , &o);
            
            if(o == 1)
            {
                printf("OVERFLOW\nFailure to Push Element\n\n");
            }
            else
            {
                printf("Element Pushed Successfully\n\n");
            }
        }
        else if(choice == 2)
        {
            n = pop(&s , &u);
            
            if(u == 1)
            {
                printf("UNDERFLOW\nFailure to Pop Element\n\n");
            }
            else
            {
                printf("Element Successfully Popped\nValue: %d\n\n" , n);
            }
        }
        else if(choice == 3)
        {
            display(s);
        }
        else if(choice == 4)
        {
            break;
        }
        else
        {
            printf("!!! Enter Something Sensible You Moron !!!\n\n");
        }
    }
    
    printf("Thank You\n\n");
    
    return(0);
}
