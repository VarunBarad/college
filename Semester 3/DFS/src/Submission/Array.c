// Author: Varun B Barad
// Enrolment No.: 130070107008
// Batch: 2A3

// Subject: Data and File Structure
// Practical: 01
// Topic: Different Operations on Array

#include <stdio.h>
#include <stdlib.h>

#define MAX 20

// Function to insert an element at proper position in given ascending array
int insert(int *arr , int *length , int n)
{
    int i , pos;
    if(*length < MAX)
    {
        for(i = 0 ; ((i < *length) && (*(arr+i) < n)) ; i++);
        if(i == *length)
        {
            *(arr+i) = n;
            pos = i;
            (*length)++;
        }
        else
        {
            pos = i;
            (*length)++;
            for(i = *length ; i > pos ; i--)
            {
                *(arr+i) = *(arr+i-1);
            }
            *(arr+pos) = n;
        }
        return(pos);
    }
    else
    {
        return(-1);
    }
}

// Function to delete the first occurence of supplied element from given array
int delete(int *arr , int *length , int n)
{
    int i , pos;
    for(i = 0 ; ((i < *length) && (*(arr+i) != n)) ; i++);
    if(i == *length)
    {
        return(-1);
    }
    else
    {
        pos = i;
        for(; i < (*length - 1) ; i++)
        {
            *(arr + i) = *(arr + i + 1);
        }
        (*length)--;
        return(pos);
    }
}

// Function to remove duplicate values from given array
void remove_duplicates(int *arr , int *length)
{
    int i , j , k;
    
    for(i = 0 ; i < *length ; i++)
    {
        for(j = (i+1) ; j < *length ; j++)
        {
            if(*(arr+j) == *(arr+i))
            {
                for(k = j ; k < (*length-1) ; k++)
                {
                    *(arr+k) = *(arr+k+1);
                }
                (*length)--;
                j--;
            }
        }
    }
}

// Function to display all the elements of the given array
void display(int *arr , int length)
{
    int i;
    if(length == 0)
    {
        printf("Array is Empty\n\n");
    }
    else
    {
        for(i = 0 ; i < (length-1) ; i++)
        {
            printf("%d->" , *(arr+i));
        }
        printf("%d\n\n" , *(arr+i));
    }
}

// Function to sort the supplied array in ascending order
void sort(int *arr , int length)
{
    int i , j , temp;
    for(i = 0 ; i < (length-1) ; i++)
    {
        for(j = (i+1) ; j < length ; j++)
        {
            if(*(arr+i) > *(arr+j))
            {
                temp = *(arr+i);
                *(arr+i) = *(arr+j);
                *(arr+j) = temp;
            }
        }
    }
}

// Function to find the index of the first occurence of specified element in given array
int search(int *arr , int length , int n)
{
    int i;
    for(i = 0 ; i < length ; i++)
    {
        if(*(arr+i) == n)
        {
            return(i);
        }
    }
    return(-1);
}

int main(int argc , char *argv[])
{
    int array[MAX] , length = 0 , choice , n , result;
    
    system("clear");
    
    while(1)
    {
        printf("Select Operation\n");
        printf("1. Insert an Element\n");
        printf("2. Delete an Element\n");
        printf("3. Search for an Element\n");
        printf("4. Sort the Array in Ascending Order\n");
        printf("5. Display All the Elements\n");
        printf("6. Exit\n");
        printf("Enter Your Choice: ");
        fflush(stdin);
        scanf("%d" , &choice);
        printf("\n");
        
        if(choice == 1)
        {
            printf("Enter Value to Insert: ");
            fflush(stdin);
            scanf("%d" , &n);
            printf("\n");
            
            result = insert(array , &length , n);
            
            if(result == -1)
            {
                printf("Array Already Full\nFailed to Insert Element\n\n");
            }
            else
            {
                printf("Element Inserted at Position %d\n\n" , result);
            }
        }
        else if(choice == 2)
        {
            printf("Enter Element to Delete: ");
            fflush(stdin);
            scanf("%d" , &n);
            printf("\n");
            
            result = delete(array , &length , n);
            
            if(result == -1)
            {
                printf("Element Not Found in Array\nFailed to Delete Element\n\n");
            }
            else
            {
                printf("Element Deleted from Position %d\n\n" , result);
            }
        }
        else if(choice == 3)
        {
            printf("Enter Element to Search for: ");
            fflush(stdin);
            scanf("%d" , &n);
            printf("\n");
            
            result = search(array , length , n);
            
            if(result == -1)
            {
                printf("Element Not Found\nFailure to Retrieve Index\n\n");
            }
            else
            {
                printf("Element Found at Position %d\n\n" , result);
            }
        }
        else if(choice == 4)
        {
            sort(array , length);
            
            printf("Array Sorted in Ascending Order\n\n");
        }
        else if(choice == 5)
        {
            display(array , length);
        }
        else if(choice == 6)
        {
            break;
        }
        else
        {
            printf("!!! Enter a Valid Choice You Idiot !!!\n\n");
        }
    }
    
    printf("Thank You\n\n");
    
    return(0);
}
