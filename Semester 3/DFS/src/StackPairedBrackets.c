#include <stdio.h>
#include <stdlib.h>

#define MAX 20

struct stack
{
	char elements[MAX];
	int top;
};

void push(struct stack *s , char c , int *o)
{
	if(s->top == (MAX-1))
	{
		*o = 1;
	}
	else
	{
		*o = 0;
		(s->top)++;
		s->elements[s->top] = c;
	}
}

char pop(struct stack *s , int *u)
{
	char temp;
	if(s->top == (-1))
	{
		*u = 1;
		return('#');
	}
	else
	{
		*u = 0;
		temp = s->elements[s->top];
		(s->top)--;
		return(temp);
	}
}

int isEmpty(struct stack s)
{
	int u;
	pop(&s , &u);
	return(u);
}

int checkExpression(char str[])
{
	struct stack s;
	int i , o , u;
	
	s.top = -1;
	
	for(i = 0 ; str[i] != 0 ; i++)
	{
		if((str[i] == '{') || (str[i] == '[') || (str[i] == '('))
		{
			push(&s , str[i] , &o);
		}
		else if(str[i] == '}')
		{
			if((isEmpty(s)) || (pop(&s , &u) != '{'))
			{
				return(0);
			}
		}
		else if(str[i] == ']')
		{
			if((isEmpty(s)) || (pop(&s , &u) != '['))
			{
				return(0);
			}
		}
		else if(str[i] == ')')
		{
			if((isEmpty(s)) || (pop(&s , &u) != '('))
			{
				return(0);
			}
		}
	}
	
	if(!isEmpty(s))
	{
		return(0);
	}
	else
	{
		return(1);
	}
}

int main(int argc , char *argv[])
{
	int choice , o , u;
	char c , str[21];
	
	system("clear");
	
	while(1)
	{
		printf("What would You Like to Do\n");
		printf("1. Check an Expression\n");
		printf("2. Exit\n");
		printf("Enter Your Choice: ");
		fflush(stdin);
		scanf("%d" , &choice);
		printf("\n");
		
		if(choice == 1)
		{
			printf("Enter the Expression to Check(Max. 20 Characters): ");
			fflush(stdin);
			scanf("%s" , str);
			printf("\n");
			
			if(checkExpression(str))
			{
				printf("Bracket Parity is Valid\n\n");
			}
			else
			{
				printf("Invalid Bracket Pairs Found\n\n");
			}
		}
		else if(choice == 2)
		{
			break;
		}
		else
		{
			printf("Enter a Valid Choice\n\n");
		}
	}
	
	printf("Thank You\n");
	
	return(0);
}
