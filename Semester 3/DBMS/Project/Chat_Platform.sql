-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 27, 2014 at 03:08 PM
-- Server version: 5.5.39-MariaDB
-- PHP Version: 5.5.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Chat_Platform`
--

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `hash` varchar(140) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL,
  `path` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`hash`, `name`, `path`) VALUES
('66129e7cb8c6b406f00ec55a54aa9f455823ee02', 'Torrent.torrent', '/var/www/html/files/');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `group_id` varchar(20) NOT NULL DEFAULT '',
  `group_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`group_id`, `group_name`) VALUES
('2014-11-27 20:29:13', 'DBMS Project');

-- --------------------------------------------------------

--
-- Table structure for table `group_members`
--

CREATE TABLE IF NOT EXISTS `group_members` (
  `group_id` varchar(20) NOT NULL DEFAULT '',
  `member_id` varchar(12) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `group_members`
--

INSERT INTO `group_members` (`group_id`, `member_id`) VALUES
('2014-11-27 20:29:13', '917600647682'),
('2014-11-27 20:29:13', '919879842236');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `message_id` varchar(20) NOT NULL,
  `sender_id` varchar(12) NOT NULL,
  `content` varchar(140) NOT NULL,
  `type` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`message_id`, `sender_id`, `content`, `type`) VALUES
('2014-11-27 20:18:35', '919879842236', 'This is our first text! Hello World!', 'text'),
('2014-11-27 20:20:04', '917600647682', 'Congratulations', 'text'),
('2014-11-27 20:23:55', '917600647682', '66129e7cb8c6b406f00ec55a54aa9f455823ee02', 'file'),
('2014-11-27 20:34:45', '919879842236', 'Broadcast message', 'text');

-- --------------------------------------------------------

--
-- Table structure for table `recipients`
--

CREATE TABLE IF NOT EXISTS `recipients` (
  `message_id` varchar(20) NOT NULL DEFAULT '',
  `recipient_id` varchar(12) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `recipients`
--

INSERT INTO `recipients` (`message_id`, `recipient_id`) VALUES
('2014-11-27 20:18:35', '917600647682'),
('2014-11-27 20:20:04', '919879842236'),
('2014-11-27 20:23:55', '919879842236'),
('2014-11-27 20:34:45', '917600647682'),
('2014-11-27 20:34:45', '919879842236');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `mobile_no` varchar(12) NOT NULL DEFAULT '',
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`first_name`, `last_name`, `mobile_no`, `password`) VALUES
('Alay', 'Patel', '917600647682', 'e88a49bccde359f0cabb40db83ba6080'),
('Varun', 'Barad', '919879842236', 'a13ee062eff9d7295bfc800a11f33704'),
('Kirti', 'Sharma', '919924431581', 'cf8c1a5f284ea61c6d7d21b2843a5131');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `files`
--
ALTER TABLE `files`
 ADD PRIMARY KEY (`hash`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `group_members`
--
ALTER TABLE `group_members`
 ADD PRIMARY KEY (`group_id`,`member_id`), ADD KEY `member_id` (`member_id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
 ADD PRIMARY KEY (`message_id`);

--
-- Indexes for table `recipients`
--
ALTER TABLE `recipients`
 ADD PRIMARY KEY (`message_id`,`recipient_id`), ADD KEY `recipient_id` (`recipient_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`mobile_no`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `group_members`
--
ALTER TABLE `group_members`
ADD CONSTRAINT `group_members_ibfk_2` FOREIGN KEY (`member_id`) REFERENCES `users` (`mobile_no`),
ADD CONSTRAINT `group_members_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`group_id`);

--
-- Constraints for table `recipients`
--
ALTER TABLE `recipients`
ADD CONSTRAINT `recipients_ibfk_2` FOREIGN KEY (`message_id`) REFERENCES `message` (`message_id`),
ADD CONSTRAINT `recipients_ibfk_1` FOREIGN KEY (`recipient_id`) REFERENCES `users` (`mobile_no`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
